using System;
using System.Collections;
using System.Collections.Generic;

using DG.Tweening;
using DG.Tweening.Core;
using PirateSwim;

using PirateSwim.ShipScripts;
using Sirenix.OdinInspector;
using TickedPriorityQueue;
using UnityEngine;

namespace UnitySteer.Behaviors
{
    public class FlockFollowBase : Steering, ISpell
    {
        /// <summary>
        /// Target point
        /// </summary>
        /// <remarks>
        /// Declared as a separate value so that we can inspect it on Unity in 
        /// debug mode.
        /// </remarks>
     protected CustomTarget _target ;

        /// <summary>
        /// Should the vehicle's velocity be considered in the seek calculations?
        /// </summary>
        /// <remarks>
        /// If true, the vehicle will slow down as it approaches its target. See
        /// the remarks on GetSeekVector.
        /// </remarks>
        [SerializeField] bool _considerVelocity;

        [FoldoutGroup("Params")] public bool inveulable = false;

        [FoldoutGroup("Params")] [SerializeField]
        int timeToExplode;

        [FoldoutGroup("Params")]   [SerializeField]
        bool cantExplode;

        [FoldoutGroup("Params")] public float RadomAimOffsetRadius  ;
        [FoldoutGroup("Direct")] public float directDurration  ;
        [FoldoutGroup("Direct")] public float DirectDelay     ;
        [FoldoutGroup("Direct")] public float aimingTimeCoef   ;
        [FoldoutGroup("Time")]   public float approachDuration ;
        Rigidbody                             _rigid;
        public    LayerMask                   _layerMaskAffected;
        protected Vector3                     randomAimOffset;
        public    GameObject                  ExplodeParticles ;
        public    float                       PredictorTick = -1;

        [SerializeField, FoldoutGroup("Explosion")]
        float ExplosionRadius = 10;

        [SerializeField, FoldoutGroup("Explosion")]
        float damage;

        [SerializeField, FoldoutGroup("Explosion")]
        float ExplosionForce;


        /// <summary>
        /// Should the vehicle's velocity be considered in the seek calculations?
        /// </summary>
        /// <remarks>
        /// If true, the vehicle will slow down as it approaches its target
        /// </remarks>
        public bool ConsiderVelocity
        {
            get { return _considerVelocity; }
            set { _considerVelocity = value; }
        }

        public new Rigidbody rigidbody
        {
            get
            {
                if (!_rigid)
                {
                    _rigid = gameObject.GetOrAdd<Rigidbody>();
                    _rigid.useGravity = false;
                }

                return _rigid;
            }
            set { _rigid = value; }
        }

        public bool Inited { get; set; } = false;

        [Serializable]
        public class CustomTarget:IEnumerable<CustomTarget>
        {
            public delegate T Func<out T>(float t, float d);

            Func<bool> _canEndFunc;
            Func<bool> _canStartFunc;
            public bool Completed()           => complete||isCanEnd;

            public bool isStarted
            {
                get
                {
                 
                    return started;
                }
            }


             DOGetter<Vector3>    pos;
            Action<CustomTarget> OnEnd;
            Action<CustomTarget> OnStart;
            private bool         complete  ;
            private bool         started  ;
            public  float        tickedTime;
            public  string       QueueName                   = "Targets";
            public  int          _maxQueueProcessedPerUpdate = 10;

            public CustomTarget nextTarget;
           // public static int count = 0;
            public CustomTarget(
                Func<bool>           canEnd,
                DOGetter<Vector3>    pos,
                Transform owner,
                float                p_tickedTime                 = .5f,
                Action<CustomTarget> onEnd                        = null,
                Action<CustomTarget> onStart                      = null,
                Func<bool>           canStartFunc                 = null,
                int                  p_maxQueueProcessedPerUpdate = 2,
                string               p_QueueName                  = "Targets"
            )
            {
                _canEndFunc = canEnd;
          
                _canStartFunc =( canStartFunc !=null?canStartFunc:(d,t)=>true);
                
                this.pos = pos;
                OnEnd = onEnd;
                OnStart = onStart;
               // QueueName = p_QueueName+count++;
                _maxQueueProcessedPerUpdate = p_maxQueueProcessedPerUpdate;
                this.owner = owner;

                var ticked = new TickedObject(Check);
           
                ticked.TickLength = tickedTime;

                var _Queue = UnityTickedQueue.GetInstance(QueueName);
                _Queue.Add(ticked);
                _Queue.MaxProcessedPerUpdate = _maxQueueProcessedPerUpdate;
                started = false;
                complete = false;
              //  CheckEnd();
            }

            Transform owner;
            public float startTime  ;
            public float t => started?Time.time - startTime:0;
            public float d =>Vector3.Distance(pos.Invoke(),owner.position);
            public int num = 0;

            public DOGetter<Vector3> Pos
            {
                get
                {
                  

                    return pos;
                }
                set { pos = value; }
            }

            public CustomTarget Chain( CustomTarget other)
            {
                other.CanStartFunc =(d,t)=> this.Completed();
                other.num = num + 1;
                return other;
            }

            void Check(object usrDat)
            {
              //  Debug.Log("dist:"+d);
                CheckEnd();
                
            }

            bool isCanStart
            {
                get
                {
                    if (started||_canStartFunc==null)
                    {
                        return true;
                    }

                    var x = _canStartFunc != null && _canStartFunc.Invoke(t,d);
                  

                    return x ;
                }
            }

            bool CheckEnd()
            {
                if (complete)
                {
                    return true;
                }

                if (!started)
                {
                    if (isCanStart)
                    {
                        if (!started)
                        {
                            started = true;
                          
                            if (OnStart != null)
                            {
                                OnStart.Invoke(this);
                               
                                OnStart = null;
                            }

                            startTime = Time.time;
                        }
                    } else
                    {
                        return false;
                    }
                }

                bool res;
                
                res = isCanEnd;
                if (res && !complete)
                {
                    complete = true;
                    if (OnEnd != null)
                    {
                        
                        OnEnd(this);
                        OnEnd = null;
                    }
                }

                return res;
            }

            public bool isCanEnd
            {
                get
                {
                
                    return _canEndFunc(t,d);
                }
       
            }

         
            public Func<bool> CanStartFunc
            {
                get { return _canStartFunc; }
                set { _canStartFunc = value;
                    
                    startTime = Time.time;
                    started = false;
                }
            }


            class Numerator : IEnumerator<CustomTarget>
            {
                CustomTarget value;


                public bool MoveNext()
                {
                    value = value.nextTarget;
                    return value == null;
                }

                public void Reset()
                {
                   
                }

                public CustomTarget Current { get; }

                object IEnumerator.Current => Current;

                
                public void Dispose()
                {
                  
                }
            }

            public System.Collections.Generic.IEnumerator<CustomTarget> GetEnumerator()
            {
                return new Numerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }


        [FoldoutGroup("Params"), ShowInInspector]
        public Vector3 PosOffset { get; set; }

       protected CustomTarget Target => Get();
        protected virtual Vector3 hitPoint( )
        {
            if (Target != null && Target.isStarted)
            {
                return  randomAimOffset + Target.Pos() + PosOffset;
            }

            return Vector3.zero;
        }

        protected Sequence mainSeq;

        [Button]
        public virtual void Init()
        {
            Inited = true;
            mainSeq = DOTween.Sequence();
         base.Awake();
        }

        public CustomTarget Get()
        {
            while (_target != null && _target.Completed())
            {
                _target = _target.nextTarget;
            }

            return _target;

          
        }


        public CustomTarget Set(CustomTarget value)
        {
            if (_target == value) return _target;
            _target = value;

            ReportedArrival = false;
            return _target;
        }
        




        protected void Explode()
        {
            if (cantExplode)
            {
                return;
            }

            var objects =   Physics.OverlapSphere(transform.position, ExplosionRadius, _layerMaskAffected.value);


            foreach (var o in objects)
            {
                if (o.attachedRigidbody)
                {
                    var rigid       = o.attachedRigidbody;
                    var damageValue = damage / (transform.position - rigid.position).sqrMagnitude;
                    if (damageValue > Mathf.Epsilon)
                    {
                        rigid.SendMessage("GetDamage", damageValue, SendMessageOptions.DontRequireReceiver);
                    }

                    rigid.AddExplosionForce(ExplosionForce, transform.position, ExplosionRadius);
                }
            }


            if (ExplodeParticles != null)
            {
                ExplodeParticles.transform.parent = null;
                ExplodeParticles.SetActive(true);
            }


            Destroy();
        }

        public virtual void Destroy()
        {
            if (inveulable)
            {
                return;
            }

            //  Debug.Log("Destroyd" + name + " :pos " + transform.position);
            this.rigidbody.velocity = Vector3.zero;
            this.rigidbody.angularVelocity = Vector3.zero;
            Inited = false;
            gameObject.SetActive(false);
            CancelInvoke();
        }

        [ShowInInspector, ReadOnly] protected Vector3 targetPoint;

        /// <summary>
        /// Calculates the force to apply to the vehicle to reach a point
        /// </summary>
        /// <returns>
        /// A <see cref="Vector3"/>
        /// </returns>
        protected override Vector3 CalculateForce()
        {
            Vector3 x;
            if (Target != null && Inited && Vector3.Distance(( x = hitPoint()), transform.position) > Vehicle.ArrivalRadius)
            {
                targetPoint = x;
                return Vehicle.GetSeekVector(targetPoint , _considerVelocity);
            }

            return Vector3.zero;
        }

        [SerializeField] bool drawGizmos = true;
        float prev=0;
        float cur=0;
        int count = 0;
        void OnDrawGizmos()
        {
            if (Inited && drawGizmos && Target != null )
            {
                Gizmos.DrawLine(transform.position, targetPoint);
              

                Utils.DrawString("count="+count+"\nprev="+prev+"\nt=" + Target.startTime + "\nd=" + Target.d, transform.position + Vector3.up * 15f, Color.red);
                if (cur != Target.startTime)
                {
                    prev = cur;
                    count++;
                    cur = Target.startTime;
                }
            }
        }

        public float PredictTime { get ; set ; }
    }
}