using MoreMountains.Tools;
using PirateSwim.Scripts.ShipScripts;
using Sirenix.OdinInspector;
using UnityEngine;

namespace UnitySteer.Behaviors
{
    [AddComponentMenu("UnitySteer/Steer/ShipFollow")]
    public class ShipFollow : FlockFollowBase
    {
        Ship _ship;


        void OnCollisionEnter(Collision other)
        {
            if (_layerMaskAffected.Contains(other.gameObject.layer))
            {
                Explode();
            }

        }

        protected override Vector3 CalculateForce()
        {
            if (Target != null)
            {
                Debug.Log(Target.QueueName + " :" + Target.isStarted + ". End:" + Target.isCanEnd);
            }

            return base.CalculateForce();
        }

       [SerializeField,ReadOnly] CustomTarget first  ;
        public override void Init()
        {
            base.Init();
            _ship=Ship.Main;
            
        
          
            
         
            if (approachDuration > 0)
            {
                var startTime = Time.time + 5f;
               

                first = new CustomTarget((d, t)=>t>1, () => transform.forward * Vehicle.Speed + transform.position,transform);
                    var second =
                    new CustomTarget((d, t) => d < Vehicle.ArrivalRadius, ()=>_ship.ShipPosAtTime(aimingTimeCoef),transform);
                var third=new CustomTarget( (d, t)=>t>3,()=>transform.position+Vector3.up,transform);
                first.Chain(second).Chain(third);
                Set(first);

            } else
            {
             var first=   new CustomTarget((d, t) => d < VeticalWave, () => _ship.ShipPosAtTime(aimingTimeCoef),transform);
                Set(Target);
            }
            var meshRenderer = GetComponentInChildren<MeshRenderer>();
#if UNITY_EDITOR
            var material = meshRenderer.sharedMaterial;
            #else
  var material = meshRenderer.material;
            #endif
            if (material != null&&material.HasProperty("_RainbowRange"))
            {
                MaterialPropertyBlock block=new MaterialPropertyBlock();
                
                block.SetFloat("_RainbowRange", Random.value * 2);
                meshRenderer.SetPropertyBlock(block);
            }
        }

      
      //  [SerializeField] bool drawGizmos=true;
        [SerializeField] int VeticalWave=20;

     
    }
    
}