using PirateSwim.Scripts.ShipScripts;

namespace UnitySteer.Behaviors
{
    public class ShipPredictor : SteerPredictor
    {
        Ship _ship;

        protected override void Awake()
        {
          Reset();
            base.Awake();
        }

        protected override void Reset()
        {
            _ship = Ship.Main;
            if (_ship)
            {
                predictor = _ship.Predictor;
                targetTransform = _ship.transform;
                _transform = _ship.transform;
                isUpdatePredictor = false;
            }
            base.Reset();
        }
    }
}