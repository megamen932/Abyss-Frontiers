using DG.Tweening.Core;
using JetBrains.Annotations;
using PirateSwim.ShipScripts;
using Sirenix.OdinInspector;
using TickedPriorityQueue;
using UnityEngine;

namespace UnitySteer.Behaviors
{
    public class SteerPredictor : DetectableObject
    {
       public PosPredictor predictor;

        /// <summary>
        /// The target point.
        /// </summary>
        public DOGetter<Vector3> Target
        {
            get { return _target; }
            set
            {
                if (_target == value) return;
                _target = value;
                predictor.Reset();
            }
        }


        public override Vector3 PredictFuturePosition(float predictionTime)
        {
            return predictor.PredictPosition(predictionTime);
        }

        [SerializeField] DOGetter<Vector3> _target ;

        [OnValueChanged(nameof(SetTargetTransform))] [SerializeField]
       protected Transform targetTransform ;


        [SerializeField] float _tickLength = 0.2f;
        [SerializeField] string _queueName  = "Predictor";
        TickedObject            _tickedObject;
        UnityTickedQueue        Queue;
        [SerializeField] int    _maxQueueProcessedPerUpdate;

        [SerializeField] float predictTime = 2f;
        
        //  [SerializeField] Vector3 PosOffset;
        //   Vector3 randomAimOffset;
        //   [SerializeField]float randomAiRadius;

        protected virtual void UpdatePredictor(object userData)
        {
            if (Target != null)
            {
                predictor.AddPosition(Target.Invoke(),_tickLength);
            }

            position = PredictFuturePosition(predictTime);
        }

        protected override void Awake()
        {
            base.Awake();
            SetTargetTransform();
        }

        void OnDrawGizmosSelected()
        {
            if (Target == null)
            {
                SetTargetTransform();
            } else
            {
                Gizmos.DrawLine(Target.Invoke(), position);
            }
        }

     [ReadOnly]   public Vector3 position;
        protected override void OnEnable()
        {
            base.OnEnable();

            //  randomAimOffset = Random.onUnitSphere * randomAiRadius;
            if (isUpdatePredictor)
            {
                _tickedObject = new TickedObject(UpdatePredictor) {TickLength = _tickLength};
                Queue = UnityTickedQueue.GetInstance(_queueName);
                Queue.Add(_tickedObject);
                Queue.MaxProcessedPerUpdate = _maxQueueProcessedPerUpdate;
                if (predictor == null)
                {
                    predictor = new PosPredictor();
                }
            }

            _transform = targetTransform;
        }

        public bool isUpdatePredictor = true;

        public override Vector3 Position
        {
            get
            {
                if (Target != null)
                {
                    return Target.Invoke();
                } else
                {
                    if (targetTransform != null)
                    {
                      return targetTransform.position + targetTransform.rotation * Center;
                    }

                    if (!_transform)
                    {
                        Reset();
                    }

                    return _transform.position;
                }
            }
        }

        void SetTargetTransform()
        {
            if (targetTransform != null)
            {
                Target = () => targetTransform.position+targetTransform.rotation*Center;
            }
        }

        protected virtual void Reset()
        {
            SetTargetTransform();
           
        }

        protected override void OnDisable()
        {
            if (Queue != null)
            {
                Queue.Remove(_tickedObject);
            }
        }
    }
}