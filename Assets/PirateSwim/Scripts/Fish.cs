﻿using UnityEngine;

public class Fish : MonoBehaviour
{
    public Transform target;
    public float     speed;

    void Start()
    {
        transform.position = Random.insideUnitSphere * 80;
    }

    void FixedUpdate()
    {
        Vector3 targetDir = (target.position + Random.insideUnitSphere * 80 + (Vector3.up * Random.Range(-50, 50))) -
                            transform.position;
        Vector3    forward  = transform.forward;
        Quaternion rotation = Quaternion.LookRotation(targetDir);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 1.5f);
        transform.position += forward / speed;
    }
}