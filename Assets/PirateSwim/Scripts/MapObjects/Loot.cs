using System.Collections;
using PirateSwim.Scripts.ShipScripts;
using UnityEngine;
using Random = UnityEngine.Random;

namespace PirateSwim
{
    [RequireComponent(typeof(Rigidbody))]
    public class Loot : MapObject
    {
        public float amount = 3f;


        public float                   MinDist = 10;
        public Vector3                 startPos;
        public Transform               CannonTransform;
        public RopeControllerRealistic rope;
        public GameObject              sword;
        public Transform               LootSpawnTransform;
        public bool                    Inited = false;
        public GameObject self;

        public enum LootStates
        {
            Free,
            Targeting,
            Looting,
            Grabbing,
            Scored
        }

        [SerializeField] LootStates _state = LootStates.Free;


        public GameObject Init()
        {
           
            collider = self.GetComponentInGOAndChildren<Collider>();
            Inited = true;
            return self;
        }





        protected override void  Start()
        {
            base.Start();
            if (!Inited)
            {
                Init();
            }

            rigidbody = GetComponent<Rigidbody>();
            startPos = transform.position;
            var startAmount = amount;
            amount = amount * Random.Range(.25f, 3f);

            ropeGrabSpeed = ropeGrabSpeed / (Mathf.Max(0.75f, amount - startAmount));           
            //   itemToSpawn.transform.localPosition = Vector3.zero;

            gravity = Physics.gravity;
            if (sword)
            {
                sword.SetActive(looting);
            }
        }

        public void StartLooting(Ship ship1, RopeControllerRealistic cannon)
        {
           _ship = ship1;
            CannonTransform = cannon.transform;
            rope = cannon;

            State = LootStates.Looting;

            if (sword != null)
            {
                sword.SetActive(true);
            }

            LootSpawnTransform.parent = cannon.target;
            // sword = LootSpawnTransform.gameObject;
        }

        float   grabTimer = 0;
        Vector3 gravity   = Vector3.zero;

        public override void ManualUpdate(object usrData)
        {
            //LootSpawnTransform.position = sword.transform.position+new Vector3(0,-0.134286f,3.742113f);
            if (State == LootStates.Free)
            {
                if (Camera.main != null && transform.position.z < Camera.main.transform.position.z)
                {
                    Die();
                }

                var transformPosition = transform.position;
                if (Camera.current != null)
                {
                    if (isSharpNoise)
                    {
                        transformPosition.y += Mathf.Cos(amount / 6 * (Time.time - startPos.z) * Mathf.PI / 3
                                               ) *
                                               waterWaveHeight * deltaTime;
                    }
                    else
                    {
                        transformPosition += Random.rotation * Vector3.one * Mathf.Cos(
                                                 amount / 6 * (Time.time - startPos.z) * Mathf.PI / 3
                                             ) *
                                             waterWaveHeight * deltaTime;
                    }

                    if (isUseGravity)
                    {
                        if (isAddNewGravity)
                        {
                            gravity += Physics.gravity * deltaTime;
                        }

                        if (gravity.magnitude > 10)
                        {
                            gravity.Scale(Random.insideUnitSphere);
                        }

                        transformPosition += (gravity * deltaTime);
                    }
                }

                transform.position = transformPosition;
            }
            else
            {
                if (!_ship)
                {
                    return;
                }

                if (State == LootStates.Looting)
                {
                    if (grabTimer < 0)
                    {
                        rope.Grab(amount);


                        if (grabTimer < -grabPullTime)
                        {
                            grabTimer = grabRestTime;
                        }
                    }

                    if (rope.ropeSectionLength <= MinDist + Mathf.Epsilon ||
                        Vector3.Distance(transform.position, CannonTransform.position) < MinDist)
                    {
                        StartCoroutine("GrabFromWater");
                    }

                    grabTimer -= deltaTime;
                }
            }
        }

        public bool looting
        {
            get { return State == LootStates.Looting; }
        }

        public bool free
        {
            get { return State == LootStates.Free; }
        }

        public LootStates State
        {
            get { return _state; }

            set
            {
                if (value > _state)
                    _state = value;
            }
        }

        [Range(0, 10)] public float      grabFromShipBackSpeed = 0.5f;
        public                GameObject ParticlesPrefab;
        [Range(0, 10)] public float      waterWaveHeight = 4f;
        [Range(0, 5)]  public float      grabPullTime    = 0.3f;
        [Range(0, 5)]  public float      grabRestTime    = 0.5f;

        public           float ropeGrabSpeed   = 1f;
        [SerializeField] bool  isSharpNoise    = false;
        [SerializeField] bool  isAddNewGravity = false;
        [SerializeField] bool  isUseGravity    = true;

        void Die()
        {
            if (_ship && State == LootStates.Grabbing)
            {
               _ship.Score(amount);
            }

            State = LootStates.Scored;
            Destroy(gameObject);
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.name == "CargoGrabber" && State != LootStates.Grabbing)
            {
                StartCoroutine("GrabFromWater");
            }
        }

        IEnumerator GrabFromWater()
        {
            rigidbody.isKinematic = true;
            State = LootStates.Grabbing;
            LootSpawnTransform.parent = transform; //отцепляем от меча, и удаляем его
            sword.transform.parent = null;
            if (sword != null)
            {
//sword.SetActive(false);
                Destroy(sword, 30);
            }

            if (rope && rope.target == transform)
            {
                rope.Init(null);
            }

            if (_ship != null)
            {
                transform.parent =_ship.GrabTransform;
                var     newPos = Vector3.zero;
                Vector3 speed  = Vector3.zero;
                if (sword != null)
                {
                    //  sword.transform.parent = transform;
                    speed = 2.5f *_ship.speed * sword.transform.forward;
                }

                for (float i = 0; i < 3; i += Time.deltaTime / grabFromShipBackSpeed)
                {
                    speed += Physics.gravity * Time.deltaTime;
                    if (sword != null)
                    {
                        sword.transform.Translate(speed * Time.deltaTime);
                    }

                    LootSpawnTransform.localPosition = Vector3.Lerp(transform.localPosition, newPos,
                        i);
                    yield return null;
                }
            }

            if (ParticlesPrefab != null)
            {
                Instantiate(ParticlesPrefab, transform);
            }

            Die();
        }

        public void StartTargeting()
        {
            State = LootStates.Targeting;
        }
    }
}