using UnityEngine;

namespace PirateSwim
{
    public class BoidFishSwarm : MonoBehaviour
    {
        public Transform boidPrefab;
        public int       swarmCount = 100;
        public int maxRadius = 25;
        public float speed = 16;
        public Transform target;
        Transform startTarget;
        public float zSpeed = 50f;
        public bool auto_start = true;
        void Start()
        {
            if(!auto_start){return;}

            GenerateSwarm();
        }

        void GenerateSwarm()
        {
            startTarget = target;
            for (var i = 0; i < swarmCount; i++)
            {
                var boid = Instantiate(boidPrefab, Random.insideUnitSphere * maxRadius, Quaternion.identity)
                    .GetComponent<Boid>();
                boid.maxRadius = maxRadius;
                boid.transform.parent = transform;
                boid.maxSpeed = (int) (boid.maxSpeed * Random.Range(0.8f, 1.2f));
            }
        }

        void FixedUpdate()
        {
            if (!target)
            {
                target = startTarget;
                
            }
            //transform.up=Vector3.up;
            if(startTarget.position.z>target.position.z||Vector3.Distance(startTarget.position,transform.position)>150)
            {
                target = startTarget;
            }
            
            var position = transform.position;
            var oldz = position.z;
            position.z = target.position.z;
             position = Vector3.Lerp(position, target.position, Time.deltaTime * speed);
            position.z = oldz+((target.position.z +70- oldz) * Time.deltaTime * zSpeed);
            transform.position = position;
        }

        void OnTriggerEnter(Collider other)
        {
          //  Debug.Log("triger target "+other.name);
            if(other.transform==startTarget){return;}
            if(Vector3.Distance(other.transform.position,startTarget.transform.position)<150)
            {
                target = other.transform;
                Debug.Log("new boid target "+other.name);
            }
        }
    }
}