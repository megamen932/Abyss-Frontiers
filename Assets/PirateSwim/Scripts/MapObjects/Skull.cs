﻿using System.Collections;
using DG.Tweening;
using PirateSwim.Scripts.ShipScripts;
using UnityEngine;
using VLB;

public class Skull : MonoBehaviour
{
    // Use this for initialization
    void Start ()
    {
        _ship = Ship.Main;
        beams = GetComponentsInChildren<VolumetricLightBeam>();
        foreach (var beam in beams)
        {
          //  beam.alphaInside = 0.1f;
         //   beam.alphaOutside = 0.1f;
        }
    }

   
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            StartCoroutine(ShootLasers());
        }
    }

    VolumetricLightBeam[] beams;
    Ship                  _ship;
    public float          StartDamging = 12f;
    public float          DamagingTime = 5;
    public float          Damage       = 5;
    public float          state        = 0;

    IEnumerator ShootLasers()
    {
        var startValue = 0.2f;
        var countOfDamagweCyles = (int) (DamagingTime / Time.fixedDeltaTime);
        state = 0;
        var rotation = transform.rotation;
        var duration = StartDamging / 3f;
        var tween = transform.DORotateQuaternion(Quaternion.LookRotation(_ship.transform.position - transform.position), duration);
        yield return tween.WaitForCompletion();
        state += 1;
        var shoot=DOTween.Sequence();
        foreach (var beam in beams)
        {
            
            shoot.Join(DOTween.To((x) => beam.alphaInside = x,  startValue, 1, duration)).Join(DOTween.To((x)=>beam.alphaOutside=x, startValue,1,duration));
            state += 5;
        }

        yield return shoot.WaitForCompletion();
        state += 10f;


        var Lasers = DOTween.Sequence();
        foreach (var beam in beams)
        {
            Lasers.Join(DOTween.To((x) => beam.fadeStart = x, 0.368f, 8, duration));
            state += 20f;
        }

      //  Lasers.AppendInterval(StartDamging);
       yield return Lasers.WaitForCompletion();
  
        state += 100;
        
        for (int i = 0; i < countOfDamagweCyles; ++i)
        {
            Debug.Log("Angle is:"+Vector3.Angle(transform.forward, _ship.transform.position - transform.position) );

            if (Vector3.Angle(transform.forward, _ship.transform.position - transform.position) < 0.5f)
            {
                _ship.GetDamage(Damage / countOfDamagweCyles);
                state += 500f/countOfDamagweCyles;
            }

            yield return new WaitForFixedUpdate();
        }

        state = -1000f;
        var fadeOut = DOTween.Sequence();
        foreach (var beam in beams)
        {

            fadeOut.Join(DOTween.To((x) => beam.alphaInside = x, 1, startValue, duration)).Join(DOTween.To((x) => beam.alphaOutside = x, 1, startValue, duration)).
                Join(DOTween.To((x) => beam.fadeStart = x,8, 0.368f, duration));
        }

        fadeOut.Join(transform.DORotateQuaternion(rotation, duration ));

        yield return  fadeOut.WaitForCompletion();
    }
}