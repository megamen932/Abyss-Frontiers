﻿using System.Collections;
using System.Collections.Generic;
//using DG.Tweening;
//using DG.Tweening.Plugins.Core.PathCore;
using PirateSwim;

using Sirenix.OdinInspector;
using UnityEngine;

public class AssemblyFigure : MonoBehaviour
{
    public List<Transform> parts = new List<Transform>();
    Vector3[]              points;
   // Path[]                 paths;

    public MeshFilter _meshFlter;


    public float duration              = 5;
    public int   _subdivisionsXSegment = 10;

    public bool createParts = true;
    public int  partsCount  = 1000;


    public float finalScale = 10;


    void Start()
    {
        if (createParts)
        {
            for (int i = 0; i < partsCount; i++)
            {
                parts.Add(GameObject.CreatePrimitive(PrimitiveType.Cube).transform);
                parts[i].localScale *= 1f;
            }
        }


        if (parts.Count > 2) StartAssembly();
    }




[Button("Start")]
    void StartAssembly()
{
   
        StartCoroutine(Assembly());
    }





    IEnumerator Assembly()
    {
        points = new Vector3[parts.Count];
      //  paths  = new Path[parts.Count];
        int perfomanceCount = 0;

       
        var _mesh = _meshFlter.mesh;

        int offset=_mesh.vertexCount/partsCount;
        for (var i = 0; i < parts.Count; i++)
        {
            var part = parts[i];
            part.parent = transform;

            var tempPos = Random.insideUnitSphere;
            tempPos.Scale(_mesh.bounds.size*scale);
            part.position = transform.position + tempPos;

         
            int errors = -1;

            bool    repeat;
            Vector3 pos = Vector3.zero;
          

            do
            {
                ++errors;

                int triangle0 =Utils.RandomArrayNumber(_mesh.triangles);

                var v0        = _mesh.vertices[_mesh.triangles[triangle0]];

                var v1 = _mesh.vertices[_mesh.triangles[triangle0 +1]];

                pos    = Vector3.Lerp(v0, v1, Random.value);
                repeat = false;


                for (int j = 0; j < i; j++)
                {
                    if (Vector3.Distance(pos, points[j]) < MinOffset)
                    {
                        repeat = true;


                        break;
                    }
                }


                ++perfomanceCount;

                if (perfomanceCount > maxPerfomansPointPerFrame)
                {

                    perfomanceCount = 0;



                    yield return null;
                }
            } while (repeat && errors < 10);




            points[i] = transform.rotation*pos*finalScale + transform.position;
        }


        


        for (var i = 0; i < parts.Count; i++)
        {
            var part = parts[i];
        //    part.DOJump(points[i], jumpPower, JupNums, duration);
        }
    }





    public           float MinOffset                 = 0.5f;
    [SerializeField] float jumpPower                 = 5;
    [SerializeField] int   JupNums                   = 3;
    [SerializeField] int   maxPerfomansPointPerFrame = 30;
    [SerializeField] float scale=1/4;
}