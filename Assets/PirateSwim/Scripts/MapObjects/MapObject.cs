using JetBrains.Annotations;
using MoreMountains.Tools;
using PirateSwim.Scripts.ShipScripts;
using TickedPriorityQueue;
using UnityEngine;

namespace PirateSwim
{
    public class MapObject :MMPoolableObject
    {
        Rigidbody _rigid;

      [NotNull]  public new Rigidbody rigidbody
        {
            get
            {
                if (!_rigid)
                {
                    _rigid = gameObject.GetOrAdd<Rigidbody>();
                    _rigid.useGravity = false;
                }

                return _rigid;
            }
            set { _rigid = value; }
        }

        [HideInInspector] public new Collider collider;
        public                       Group    group;

        protected Ship _ship;

      
        UnityTickedQueue  _steeringQueue;
        TickedObject      tickedObject;
        public static int _maxQueueProcessedPerUpdate = 50;

       protected  float deltaTime=0.3f;

        protected override void OnEnable()
        {
            base.OnEnable();
            if (!_ship)
            {
                _ship = Ship.Main;
            }

            _steeringQueue.Add(tickedObject);
        }

        protected override void OnDisable()
        {
            _steeringQueue.Remove(tickedObject);
            base.OnDisable();
        }


        public virtual void ManualUpdate(object userData)
        {
            if (Camera.main != null && transform.position.z < Camera.main.transform.position.z - 25f)
            {
                Destroy();
            }
        }

        void Awake()
        {
            _ship = Ship.Main;
            collider = gameObject.GetComponentInGOAndChildren<Collider>();
           
                tickedObject = new TickedObject(ManualUpdate);
           


            _steeringQueue = UnityTickedQueue.GetInstance(gameObject.layer.ToString());
            tickedObject.TickLength = deltaTime;
            _steeringQueue.MaxProcessedPerUpdate = _maxQueueProcessedPerUpdate;
        }

        protected virtual void Start()
        {
            rigidbody = gameObject.SafeGetComponentInGOAndChildren<Rigidbody>();
          
          
        }

        private void DeQueue()
        {
            if (_steeringQueue != null)
            {
                _steeringQueue.Remove(tickedObject);
            }
        }

        public override void Destroy()
        {
            DeQueue();
            base.Destroy();
        }
    }
}