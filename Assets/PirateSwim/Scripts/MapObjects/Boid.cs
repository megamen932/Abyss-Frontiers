using UnityEngine;

namespace PirateSwim
{
   public class Boid : MonoBehaviour
{
    public float turnSpeed = 10;
    public int maxSpeed = 15;
    public float cohesionRadius = 70;
    public int maxBoids = 10;
    public float separationDistance = 5;
    public float cohesionCoefficient = 5;
    public float alignmentCoefficient = 4;
    public float separationCoefficient = 2;
    public float tick = 2;
    public Transform model;
    public LayerMask boidsLayer;

    [HideInInspector] public Vector3 velocity;

    private Collider[] boids;
    private Vector3 cohesion;
    private Vector3 separation;
    private int separationCount;
    private Vector3 alignment;

    private Boid b;
    private Vector3 vector;
    private int i;
    public int maxRadius = 25;

    void Awake()
    {
        velocity = Random.onUnitSphere*maxSpeed;
    }

    private void Start()
    {
        model = transform;
        InvokeRepeating("CalculateVelocity", Random.value * tick, tick);
        InvokeRepeating("UpdateRotation", Random.value, tick);
    }

    void CalculateVelocity()
    {
       
        boids = Physics.OverlapSphere(transform.position, cohesionRadius, boidsLayer.value);
        if (boids.Length < 2) return;

        velocity = Vector3.zero;
        cohesion = Vector3.zero;
        separation = Vector3.zero;
        separationCount = 0;
        alignment = Vector3.zero;
        
        for (i = 0; i < boids.Length && i < maxBoids; i++)
        {
            b = boids[i].GetComponent<Boid>();
            if(!b){continue;}
            cohesion += b.transform.localPosition;
            alignment += b.velocity;
            vector = transform.position - b.transform.position;
            if (vector.sqrMagnitude > 0 && vector.sqrMagnitude < separationDistance * separationDistance)
            {
                separation += vector / vector.sqrMagnitude;
                separationCount++;
            }
        }

        cohesion = cohesion / (boids.Length > maxBoids ? maxBoids : boids.Length);
        cohesion = Vector3.ClampMagnitude(cohesion - transform.localPosition, maxSpeed);
        cohesion *= cohesionCoefficient;
        if (separationCount > 0)
        {
            separation = separation / separationCount;
            separation = Vector3.ClampMagnitude(separation, maxSpeed);
            separation *= separationCoefficient;
        }
        alignment = alignment / (boids.Length > maxBoids ? maxBoids : boids.Length);
        alignment = Vector3.ClampMagnitude(alignment, maxSpeed);
        alignment *= alignmentCoefficient;
        var z = velocity.z;
        //velocity.z = 0;
        velocity.z = Mathf.Abs(z);
        velocity = Vector3.ClampMagnitude(cohesion + separation + alignment, maxSpeed);
        
    }

   
    void UpdateRotation()
    {
      
       
        if (velocity != Vector3.zero && model.forward != velocity.normalized)
        {
            model.forward = Vector3.RotateTowards(model.forward, velocity, Time.fixedDeltaTime*turnSpeed*Mathf.Deg2Rad, tick);
        }
    }

    void Stop()
    {
      //  StopAllCoroutines();
        CancelInvoke("CalculateVelocity");
        CancelInvoke("UpdateRotation");
    }

    void FixedUpdate()
    {
        if (transform.localPosition.sqrMagnitude > maxRadius * maxRadius)
        {
            velocity += -transform.localPosition / maxRadius;
        }
        transform.localPosition += velocity * Time.deltaTime;
    }
}
}