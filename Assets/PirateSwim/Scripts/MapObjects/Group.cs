using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace PirateSwim
{
    public class Group : MonoBehaviour
    {
        public static int groupCount;
        public        int nextName => groupCount % 6;
        public        int countOfMembersTotal = -1;

        public int countOfMembersCurrent => members.Count;


        public int   countOfMemebersFree => countOfMembersTotal - countOfMembersCurrent;
        public float ShareGravityForSecs = -.5f;

        public        Vector3         center;
        public        Quaternion      rotation;
        public        Vector3         FlowCurrent = Vector3.zero;
        public        List<MapObject> members     = new List<MapObject>();
        public static List<Group>     groups      = new List<Group>();
        bool                          Inited      = false;
        [SerializeField] GroupStates  State       = GroupStates.Dance;
        Transform                     _transformGroupParent;
        [Header("Boid")] public float   turnSpeed             = .1f;
        public                  int   maxSpeed              = 50;
        public                  float cohesionRadius        = 50;
        public                  int   maxBoids              = 3;
        public                  float separationDistance    = 4000;
        public                  float cohesionCoefficient   = 3;
        public                  float alignmentCoefficient  = 10;
        public                  float separationCoefficient = 5;
        public                  float tick                  = 2;
        [SerializeField] float maxRadius = 50;
      

        public float randomMultiplier       = 1;
        public float randomMultiplierOffset = 2f;


        public  bool    autoInit = false;
        [SerializeField] float recalcCenterTime=3;
      





        public Group()
        {
            groupCount = groups.Count;
        }





        void Start()
        {
            if (autoInit)
            {
                Init(countOfMembersTotal, State);
            }
        }





        public void Init(int CountOMembers, GroupStates p_states = GroupStates.Random, float duration = -1)
        {
            while  (p_states == GroupStates.Random)
            {
                Array         values    = Enum.GetValues(typeof(GroupStates));
                System.Random random    = new System.Random();
                GroupStates   randomBar = (GroupStates) values.GetValue(random.Next(values.Length));
                p_states = randomBar;
            }


            State               = p_states;
            countOfMembersTotal = CountOMembers;

            rotation         =  Random.rotation;
            randomMultiplier *= Random.Range(0, randomMultiplierOffset);


            if (!Inited)
            {
                groups.Add(this);
                groupCount++;
            }


            _transformGroupParent = transform;
            gameObject.name       = "BoidChildGroup(" + nextName + "/" + groupCount + ")";

            //  _transformBoidParent.parent = transform;
            Inited = true;

            InvokeRepeating(nameof(CalcCenter), 0, recalcCenterTime);

            if (countOfMemebersFree == 0)
            {
                if (_transformGroupParent != null)
                {
                    _transformGroupParent.position = center;
                }
            }


            if (duration > 0)
            {
                Invoke(nameof(StopGroup), duration);
            }


            if (State == GroupStates.Boid)
            {
                foreach (var member in members)
                {
                  var boid= member.gameObject.GetOrAdd<Boid>();
                    boid.maxRadius        = (int)maxRadius;
                    boid.maxSpeed         = (int) (maxSpeed * Random.Range(0.8f, 1.2f));
                    boid.separationDistance=separationDistance;
                    boid.alignmentCoefficient = alignmentCoefficient;
                    boid.cohesionCoefficient= cohesionCoefficient;
                    boid.separationCoefficient= separationCoefficient;
                    boid.cohesionRadius = cohesionRadius;
                    boid.tick= tick;
                    boid.boidsLayer.value = 1 << gameObject.layer;
                    boid.maxBoids = maxBoids;
                    boid.model = boid.transform.GetChild(0);
                    boid.turnSpeed = turnSpeed;

                }
            }
        }





        void CalcCenter()=> center = Utils.CalcCenterList(members);
    


    void StopGroup()
        {
            for (int i = 0; i < transform.childCount; ++i)
            {
                transform.GetChild(0).parent = transform.parent;
            }


            KillDestroy();
        }





        public void RandomInit(GroupStates p_states = GroupStates.Random)
        {
            while  (p_states == GroupStates.Random)
            {
                Array         values    = Enum.GetValues(typeof(GroupStates));
                p_states = (GroupStates) values.GetValue( Random.Range(0, values.Length) );
            }


           countOfMembersTotal = Random.Range(3, 15);
           
            Init( countOfMembersCurrent, p_states);

        }





        public enum GroupStates
        {
            Together,
       
            Rotating,
            Dance,
            Boid,
           
            Cohesion,
            Random,
            Cohesion2
        };





        void FixedUpdate()
        {
            if (!Inited)
            {
                return;
            }


            while  (State == GroupStates.Random)
            {
                Array values = Enum.GetValues(typeof(GroupStates));
                State = (GroupStates) values.GetValue( Random.Range(0, values.Length) );
            }


            members = members.FindAll((m) => m != null && m.isActiveAndEnabled);
            if (countOfMembersCurrent == 0)
            {
                KillDestroy();
            }


         


       
            Vector3 position;

            var coef =   randomMultiplier * cohesionCoefficient;


            switch (State)
            {
                case GroupStates.Cohesion:


                    for (var i = 1; i < members.Count; i++)
                    {
                        var member     = members[i];
                        var prevMember = members[i - 1];

                        // var localCollisionPos = transform.InverseTransformPoint(contacts[0].point);


                        if (member.rigidbody == null)
                        {
                            member.rigidbody            = member.GetOrAdd<Rigidbody>();
                            member.rigidbody.useGravity = prevMember.rigidbody.useGravity;
                        }


                        var dir   = (prevMember.transform.position - member.transform.position) ;
                        var force =  (dir.sqrMagnitude - separationCoefficient) * dir.normalized * coef ;


                        member.rigidbody.AddForce(force, ForceMode.Force);
                    }


                    break;
                case GroupStates.Cohesion2:


                    for (var i = 0; i < members.Count; i++)
                    {
                        var member = members[i];


                        // var localCollisionPos = transform.InverseTransformPoint(contacts[0].point);


                        if (member.rigidbody == null)
                        {
                            member.rigidbody = member.GetOrAdd<Rigidbody>();
                        }


                        var dir   = (center - member.transform.position) ;
                        var force =  (dir.sqrMagnitude-separationCoefficient ) * dir.normalized * coef ;


                        member.rigidbody.AddForce(force, ForceMode.Force);
                    }


                    break;


                case GroupStates.Together:


                    foreach (var member in members)
                    {
                        position =  member.transform.position;
                        position += Mathf.Sin(Time.time)*(center - FlowCurrent - position) * Time.deltaTime;


                        var transformPosition = Vector3.Slerp(member.transform.position, position,
                                                              Vector3.Distance(position, center) *
                                                              Vector3.Distance(position, center) * cohesionCoefficient /
                                                              Time.deltaTime);


                        MoveTo(member, transformPosition);
                    }


                  
            


                    break;
                case GroupStates.Rotating:


                    foreach (var member in members)
                    {
                        var angle=  turnSpeed*Mathf.Sin(Time.time * Mathf.PI) * cohesionCoefficient +
                                4 * Mathf.Cos(Time.time / alignmentCoefficient) /
                                ((member.transform.position - center).sqrMagnitude + 0.1f);
                        member.transform.RotateAround(center, rotation * Vector3.one,angle);
                    }


                    break;
             


                
                case GroupStates.Boid:
                    
                    break;
            }


            // _transformBoidParent.position = center;
        }





        void MoveTo(MapObject member, Vector3 transformPosition)
        {
            if (member.rigidbody != null)
            {
                member.rigidbody.MovePosition(transformPosition);
            } else
            {
                member.transform.position = transformPosition;
            }
        }








        void KillDestroy()
        {
            groups.Remove(this);
            Destroy(gameObject);
        }





        public bool AddMember(MapObject member)
        {
            if (!Inited)
            {
                RandomInit();
            }


            if (countOfMemebersFree <= 0 && !member && !member.rigidbody)
            {
                return false;
            }


            //   member.deathGroupAction =  delegate { Remove(member); };
            //    member.OnDie            += member.deathGroupAction;

            member.group = this;
            members.Add(member);
           CalcCenter();


            if (countOfMemebersFree == 0)
            {
                if (_transformGroupParent != null)
                {
                    _transformGroupParent.position = center;
                }
            }


            return true;
        }





        public void Remove(MapObject member)
        {
            member.group = null;

            //    member.OnDie -= member.deathGroupAction;
            members.Remove(member);
        }
    }
}