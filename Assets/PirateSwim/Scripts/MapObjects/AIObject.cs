using System;
using JetBrains.Annotations;
using Sirenix.Serialization;
using UnityEngine;
using UnityEngine.Events;

namespace PirateSwim.Scripts.MapObjects
{
    [RequireComponent(typeof(Rigidbody))]
    public abstract class AIObject : TickedMonoBehaviour
    {
    #region Fields

        [NotNull] public new Rigidbody       rigidbody;
        [NotNull]            EvilManagerBase evil;

    #region Private serialized fields

    #endregion

    #endregion

    #region Properties

        public float Evilness
        {
            get => evil.Evilness;
            set => evil.Evilness = value;
        }


        public Vector3 velocity
        {
            get => rigidbody.velocity;
            set => rigidbody.velocity = value;
        }

        public Vector3 angularVelocity
        {
            get => rigidbody.angularVelocity;
            set => rigidbody.angularVelocity = value;
        }

        public float mass
        {
            get => rigidbody.mass;
            set => rigidbody.mass = value;
        }

        /// <summary>
        /// return true if in same Deep as Main Sheep
        /// </summary>
        public bool InDeep => CurDeep == evil.CurDeep;

        /// <summary>
        /// returns Deep where objects locates
        /// </summary>
        public DeepManager CurDeep => evil.GetDeep(transform.position);

    #endregion


    #region Methods

    #region Start / Init Methods

        protected override void OnAwake()
        {
            rigidbody = GetComponent<Rigidbody>();
            evil = EvilManager.Instance;
        }

    #endregion

        public void MovePosition(Vector3 position)
        {
            rigidbody.MovePosition(position);
        }

        public void MoveRotation(Quaternion rot)
        {
            rigidbody.MoveRotation(rot);
        }

    #endregion
    }
}