using UnityEngine;

namespace PirateSwim
{
    public class TerrainMover : MonoBehaviour
    {
        public Transform target;
       // public VirtualJoystick.Joystick  joy;
        public float     rotateSpeed  = 2;
        public float     wantedRadius = 10;
        public float     invert       = 1;
        public float     RadiusSpeed  = 1f;
        public Vector3   startPos;
        public bool      changeGravity = true;
        public bool      useWorld      = true;
        public Vector2 input;
        void Start()
        {
            startPos = transform.position;
        }

        void FixedUpdate()
        {
            input =new Vector2(Input.GetAxis("RotateHorizontal") ,Input.GetAxis("RotateVertical") ) ;
            
            if (useWorld)
            {
                transform.RotateAround(target.position, target.forward,
                    (input.x) * rotateSpeed);
                transform.RotateAround(target.position, target.right,
                    (input.y) * RadiusSpeed);
            }
            else
            {
                transform.RotateAround(target.position, Vector3.forward,
                    (input.x) * rotateSpeed);
                transform.RotateAround(target.position, Vector3.right,
                    (input.y) * RadiusSpeed);
            }

            if (changeGravity)
            {
                var dir = transform.position - target.position;
                dir.z = 0;
                Physics.gravity = invert * dir.normalized;
            }
        }

        Vector3 SpherePos(float radius, float VerticalAngle, float HorizantalAngle)
        {
            var x = radius * Mathf.Sin(HorizantalAngle) * Mathf.Cos(VerticalAngle);
            var y = radius * Mathf.Sin(HorizantalAngle) * Mathf.Sin(VerticalAngle);
            var z = radius * Mathf.Cos(HorizantalAngle);
            return new Vector3(x, y, z);
        }
    }
}