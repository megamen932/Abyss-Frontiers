﻿using UnityEngine;
using System;
using PirateSwim;

namespace MoreMountains.Tools
{
    public interface IMMObjectBounds
    {
        Bounds ObjBounds { get; }
    }

    public class MMObjectBounds : Behaviour, IMMObjectBounds
    {
        public enum WaysToDetermineBounds
        {
            Collider,
            Collider2D,
            Renderer,
            Undefined
        }

        public WaysToDetermineBounds BoundsBasedOn { get; private set; }


         public Bounds ObjBounds{
            get
            {
                if (!boundsInited)
                {
                    _bounds = GetBounds();
                    boundsInited  = true;
                }


                return _bounds;
            }
        }


        Bounds      _bounds;
        private bool boundsInited = false;





        /// <summary>
        /// When this component is added we define its bounds.
        /// </summary>
        public virtual void Reset()
        {
            DefineBoundsChoice();
            boundsInited = false;
        }





        /// <summary>
        /// Tries to determine automatically what the bounds should be based on.
        /// In this order, it'll keep the last found of these : Collider2D, Collider or Renderer.
        /// If none of these is found, it'll be set as Undefined.
        /// </summary>
        protected virtual void DefineBoundsChoice()
        {
            BoundsBasedOn = WaysToDetermineBounds.Undefined;




            if (gameObject.GetComponentInGOAndChildren<Collider>() != null)
            {
                BoundsBasedOn = WaysToDetermineBounds.Collider;
                return;
            }


            if (gameObject.GetComponentInGOAndChildren<Renderer>() != null)
            {
                BoundsBasedOn = WaysToDetermineBounds.Renderer;
                return;
            }
            
            if (gameObject.GetComponentInGOAndChildren<Collider2D>() != null)
            {
                BoundsBasedOn = WaysToDetermineBounds.Collider2D;
            }
        }





        /// <summary>
        /// Returns the bounds of the object, based on what has been defined
        /// </summary>
        private  Bounds GetBounds()
        {
            switch (BoundsBasedOn)
            {
                case WaysToDetermineBounds.Renderer:


                {
                    if (gameObject.GetComponentInGOAndChildren<Renderer>() == null)
                    {
                        throw new Exception("The PoolableObject " + gameObject.name +
                                            " is set as having Renderer based bounds but no Renderer component can be found.");
                    }


                    return gameObject.GetComponentInGOAndChildren<Renderer>().bounds;
                }
                case WaysToDetermineBounds.Collider:


                {
                    var col = gameObject.GetComponentInGOAndChildren<Collider>();


                    if (col == null)
                    {
                        throw new Exception("The PoolableObject " + gameObject.name +
                                            " is set as having Collider based bounds but no Collider component can be found.");
                    }


                    return col.bounds;
                }
                case WaysToDetermineBounds.Collider2D:


                {
                    if (gameObject.GetComponentInGOAndChildren<Collider2D>() == null)
                    {
                        throw new Exception("The PoolableObject " + gameObject.name +
                                            " is set as having Collider2D based bounds but no Collider2D component can be found.");
                    }


                    return gameObject.GetComponentInGOAndChildren<Collider2D>().bounds;
                }
                default:


                    throw new NotImplementedException("No other ways to deterime collider");


                    return new Bounds(Vector3.zero, Vector3.zero);
            }
        }
    }
}