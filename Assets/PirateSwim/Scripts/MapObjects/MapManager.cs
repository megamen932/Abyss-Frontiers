using UnityEngine;

namespace PirateSwim.Scripts.MapObjects
{
    public class MapManager : MonoBehaviour
    {
        public static MapManager instance;
      

      //  MapObject[] _mapObject;
       // List<MapObject> _mapObjectNew;
       
        [SerializeField] float deltaTime=0.5f;
        [SerializeField] float FindNewTime=1f;

        public int maxCachedObjects = 500;
        public int _maxQueueProcessedPerUpdate = 25;
      

        void Start()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(this);
                
            } else if(instance!=this)
            {
                Destroy(this);
                return;
            }


            _Queue = UnityTickedQueue.GetInstance(QueueName);
            _Queue.MaxProcessedPerUpdate = _maxQueueProcessedPerUpdate;
    
         //   Queue.Add(TickedObject);
        //    Queue.MaxProcessedPerUpdate = _maxQueueProcessedPerUpdate;
         //   _mapObjectNew=new List<MapObject>(maxCachedObjects);
            
        
            InvokeRepeating("UpdateAll",deltaTime,deltaTime);
        }

        public string QueueName = "MapObjects";
      
        UnityTickedQueue _Queue;


        public void Add(MapObject mapObject)
        {
           
          // _mapObjectNew.Add(mapObject);
         //   var TickedObject = new TickedObject(mapObject.ManualUpdate);
         //   TickedObject.TickLength = deltaTime;
          //  _Queue.Add(TickedObject);
       
        }

        public void Remove(MapObject mapObject)
        {
        //    _mapObjectNew.Remove(mapObject);
      
       //    _Queue.Remove(mapObject)
         //   base.OnDisable();
        
        }
    }
}