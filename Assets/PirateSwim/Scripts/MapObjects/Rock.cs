using System;
using DG.Tweening;
using ProceduralToolkit;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace PirateSwim
{
    public class RockSettings : ScriptableObject
    {
        float damage = 10f;

      

        Quaternion rotation;

        float speedOfRotating = 15f;


        public bool useGravity = true;

        public Vector3  gravity;
        public Material mat;
        public bool     changeAlpha       = false;
        float           InvisibleDistance = 600;
        float           VisibleDistance   = 50;


        float startTime;

       float chanceORotation = .5f;

   
        float FollowPlayerForce = .08f;

        float denisty = 10f;
   

        public float MaxSpeed = 30;

        bool DebugOffAnyMovement = false;
        bool changeMettalic      = true;
        int  predictTime         = 4;



        [HideInInspector] bool isRotating             = false;
        [HideInInspector] bool isRotatingTowardPlayer = false;
        [HideInInspector] bool isAimToPlayer          = false;
        
        
    }

    public class Rock : MapObject
    {
        float damage = 10f;

        //  public float DamagingTime = 0.3f;

        //float chanceOfRotating = 100/collider.bounds.size.Magnitude;

        Quaternion rotation;

        float speedOfRotating = 15f;

        //    public bool damaginTimerTicked = false;
        public bool useGravity = true;

        public Vector3  gravity;
        public Material mat;
        public bool     changeAlpha       = false;
        float           InvisibleDistance = 600;
        float           VisibleDistance   = 50;


        float startTime;

        [SerializeField] float chanceORotation   = .5f;
//        float                  chanceOfAiming    = .8f;
        float                  FollowPlayerForce = .08f;

        float denisty = 10f;
        //        bool                   Cohesion                  = true;
        //      public float           ChanseOfCollisionCohesion = 0.78f;
        //     float                  CohesionDuration          = -1;


        public float MaxSpeed = 30;

        bool DebugOffAnyMovement = false;
        bool changeMettalic      = true;
        int  predictTime         = 4;

     

        [HideInInspector] bool isRotating             = false;
        [HideInInspector] bool isRotatingTowardPlayer = false;
        [HideInInspector] bool isAimToPlayer          = false;

        public float Damage
        {
            get { return damage; }
            private  set { damage = value; }
        }


        MeshRenderer _meshRenderer  ;

        protected override void Start()
        {
            base.Start();


            rigidbody.useGravity = false;


            _meshRenderer = gameObject.GetComponentInGOAndChildren<MeshRenderer>();

            mat = _meshRenderer.material;
            //    mat.SetOverrideTag("RenderType", "Fade");

            if (DebugOffAnyMovement)
            {
                rigidbody.isKinematic = true;
            }

            if (enabled)
            {
                OnEnable();
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            if (!rigidbody | !mat)
            {
                Start();
            }


            rigidbody.mass       = denisty * ObjBounds.size.magnitude;


            // isRotating = Random.value < chanceORotation && group == null;


            //    AimToPlayer = Random.value < _ship.deepLevelNorm * chanceOfAiming;

            propertyBlock = new MaterialPropertyBlock();
            propertyBlock.SetFloat("_StartTime", Time.timeSinceLevelLoad);
          
            _isBoundsRotateInited = false;
            if (_meshRenderer.isVisible)
            {
                propertyBlock.SetFloat("_StartTime", Time.timeSinceLevelLoad+InvisibleTime);
            }

            _meshRenderer.SetPropertyBlock(propertyBlock);
            //mat.SetFloat("_StartTime", Time.timeSinceLevelLoad);
        }


        public override void Destroy()
        {
            base.Destroy();
        }

        MaterialPropertyBlock propertyBlock;

        public override void ManualUpdate(object userData)
        {
            base.ManualUpdate(userData);


            rigidbody.velocity = Vector3.Lerp(rigidbody.velocity, Vector3.ClampMagnitude( rigidbody.velocity, MaxSpeed), deltaTime);

            if (DebugOffAnyMovement)
            {
                return;
            }

            if (mat != null)
            {
                if (changeAlpha)
                {
                    var mat_color = mat.color;


                    mat_color.a = Mathf.Clamp(1 - (transform.position.z - (Camera.main.transform.position.z + VisibleDistance)) / InvisibleDistance, 0, 1);


                    mat.color = mat_color;
                }


                if (changeMettalic)
                {
                    propertyBlock.SetFloat("_Metallic", _ship.deepLevelNorm);
                    _meshRenderer.SetPropertyBlock(propertyBlock);
                }
            }


            if (useGravity)
            {
                rigidbody.AddForce(gravity, ForceMode.Acceleration);
            }


            if (isAimToPlayer)
            {
                AimToPlayer();
            }


            if (isRotatingTowardPlayer)
            {
              
            } else
            {
                if (isRotating)
                {
                    rigidbody.DORotate(rotation * transform.forward * speedOfRotating, deltaTime, RotateMode.LocalAxisAdd);
                }
            }
        }

        void FixedUpdate()
        {
            if (isRotatingTowardPlayer)
            {
                RotateTowardsPlayer();
            }
        }

        void AimToPlayer()
        {
            var dist = _ship.ShipPosAtTime(predictTime) - transform.position;

            rigidbody.AddForce(dist * FollowPlayerForce, ForceMode.Force);
        }

        // float rotatingSpeed;
        bool _isBoundsRotateInited = false;

        [ShowInInspector, ReadOnly, SerializeField]
        Vector3 rotatePivot;

        Vector3 pivot  ;
        bool    isAlwaysDrawGizmos   = true;
        bool    isDrawSelectedGizmos = true;
        Vector3 pivotShift;
        [SerializeField] float InvisibleTime=1;

        void OnDrawGizmos()
        {
            if (!isAlwaysDrawGizmos)
            {
                return;
            }

            GizmosDraw();
        }

        void OnDrawGizmosSelected()
        {
            if (!isDrawSelectedGizmos || isAlwaysDrawGizmos)
            {
                return;
            }

            GizmosDraw();
        }

        void GizmosDraw()
        {
            if (_isBoundsRotateInited && isRotatingTowardPlayer)
            {
                Gizmos.DrawLine(transform.position,this.pivot);
             
                var dir          = pivotShift;
                var dirMagnitude = dir.magnitude;
                if (rotatePivot == Vector3.up)
                {
                    Gizmos.color = Color.blue;
                    GizmosE.DrawWireCircleXZ(pivot, Quaternion.LookRotation(pivot, rotatePivot), dirMagnitude);
                } else if (rotatePivot == Vector3.forward)
                {
                    Gizmos.color = Color.red;
                    GizmosE.DrawWireCircleXY(pivot, Quaternion.LookRotation(pivot, rotatePivot), dirMagnitude);
                } else if (rotatePivot == Vector3.right)
                {
                    Gizmos.color = Color.green;
                    GizmosE.DrawWireCircleYZ(pivot, Quaternion.LookRotation(pivot, rotatePivot), dirMagnitude);
                }
            }
        }


        public void RotateTowardsPlayer()
        {
            if (!_isBoundsRotateInited)
            {
                float max    = 0;
                var   extens = ObjBounds.extents;
                if (extens.x > max)
                {
                    max = extens.x;
                    rotatePivot = Vector3.up;
                }

                if (extens.y > max)
                {
                    max = extens.y;
                    rotatePivot = Vector3.forward;
                }

                if (extens.z > max)
                {
                    max = extens.z;
                    rotatePivot = Vector3.right;
                }

                _isBoundsRotateInited = true;
                pivot = rigidbody.ClosestPointOnBounds(_ship.transform.position);
                if (pivot == Vector3.zero)
                {
                    pivot = ObjBounds.ClosestPoint(_ship.transform.position);
                }

                if (pivot == Vector3.zero)
                {
                    pivot = transform.position + ObjBounds.extents.magnitude * _ship.transform.position - transform.position;
                }

                pivotShift =pivot- transform.position;
                

                DOVirtual.DelayedCall(360f / speedOfRotating, () => _isBoundsRotateInited = false , false);
            }


            RotateAround(pivot, rotatePivot, speedOfRotating*deltaTime);
        }

        public void RotateAround(Vector3 point, Vector3 axis, float angle)
        {
            Vector3 vector3 = Quaternion.AngleAxis(angle, axis) * (rigidbody.position - point);
            
            rigidbody.MovePosition(point + vector3);
            rigidbody.MoveRotation(Quaternion.Euler(axis * angle * ((float) Mathf.PI / 180f)));
        }

        public void Init( Quaternion groupRot, Vector3 p_gravity, bool p_forceUseGravity)
        {
            rotation   = groupRot;
            gravity    = p_gravity;
            useGravity = p_forceUseGravity;
            _isBoundsRotateInited = false;
        }
    }
}