﻿using PirateSwim.Scripts.ShipScripts;
using UnityEngine;

public class BackGround : MonoBehaviour
{
	public Ship ship;

	public float YScrollSpeed=0.5f;

	public float ZOffset = 20f;
	// Use this for initialization
	void Start ()
	{
		ship = Ship.Main;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		var transformPosition = ship.transform.position + Vector3.forward * (ship.MinZObstaclesOffset+ZOffset);
		transformPosition.y = (ship.deepLevel - ship.startPos.y) * YScrollSpeed;
		transform.position = transformPosition;
	}
}
