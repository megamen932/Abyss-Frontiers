﻿using System;
using JetBrains.Annotations;
using PirateSwim;
using PirateSwim.Scripts.ShipScripts;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

public class DeepManager : TickedMonoBehaviour
{
    [Serializable]
    public struct DeepBounds
    {      
 
     

        [SerializeField, Sirenix.OdinInspector.PropertyRange(0, nameof(end)), ValidateInput(nameof(ValidateMaxDeep))]
        public float start ;

        [SerializeField, Sirenix.OdinInspector.PropertyRange(nameof(start), nameof(MaxDist)), ValidateInput(nameof(ValidateMaxDeep))]
        public float end   ;
      
      public Vector2 Vector2=>new Vector2(x,y);
      float Size => end - start;
    

      public float x => start;
      public float y => end;
      public float min => start;
      public float max => end;
      

      public static float MaxDist => Ship.Main.MaxDist;
      public bool this[float deep] => deep >= start && deep <= end;


        public DeepBounds(Vector2 bounds) :this(bounds.x, bounds.y)
        {
         
        }

        public DeepBounds(float start, float end) 
        {
            this.start = Mathf.Min(start, end);
            this.end = Mathf.Max(start, end);
        }

   
        static bool ValidateMaxDeep(float x)
        {
        

        
            return x <= MaxDist;
        }
    }

    #region Fields

    [SerializeField,OnValueChanged(nameof(ChangeName))] DeepBounds _bounds = new DeepBounds(0, 2000);

    [TabGroup("Evillnes", "Calming"),ValidateInput(nameof(NotZero), nameof(maxCalmingTime) + " must be not equal to 0.", InfoMessageType.Error)] [SerializeField]
    float maxCalmingTime = 100;

    [NotNull] Ship _ship;

    [ReadOnly, TabGroup("Debug"), ShowInInspector]
    float _irritation = 0;

    [ReadOnly, TabGroup("Debug") , ShowInInspector]
    public float _calmingTime = 0;

    [ReadOnly, TabGroup("Debug"), ShowInInspector]
    public float _timeInDeep = 0;

    [ReadOnly, TabGroup("Debug"), ShowInInspector]
    private float _calming;


    [SerializeField, TabGroup("Evillnes", "Irritation")]
    float maxEvilness = 2;

    [SerializeField, TabGroup("Evillnes", "Irritation")]
    float minEvilness = -1;

    [SerializeField, TabGroup("Evillnes", "Rage"), PropertyRange(nameof(minEvilness), nameof(maxEvilness))]
    float rageStart = 1;

    [SerializeField, TabGroup("Evillnes", "Rage")]
    float maxRageTime = 30f;

    [SerializeField, TabGroup("Evillnes", "Rage")]
    float rageCalmingTime = 15f;


    [SerializeField, HideIf(nameof(IrritationTimeVisibleFunction))]
    float MaxIrritationTime;

    [SerializeField, TabGroup("Evillnes", "Irritation")]
    bool           autoMaxIrritationTime = true;

    [SerializeField, TabGroup("Evillnes", "Irritation")]
    AnimationCurve irritationCurve       = AnimationCurve.EaseInOut(0, 0.1f, 1, 1);

    [SerializeField, TabGroup("Evillnes", "Calming")]
    AnimationCurve calmingCurve          = AnimationCurve.EaseInOut(0, 0.1f, 1, 1);

     [SerializeField, TabGroup("Debug")] bool                   _canRage           = true;
    [SerializeField, TabGroup("Evillnes", "Rage")] float maxEvillStepToRage = 0.1f;


    [HideInInspector] private SwitchEvent<DeepManager> shipInDeepEvent ;
    

    [ReadOnly, TabGroup("Debug"), ShowInInspector]
    bool _shipWasInDeep = false;


    [HideInInspector] private   SwitchEvent<DeepManager> inRageEvent;
    [ TabGroup("Debug"), ShowInInspector] bool                     inRage;

    #endregion

    #region Propetries

    public bool InRage
    {
        get => inRage;
        private set
        {
            if (inRage == value)
            {
                return ;
            }

            InRage = value;
            inRageEvent.Changed(InRage);
        }
    }


    public DeepBounds Bounds
    {
        get => _bounds;
        private set => _bounds = value;
    }


    #region ScriptPropetris

    public bool InDeep([NotNull] Transform transform)
    {
        return InDeep(_ship.DeepYToReal(transform.position.y));
    }

    public bool InDeep(float deep)
    {
        return _bounds[deep];
    }

    public bool ShipInDeep() => InDeep(_ship.deepLevel);

    public float TimeInDeep
    {
        get => _timeInDeep;
        set => _timeInDeep = Mathf.Clamp(value, 0, MaxIrritationTime);
    }

    public float CalmingTime
    {
        get => _calmingTime;
        set => _calmingTime =  Mathf.Clamp(value, 0, maxCalmingTime);
    }

    public float Evilness { get ; set ; }

    public float Irritation
    {
        get => _irritation;
        set => _irritation = Mathf.Clamp(value, minEvilness, maxEvilness);
    }

    public SwitchEvent<DeepManager> ShipInDeepEvent
    {
        get { return shipInDeepEvent; }
        set { shipInDeepEvent = value; }
    }

    public SwitchEvent<DeepManager> InRageEvent
    {
        get { return inRageEvent; }
        set { inRageEvent = value; }
    }

    #endregion

    #endregion

    #region Methods

    public void Reset()
    {
        targetTickTime = 1f;
        queueName = "Managers";
        maxCalmingTime = 100;
    }

    protected override void OnAwake()
    {
        _ship = Ship.Main;
        shipInDeepEvent = new SwitchEvent<DeepManager>(this);

        if (autoMaxIrritationTime)
        {
            MaxIrritationTime = Mathf.Abs(_bounds.y - _bounds.x) / _ship.SwipingSideSpeed.y;
        }
    }

    protected override void DoUpdate(float deltaTime)
    {
        var curValue = ShipInDeep();
        var changed  = _shipWasInDeep == curValue;
        if (curValue)
        {
            if (changed)
            {
                shipInDeepEvent.Activate();
            }

            TimeInDeep += deltaTime;
            Irritation = irritationCurve.Evaluate(TimeInDeep / MaxIrritationTime);
        } else
        {
            if (changed)
            {
                CalmingTime = 0;
                shipInDeepEvent.Deactivate();
            }

            CalmingTime += deltaTime;
            _calming = calmingCurve.Evaluate(CalmingTime / maxCalmingTime);
        }

        var prevEvillness = Evilness;
        Evilness = Irritation - _calming;

        if (Evilness > rageStart || Evilness - prevEvillness > maxEvillStepToRage)
        {
            TryToRage();
        }

        void TryToRage()
        {
            if (InRage)
            {
                return;
            }

            if (_canRage)
            {
                InRage = true;

                _canRage = false;
                Utils.Invoke(maxRageTime, () =>
                {
                    InRage = false;
                    Utils.Invoke(rageCalmingTime, () => _canRage = true);
                });
            }
        }


        _shipWasInDeep = curValue;
    }

    void OnDrawGizmos()
    {
        if (EvilManager.Instance.DrawGizmos)
        {
            if (SceneView.lastActiveSceneView != null)
            {
                var pivot = SceneView.lastActiveSceneView.pivot;
                var size  = (Bounds.y-Bounds.x);
                Handles.DrawWireCube(new Vector3(pivot.x,size*.5f,
                    pivot.z),new Vector3(200,size,200));
            }
        }
    }

    #endregion

    #region DebugAndNonGameFunctions

    private bool IrritationTimeVisibleFunction()
    {
        return autoMaxIrritationTime && !Application.isPlaying;
    }

    void ChangeName(DeepBounds bounds)
    {
        name = $"DeepManager {(int)bounds.min}-{(int)bounds.max}";
    }

    #endregion
}