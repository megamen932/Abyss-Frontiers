using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using QuickEngine.Common;
using Sirenix.OdinInspector;
using UnityEngine;
using Debug = System.Diagnostics.Debug;

namespace PirateSwim
{
    public class EvilManagerBase : TickedMonoBehaviour
    {
        [ReadOnly]  DeepManager curDeep;
        DeepManager[]                 _deepManagers;
        [SerializeField]bool _drawGizmos;

        #region Public propetries

        
        public DeepManager this[float deep] => _deepManagers.FirstOrDefault(manager => manager.InDeep(deep));

        public bool DrawGizmos
        {
            get => _drawGizmos;
            set => _drawGizmos = value;
        }

        #endregion
        #region Methods

        #region Start / Init

        void Reset()
        {
            targetTickTime = 1f;
            queueName = "Managers";
        }


        protected override void OnAwake()
        {
            _deepManagers = FindObjectsOfType<DeepManager>();
            Debug.Assert(_deepManagers != null, nameof(_deepManagers) + " != null");
            if (_deepManagers.Length > 0)
            {
                foreach (var manager in _deepManagers)
                {
                    manager.ShipInDeepEvent.On += ChangeCurDeep;
                }

                curDeep = _deepManagers[0];
            }
        }

         #endregion

         protected override void DoUpdate(float deltaTime)
        {
        }

        void ChangeCurDeep(DeepManager newDeep)
        {
            curDeep = newDeep;
        }

        #endregion
        

        #region CurDeep Delegating Methods

        // public float Evillness => curDeep.Evilness;
        //  public float Irritation => curDeep.Irritation;
        //   public float Calming => curDeep.Calming;


        public bool InDeep(Transform transform)
        {
            return curDeep.InDeep(transform);
        }

        public bool ShipInDeep()
        {
            return curDeep.ShipInDeep();
        }

        public float TimeInDeep
        {
            get => curDeep.TimeInDeep;
            set => curDeep.TimeInDeep = value;
        }

        public float CalmingTime
        {
            get => curDeep.CalmingTime;
            set => curDeep.CalmingTime = value;
        }

        public float Evilness
        {
            get => curDeep.Evilness;
            set => curDeep.Evilness = value;
        }

        public float Irritation
        {
            get => curDeep.Irritation;
            set => curDeep.Irritation = value;
        }


        public bool InRage => curDeep.InRage;

        public DeepManager.DeepBounds Bounds => curDeep.Bounds;

        public SwitchEvent<DeepManager> ShipInDeepEvent
        {
            get => curDeep.ShipInDeepEvent;
            set => curDeep.ShipInDeepEvent = value;
        }

        public SwitchEvent<DeepManager> InRageEvent
        {
            get => curDeep.InRageEvent;
            set => curDeep.InRageEvent = value;
        }


        public DeepManager CurDeep
        {
            get { return curDeep; }
          private  set { curDeep = value; }
        }

        public DeepManager GetDeep(Vector3 pos) => this[pos.y];

        #endregion

        #region Non Game Functions

        public override string ToString()
        {
            return $"{base.ToString()}, {nameof(curDeep)}: {curDeep}";
        }

        #endregion
    }

    public class EvilManager : Singleton<EvilManagerBase>
    {
    }
}