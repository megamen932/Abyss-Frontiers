using UnityEngine;

namespace PirateSwim
{
    public class PauseManager : MonoBehaviour
    {
        public void Pause()
        {
            Time.timeScale = 0;
        }





        private static PauseManager _instance;

        public static PauseManager Instance{
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<PauseManager>();


                    if (_instance == null)
                    {
                        GameObject singleton = new GameObject("PauseManager");
                        _instance = singleton.AddComponent<PauseManager>();
                        DontDestroyOnLoad(singleton);
                    }
                }


                return _instance;
            }
        }





        void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Debug.Log("[DoozyUI] There cannot be two SceneLoaders active at the same time. Destryoing this one!");
                Destroy(gameObject);


                return;
            }


            _instance = this;
            DontDestroyOnLoad(gameObject);
            Resume();
        }





        public void Resume()
        {
            Time.timeScale = 1;
        }





        public void OnGameEvent(string gameEvent)
        {
            if (gameEvent.Contains("TogglePause"))
            {
                TogglePause();
            }


            if (gameEvent.Contains("Pause"))
            {
                Pause();
            }


            if (gameEvent.Contains("Resume"))
            {
                Resume();
            }
        }





        public void TogglePause()
        {
            Time.timeScale = Time.timeScale == 0 ? 1 : 0;
        }
    }
}