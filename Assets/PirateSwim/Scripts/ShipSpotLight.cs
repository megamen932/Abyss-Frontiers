﻿using System.Collections;
using DG.Tweening;
using UnityEngine;
using VLB;

public class ShipSpotLight : MonoBehaviour
{
    [SerializeField] float errorTime = 30f;
    float                  startIntensity       = 0;
    VolumetricLightBeam    _volumetricLightBeam;
    [SerializeField] float                  minTimeToCrash = 10f;
    bool                   canCrash       = true;
    Light                  spotLight  ;
    [SerializeField] float _durationFallOff = 0.25f;
    [SerializeField] float MaxLightStrength = 1.125f;
    [SerializeField] float _durationFallOn = 0.14f;
    [SerializeField] int _loopsCount = 3;
    [SerializeField] float _durationReturnToNormal = 1.25f;
    [SerializeField] float _maxWaitMultiplier=5;
    [SerializeField] float _minLightStrength = .05f;
    [SerializeField] float _MinStrengthToBeamOn = 0.25f;

    void Start ()
    {
        spotLight = GetComponent<Light>();
        _volumetricLightBeam = GetComponent<VolumetricLightBeam>();
        startIntensity = spotLight.intensity;
        Invoke("DoError", errorTime * Random.Range(1f, _maxWaitMultiplier));
    }

    void OnEnable()
    {
      
        Invoke("DoError",errorTime*Random.Range(1f,_maxWaitMultiplier));
    }

    void OnDisable()
    {
        CancelInvoke("DoError");
    }

   
    void FixedUpdate ()
    {      

        _volumetricLightBeam.enabled = spotLight.intensity>_MinStrengthToBeamOn*startIntensity;
    }

    void DoError()
    {
        StartCoroutine("Crash");
    }

    IEnumerator Crash()
    {
        if (!canCrash)
        {
            yield break;
        }

        canCrash = false;
        Sequence mySequence = DOTween.Sequence();
        var _loops = _loopsCount * Random.Range(1, 3);
        mySequence.Append(  spotLight.DOIntensity(startIntensity * _minLightStrength, _durationFallOff)).Append(spotLight.DOIntensity(startIntensity*MaxLightStrength, _durationFallOn)).SetLoops(
            _loops).Play();
        yield return new WaitForSeconds( mySequence.Duration());
        
        spotLight.DOIntensity(startIntensity , _durationReturnToNormal);  
        yield return new WaitForSeconds(minTimeToCrash);
        canCrash = true;
        Invoke("DoError", (errorTime+minTimeToCrash) * Random.Range(1f, _maxWaitMultiplier));
    }
}