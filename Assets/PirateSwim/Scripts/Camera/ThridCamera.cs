﻿using System;
using System.Collections;
using Cinemachine;
using PirateSwim.Scripts.ShipScripts;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.PostProcessing;


[ExecuteInEditMode]
public class ThridCamera : MonoBehaviour {










    [SerializeField] Camera main;
    [SerializeField] Camera _camera;
    [SerializeField] bool   ColorSameAsMain;
    [SerializeField] bool   FovSameAsMain;
    [SerializeField] bool   FarClipPlaneToFog=false;
    [SerializeField] bool   NearClipPlaneToMainEnd=false;
    [SerializeField] bool   enableFOG=false;
    CinemachineBrain brain;


    public static ThridCamera[] cameras;
  

    // Start is called before the first frame update
    void Start()
    {
       

        // _camera = GetComponent<Camera>();
        if (!main && Camera.main)
        {
            main = Camera.main;
          
            brain = main.GetComponent<CinemachineBrain>();
            brain.m_CameraActivatedEvent.AddListener(CameraChanges);
        }

        if (enableFOG)
        {
            //  var volume = PostProcessManager.instance.QuickVolume(gameObject.layer, 100f);
            var layer = GetComponent<PostProcessLayer>();
            if (layer != null)
            {
                layer.fog.enabled = true;
            }
        }

        //ChangeClipPlanes();
    }

    void CameraChanges(ICinemachineCamera arg0, ICinemachineCamera arg1)
    {
        CameraChanges();
    }


    void Awake()
    {
     

        if (!_camera)
        {
            _camera = GetComponent<Camera>();
        }

        // ChangeClipPlanes();
    }

  

   


    void ChangeClipPlanes()
    {
        if (_camera != null && Ship.Main)
        {
            if (FarClipPlaneToFog)
            {
                _camera.farClipPlane = RenderSettings.fogEndDistance;
            }

            if (NearClipPlaneToMainEnd)
            {

                _camera.nearClipPlane = main.farClipPlane;
            }
            //main.farClipPlane = _camera.nearClipPlane;


        }
    }

    public void CameraChanges()
    {
        ChangeClipPlanes();
        if (ColorSameAsMain  && main) _camera.backgroundColor = main.backgroundColor;
        if (FovSameAsMain  && main) _camera.fieldOfView = main.fieldOfView;
    }


    public  static void UpdateAllCameras()
    {
        if (cameras == null)
        {
            cameras = FindObjectsOfType<ThridCamera>();
        }

        foreach (var thridCamera in cameras)
        {
            thridCamera.CameraChanges();
        }
    }
}
