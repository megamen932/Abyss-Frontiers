﻿using System;
using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

public class CustomUpdateBehaviourBase : MonoBehaviour
{
    [EnumToggleButtons] public UpdateType updates = UpdateType.Fixed;
    public int skipFrames = 2;

    [Flags]
    public enum UpdateType
    {
        Fixed            =1,
        Update           =1<<2 ,
        Late             =1<<3    ,
        NFrameAfterFixed =1<<4 ,
        All              = Fixed | Update | Late,
    }

    void Update()
    {
        if ((updates & UpdateType.Update) != 0)
        {
            Movement();
        }
    }

    void Movement()
    {
  
      
       
    }

    IEnumerator CustomUpdate()
    {
        for (var i = 0; i < skipFrames; ++i)
        {
            yield return null;
        }

        Movement();
    }

    void LateUpdate()
    {
        if ((updates &  UpdateType.Late) != 0)
        {
            Movement();
        }

       
    }

    void FixedUpdate()
    {
        if ((updates &  UpdateType.Fixed) != 0)
        {
            Movement();
        }

        if ((updates &  UpdateType.NFrameAfterFixed) != 0)
        {
            StartCoroutine(CustomUpdate());
        }
    }
}