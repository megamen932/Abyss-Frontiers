﻿using System.Linq;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

[ExecuteAlways,CreateAssetMenu(fileName = "AutoCamera",menuName="Scene Auto Camera",order = 51)]
public class AutoSceneCamera : ScriptableObject
{
    const  string    CameraActivateAutoCameraV   = "Camera/Activate auto Camera #&v";
    const  string    CameraDeactivateAutoCameraV = "Camera/Deactivate auto Camera #&v";
    const  string    CameraLookToTargetV         = "Camera/Look To Target #v";
    public Transform target;
    public float     Distance = 50;
   public bool             active ;
   [SerializeField] float ForwardDistance=10    ;
   [SerializeField] float MaxSmoothDistance = 300;

    [SerializeField] float MaxForwardDistance = 2000;


    [CanBeNull] static AutoSceneCamera _instance  ;

    [NotNull]
    public static AutoSceneCamera Instance
    {
        get
        {
            if (_instance == null)
            {
                var viewCameras = Resources.FindObjectsOfTypeAll<AutoSceneCamera>();
                _instance = viewCameras.Any() ? viewCameras.FirstOrDefault() : CreateInstance<AutoSceneCamera>();
            }

         
            return _instance;
        }
    }

    bool _inited    ;

    void Awake()
    {
        if (_instance!=null&&_instance != this)
        {
            Debug.Log("Deleting this duplicate instance of AutoCamera");
            DestroyImmediate(this);
        }

        if (target == null)
        {
            var ship = GameObject.FindWithTag("Player");
            if (ship != null)
            {
                // if (Ship.ship == null) Ship.ship = ship;
                target = ship.transform;
            }
        }

        if (!target)
        {
            Debug.Log("Auto Scene Camera Can't find target");
            return;
        }

        _inited = true;
        SceneView.onSceneGUIDelegate += OnSceneFunc;
    }

    void Destroy()
    {
        SceneView.onSceneGUIDelegate -= OnSceneFunc;
        DestroyImmediate(this);
    }


#region ToolsBar

    [MenuItem(CameraActivateAutoCameraV)]
    public static void ActiveTargetSceneView()
    {
        Instance.SetActive(true);
    }

    [MenuItem(CameraDeactivateAutoCameraV)]
    public static void DeactiveTargetSceneView()
    {
        Instance.SetActive(false);
    }

    [MenuItem(CameraActivateAutoCameraV, false), MenuItem(CameraDeactivateAutoCameraV, true)]
    static bool ValidateMenuItems()
    {
        return Instance.active;
    }

#endregion

    void SetActive(bool Active)
    {
        active = Active;
        if (active)
        {
            if (target == null)
            {
                Awake();
            }

            CheckDist(SceneView.lastActiveSceneView);
        }
    }

    [MenuItem(CameraLookToTargetV)]
    public static void Look()
    {
        Instance.LookAt();
    }


    void OnSceneFunc(SceneView view)
    {
        RenderSceneGUI();
        CheckDist(view);

        //  OrbitCameraBehavior(SceneView.currentDrawingSceneView);
    }

    void CheckDist(SceneView view)
    {
        if (active && target)
        {
            var distance = target.position.z - view.pivot.z;
            var dist     = distance > Distance || -distance > MaxForwardDistance;

            //   transform.position = view.pivot;


            if (dist)
            {
                LookAt();
            }
        }
    }

    public void RenderSceneGUI( )
    {
        var style = new GUIStyle {margin = new RectOffset(10, 10, 10, 10) };

        Handles.BeginGUI();
        GUILayout.BeginArea(new Rect(20, 20, 180, 300), style);
        var rect = EditorGUILayout.BeginVertical();
        GUI.Box(rect, GUIContent.none);

        GUILayout.Toggle(active, "Auto Camera");

        EditorGUILayout.EndVertical();
        GUILayout.EndArea();
        Handles.EndGUI();
    }



    [Button("Look")]
    void LookAt()
    {
        if (!target)
        {
            return;
        }

        var view = SceneView.lastActiveSceneView;


        Vector3 predictPosition = view.pivot ;
        predictPosition.z = target.position.z + ForwardDistance;


        var distance = Mathf.Abs(target.position.z - view.pivot.z);
        if (distance <= MaxSmoothDistance)
        {
            view.LookAt(predictPosition);
        } else
        {
            view.pivot = predictPosition;
        }
    }


    private void OrbitCameraBehavior(SceneView view)
    {
        Event current = Event.current;
        view.FixNegativeSize();
        if (Selection.activeGameObject != null)
        {
            var o = Selection.activeGameObject.transform;

            Quaternion l_target = o.rotation;
            Quaternion quaternion1 = Quaternion.AngleAxis((float) (current.delta.y * (3.0 / 1000.0) * 57.2957801818848), l_target * Vector3.right) *
                                     l_target;
            Quaternion quaternion2 = Quaternion.AngleAxis((float) (current.delta.x * (3.0 / 1000.0) * 57.2957801818848), Vector3.up) * quaternion1;
            if (view.size < 0.0)
            {
                view.pivot = view.camera.transform.position;
                view.size = 0.0f;
            }

            view.rotation = quaternion2;
        }
    }
}