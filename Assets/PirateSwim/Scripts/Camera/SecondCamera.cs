﻿using System;
using System.Collections;
using Cinemachine;
using PirateSwim;
using PirateSwim.Scripts.ShipScripts;
using Sirenix.OdinInspector;
using UnityEngine;

public class SecondCamera : MonoBehaviour
{
    public Vector3 velocitySlower;

    public Transform target;

    Vector3 startPos;

    Rigidbody               rigid;
    Quaternion              startRotaion;
    public           bool   UseRotation;
    public    new       Camera camera;
    public           Camera main;
    [SerializeField] bool   isChangeColorSameAsMain;
    CinemachinePOV _pov;

    [SerializeField] CinemachineVirtualCamera _cinemachineVirtualCamera;

    public float liftSpeed = 1f;

    // Start is called before the first frame update
    void Start()
    {
        rigid = target.GetComponent<Rigidbody>();
        startRotaion = transform.rotation;
        if (!main && Camera.main)
        {
            main = Camera.main;
        }

        _pov =  _cinemachineVirtualCamera.GetCinemachineComponent<CinemachinePOV>();

        prevTime = Time.time-Time.fixedDeltaTime;
    }

   

    Vector3 prevTargetPos;

    [Flags]
    public enum UpdateType
    {
        None=0,
        Fixed=1,
        Update=1<<2 ,
        Late =1<<3    ,
        NFrameAfterFixed=1<<4 ,
        All = Fixed | Update | Late,
    }

    [EnumToggleButtons] public UpdateType updates = UpdateType.Late;

    public int skipFrames = 2;

    void Update()
    {
        if ((updates & UpdateType.Update) != 0)
        {
            Movement();
        }
    }

    float prevTime;
    void Movement()
    {
        var target_velocity = (target.position - prevTargetPos)/(Time.time-prevTime);
        if (Utils.IsVectorNaN(target_velocity))
        {
            return;
        }

        prevTargetPos = target.position;
        prevTime = Time.time;
       

        var speed = Utils.Div(target_velocity, velocitySlower) + Vector3.down * liftSpeed;
        transform.Translate(speed * Time.deltaTime);
        if (UseRotation)  transform.rotation = target.rotation * startRotaion;
        if (isChangeColorSameAsMain && camera && main) camera.backgroundColor = main.backgroundColor;
        if (_pov)
        {
            _pov.m_HorizontalAxis.Value += Ship.Main.curVelocity.x*_pov.m_HorizontalAxis.m_MaxSpeed*Time.deltaTime;
            _pov.m_VerticalAxis.Value -= Ship.Main.curVelocity.y * _pov.m_VerticalAxis.m_MaxSpeed * Time.deltaTime;
            
            
        }
    }

    IEnumerator CustomUpdate()
    {
        for (var i = 0; i < skipFrames; ++i)
        {
            yield return null;
        }

        Movement();
    }

    void LateUpdate()
    {
        if ((updates &  UpdateType.Late) != 0)
        {
            Movement();
        }
    }

    void FixedUpdate()
    {
        if ((updates &  UpdateType.Fixed) != 0)
        {
            Movement();
        }

        if ((updates &  UpdateType.NFrameAfterFixed) != 0)
        {
            StartCoroutine(CustomUpdate());
        }
    }
}