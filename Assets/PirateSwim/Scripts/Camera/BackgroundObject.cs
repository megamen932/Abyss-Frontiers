﻿using PirateSwim.Scripts.ShipScripts;
using UnityEngine;
[ExecuteInEditMode]
public class BackgroundObject : MonoBehaviour
{
    Ship ship;

    public float StartDeep;

    public float EndDeep;

    public float ZOffset = 20f;

    public float yBounds = 60;

    public float coefZ = 1;
    public float CalcDeepffset = 300;

    // Use this for initialization
    void Start ()
    {
        ship = Ship.Main;
    }

    void FixedUpdate()
    {
        Movement();
    }

    void Movement ()
    {
        var ShipY = ship.deepLevel;
        if (ShipY+CalcDeepffset < StartDeep )
        {
            return;
        }

        var z                = ship.MinZObstaclesOffset + ZOffset;
        var RelativeDistance = z / Mathf.Tan(Camera.main.fieldOfView * .5f * Mathf.Deg2Rad) * coefZ + yBounds;
        
      
      
        var endPosY          = EndDeep - RelativeDistance ;
        if (ShipY > endPosY + CalcDeepffset)
        {
            return;
        }

        var startPosY = StartDeep + RelativeDistance ;

        var transformPosition = ship.transform.position + Vector3.forward * (z);

        var t = Mathf.Clamp01((ShipY - startPosY) / (endPosY - startPosY));

        var Y = Mathf.Lerp( startPosY, endPosY, t );


        transformPosition.y =  ship.DeepYToReal( Y);

        transform.position = transformPosition;
    }
}