﻿using UnityEngine;


public class ChangeCameraCullingMask : MonoBehaviour
{
    [SerializeField] LayerMask     layers;
    [SerializeField] MonoBehaviour componentToTrack;

    [SerializeField] bool activated = false;
    [SerializeField] int  save;
    [SerializeField] bool exclude = true;
    int readylayer;
    Camera main;

    void Start()
    {
       main=Camera.main;
       
    }

    void FixedUpdate()
    {
        if (componentToTrack.enabled && !activated)
        {
            if (main != null)
            {
                save = main.cullingMask;
                if(!exclude)
                {
                    readylayer = layers.value;
                } else
                {
                   readylayer =main.cullingMask-layers.value;
                }

                main.cullingMask = readylayer;

                activated = true;
            }
        }

        if (activated && !componentToTrack.enabled)
        {
            if(main != null && main.cullingMask==readylayer)
            main.cullingMask = save;
            activated = false;
        }
    }
}