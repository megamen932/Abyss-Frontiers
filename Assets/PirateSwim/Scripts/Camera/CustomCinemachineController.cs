﻿using Cinemachine;
using PirateSwim.Scripts.ShipScripts;
using UnityEngine;
[ExecuteInEditMode]
public class CustomCinemachineController : MonoBehaviour
{
    CinemachineVirtualCamera cam;
    public bool FistPerson = false;

    Ship _ship;

    // Use this for initialization
    void Awake()
    {
    
        _ship = Ship.Main;
        cam = GetComponent<CinemachineVirtualCamera>();
      
    }

    void UpdateLens()
    {
        LensSettings lens = cam.m_Lens;
        lens.FarClipPlane = _ship.farClipEnd(transform);
        if (!FistPerson)
        {
            lens.NearClipPlane = _ship.CameraThirdPesonNearClip;
            lens.FieldOfView = _ship.thirdPersonViewFov;
        } else
        {
            lens.FieldOfView = _ship.firstPersonViewFov;
        }

        cam.m_Lens = lens;
    }

   
    void FixedUpdate ()
    {
        UpdateLens();
    }
}