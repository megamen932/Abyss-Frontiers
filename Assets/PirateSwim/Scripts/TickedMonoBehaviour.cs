﻿using JetBrains.Annotations;
using PirateSwim;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using TickedPriorityQueue;
using UnityEngine;
using Debug = System.Diagnostics.Debug;

public abstract class TickedMonoBehaviourBase : SerializedMonoBehaviour
{
    protected abstract void DoUpdate(float deltaTime);


    [SerializeField, FoldoutGroup("Ticked")]  [OnValueChanged(nameof(DeltaTimeChanged))]
    protected float targetTickTime = 0.1f;

    [ReadOnly, SerializeField, FoldoutGroup("Ticked")]
    float deltaTime;

    [SerializeField, FoldoutGroup("Ticked")]
    protected bool autoDeltaTime = true;

    [NotNull] UnityTickedQueue _steeringQueue;
    TickedObject               tickedObject;

    [ShowInInspector, FoldoutGroup("Ticked")] [OnValueChanged(nameof(QueueLengthChanged))]
    protected static int maxQueueProcessedPerUpdate = 50;

    [SerializeField, FoldoutGroup("Ticked"), OnValueChanged(nameof(QueueNameChanged))]
    protected string queueName = "null";

    #region Methods

    #region Ticked

    protected virtual void OnEnable()
    {
        Debug.Assert(_steeringQueue != null, nameof(_steeringQueue) + " != null");
        _steeringQueue.Add(tickedObject);
        lastUpdateTime = Time.time;
        Enable();
    }

    void DeltaTimeChanged()
    {
        if (tickedObject == null)
        {
            Awake();
        }

        tickedObject.TickLength = targetTickTime;
    }

    void QueueLengthChanged()
    {
        if (_steeringQueue == null)
        {
            Awake();
        }

        _steeringQueue.MaxProcessedPerUpdate = maxQueueProcessedPerUpdate;
    }

    protected virtual void Awake()
    {
        tickedObject = new TickedObject(DoUpdate);

        QueueNameChanged(queueName);


        DeltaTimeChanged();
        QueueLengthChanged();
        OnAwake();
    }


    float lastUpdateTime = 0;

    void DoUpdate(object userdata)
    {
        if (!isActiveAndEnabled)
        {
            OnDisable();
            return ;
        }

        if (autoDeltaTime)
        {
            deltaTime = Time.time - lastUpdateTime;
            deltaTime = deltaTime >= targetTickTime ? deltaTime : targetTickTime;
        } else
        {
            deltaTime = targetTickTime;
        }

        lastUpdateTime = Time.time;
        DoUpdate(deltaTime);
    }

    protected virtual void OnDisable()
    {
        Debug.Assert(_steeringQueue != null, nameof(_steeringQueue) + " != null");
        _steeringQueue.Remove(tickedObject);
        Disable();
    }

    #endregion

    #region Virtual

    protected virtual void OnAwake()
    {
    }


    protected virtual void Disable()
    {
    }

    protected virtual void Enable()
    {
    }

    #endregion

    #endregion

    #region DebugAndNonGameFunctions

    protected bool NotZero(float x) => Utils.NotZero(x);

    void QueueNameChanged(string newName)
    {
        if (newName == "null")
        {
            newName = gameObject.layer.ToString();
        } else
        {
        }

        _steeringQueue = UnityTickedQueue.GetInstance(newName);
    }

    #endregion
}

public abstract class TickedMonoBehaviour : TickedMonoBehaviourBase
{
    protected sealed override void OnEnable()
    {
        base.OnEnable();
    }

    protected sealed override void Awake()
    {
        base.Awake();
    }

    protected sealed override void OnDisable()
    {
        base.OnDisable();
    }
}