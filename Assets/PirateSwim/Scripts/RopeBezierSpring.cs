using System.Collections.Generic;
using UnityEngine;

//If we have a stiff rope, such as a metal wire, then we need a simplified solution
//this is also an accurate solution because a metal wire is not swinging as much as a rope made of a lighter material
namespace PirateSwim
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(LineRenderer))]
    public class RopeBezierSpring : MonoBehaviour
    {
        //Objects that will interact with the rope
        public Transform whatTheRopeIsConnectedTo;
        public Transform target;

        //Line renderer used to display the rope
        public LineRenderer lineRenderer;

        //A list with all rope sections
        public List<Vector3> allRopeSections = new List<Vector3>();

        //Rope data
        [SerializeField] private float _ropeLength   = 1f;
        public                   float minRopeLength = 1f;

        public float maxRopeLength = 20f;

        //Mass of what the rope is carrying
        public float loadMass = 1;

        //How fast we can add more/less rope
        public float winchSpeed = 2f;

        public float density = 7750f;

        //The radius of the wire
        public float radius = 0.02f;

        //The joint we use to approximate the rope
        SpringJoint  springJoint;
        public bool  useGravity       = false;
        bool         rope             = false;
        bool         hasChangedRope   = false;
        public bool  autoStart        = false;
        public bool  autoLengthAtInit = true;
        public float dampingCoof      = 0.5f;
        public bool  isKinematic      = true;

        public float RopeLength
        {
            get { return _ropeLength; }
            set
            {
                hasChangedRope = true;
                _ropeLength = Mathf.Clamp(value, minRopeLength, maxRopeLength);
                UpdateWinch();
            }
        }

        void Start()
        {
            if(!whatTheRopeIsConnectedTo)
            whatTheRopeIsConnectedTo = transform;
            if (!autoStart || !target||rope)
            {
                return;
            }

            Init(target);
        }

        public void Init(Transform newTarget, float maxDist = -1, bool isThisKinematic = false)
        {
            isKinematic = isThisKinematic;
            target = newTarget;
            if (isKinematic == false)
            {
                springJoint = whatTheRopeIsConnectedTo.GetComponent<SpringJoint>();
                if (!springJoint)
                {
                    springJoint = whatTheRopeIsConnectedTo.gameObject.AddComponent<SpringJoint>();
                }

                springJoint.connectedBody = target.GetComponent<Rigidbody>();
                if (!springJoint.connectedBody)
                {
                    springJoint.connectedBody = target.gameObject.AddComponent<Rigidbody>();
                }

                springJoint.autoConfigureConnectedAnchor = false;
                springJoint.anchor = Vector3.zero;
                springJoint.connectedAnchor = Vector3.zero;

                springJoint.connectedBody.useGravity = useGravity;
                //Add the weight to what the rope is carrying
                target.GetComponent<Rigidbody>().mass = loadMass;
                UpdateSpring();
            }

            if (maxDist == -1)
            {
                _ropeLength = Vector3.Distance(target.position, whatTheRopeIsConnectedTo.position);
            }

            //Init the line renderer we use to display the rope
            lineRenderer = GetComponent<LineRenderer>();

            //Init the spring we use to approximate the rope from point a to b


            rope = true;
        }

        void FixedUpdate()
        {
            if (!rope)
            {
                return;
            }

            if (!target)
            {
                return;
            }

            if (Input.GetKey(KeyCode.O) && RopeLength < maxRopeLength)
            {
                RopeLength += winchSpeed * Time.deltaTime;
            }
            else if (Input.GetKey(KeyCode.I) && RopeLength > minRopeLength)
            {
                RopeLength -= winchSpeed * Time.deltaTime;
            }

            //Display the rope with a line renderer
            DisplayRope();
        }

        //Update the spring constant and the length of the spring
        private void UpdateSpring()
        {
            if (isKinematic == true)
            {
                return;
            }
            //Someone said you could set this to infinity to avoid bounce, but it doesnt work
            //kRope = float.inf

            //
            //The mass of the rope
            //
            //Density of the wire (stainless steel) kg/m3


            float volume = Mathf.PI * radius * radius * RopeLength;

            float ropeMass = volume * density;

            //Add what the rope is carrying
            ropeMass += loadMass;


            //
            //The spring constant (has to recalculate if the rope length is changing)
            //
            //The force from the rope F = rope_mass * g, which is how much the top rope segment will carry
            float ropeForce = ropeMass * 9.81f;

            //Use the spring equation to calculate F = k * x should balance this force, 
            //where x is how much the top rope segment should stretch, such as 0.01m

            //Is about 146000
            float kRope = ropeForce / 0.01f;

            //print(ropeMass);

            //Add the value to the spring            
            springJoint.spring = kRope * 1.0f;
            springJoint.damper = kRope * 0.8f * dampingCoof;

            //Update length of the rope
            springJoint.maxDistance = RopeLength;
        }

        //Display the rope with a line renderer
        private void DisplayRope()
        {
            //This is not the actual width, but the width use so we can see the rope
          //  float ropeWidth = 0.2f;

           // lineRenderer.startWidth = ropeWidth;
         //   lineRenderer.endWidth = ropeWidth;


            //Update the list with rope sections by approximating the rope with a bezier curve
            //A Bezier curve needs 4 control points
            Vector3 A = whatTheRopeIsConnectedTo.position;
            Vector3 D = target.position;

            //Upper control point
            //To get a little curve at the top than at the bottom
            Vector3 B = A + whatTheRopeIsConnectedTo.up * (-(A - D).magnitude * 0.1f);
            //B = A;

            //Lower control point
            Vector3 C = D - target.up * ((A - D).magnitude * 0.5f);

            //Get the positions
            BezierCurve.GetBezierCurve(A, B, C, D, allRopeSections);


            //An array with all rope section positions
            Vector3[] positions = new Vector3[allRopeSections.Count];

            for (int i = 0; i < allRopeSections.Count; i++)
            {
                positions[i] = allRopeSections[i];
            }

            //Just add a line between the start and end position for testing purposes
            //Vector3[] positions = new Vector3[2];

            //positions[0] = whatTheRopeIsConnectedTo.position;
            //positions[1] = whatIsHangingFromTheRope.position;


            //Add the positions to the line renderer
            lineRenderer.positionCount = positions.Length;
            lineRenderer.material.mainTextureOffset=new Vector2(Time.time,0);
           // lineRenderer.material.mainTextureScale=new Vector2( lineRenderer.material.mainTextureScale.x,1+Mathf.Cos(Time.time*.1f)*3);
            lineRenderer.SetPositions(positions);
        }

        //Add more/less rope
        private void UpdateWinch()
        {
            if (hasChangedRope)
            {
                //Need to recalculate the k-value because it depends on the length of the rope
                UpdateSpring();
            }

            hasChangedRope = false;
        }

        public static class BezierCurve
        {
            //Update the positions of the rope section
            public static void GetBezierCurve(Vector3 A, Vector3 B, Vector3 C, Vector3 D, List<Vector3> allRopeSections)
            {
                //The resolution of the line
                //Make sure the resolution is adding up to 1, so 0.3 will give a gap at the end, but 0.2 will work
                float resolution = 0.05f;

                //Clear the list
                allRopeSections.Clear();


                float t = 0;

                while (t <= 1f)
                {
                    //Find the coordinates between the control points with a Bezier curve
                    Vector3 newPos = DeCasteljausAlgorithm(A, B, C, D, t);

                    allRopeSections.Add(newPos);

                    //Which t position are we at?
                    t += resolution;
                }

                allRopeSections.Add(D);
            }

            //The De Casteljau's Algorithm
            static Vector3 DeCasteljausAlgorithm(Vector3 A, Vector3 B, Vector3 C, Vector3 D, float t)
            {
                //Linear interpolation = lerp = (1 - t) * A + t * B
                //Could use Vector3.Lerp(A, B, t)

                //To make it faster
                float oneMinusT = 1f - t;

                //Layer 1
                Vector3 Q = oneMinusT * A + t * B;
                Vector3 R = oneMinusT * B + t * C;
                Vector3 S = oneMinusT * C + t * D;

                //Layer 2
                Vector3 P = oneMinusT * Q + t * R;
                Vector3 T = oneMinusT * R + t * S;

                //Final interpolated position
                Vector3 U = oneMinusT * P + t * T;

                return U;
            }
        }
    }
}