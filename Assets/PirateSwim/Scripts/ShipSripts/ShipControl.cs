using BindingsExample;
using UnityEngine;

namespace PirateSwim.Scripts.ShipSripts
{
    public class ShipControl : MonoBehaviour
    {
        public PlayerActions playerActions;
        string        saveData;


        void OnEnable()
        {
            // See PlayerActions.cs for this setup.
            playerActions = PlayerActions.CreateWithDefaultBindings();
            //playerActions.Move.OnLastInputTypeChanged += ( lastInputType ) => Debug.Log( lastInputType );

            LoadBindings();
        }


        void OnDisable()
        {
            // This properly disposes of the action set and unsubscribes it from
            // update events so that it doesn't do additional processing unnecessarily.
            playerActions.Destroy();
        }


        void SaveBindings()
        {
            saveData = playerActions.Save();
            PlayerPrefs.SetString( "Bindings", saveData );
        }


        void LoadBindings()
        {
            if (PlayerPrefs.HasKey( "Bindings" ))
            {
                saveData = PlayerPrefs.GetString( "Bindings" );
                playerActions.Load( saveData );
            }
        }


        void OnApplicationQuit()
        {
            PlayerPrefs.Save();
        }

    }
}