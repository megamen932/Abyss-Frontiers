﻿using System;
using System.Collections.Generic;
using Cinemachine;
using DG.Tweening;
using JetBrains.Annotations;
using PirateSwim.Scripts.ShipSripts;
using PirateSwim.ShipScripts;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace PirateSwim.Scripts.ShipScripts
{
    public class Ship : MonoBehaviour
    {
        public enum ShipStates
        {
            Flying,
            OnTrick,
        }

        #region Methods

        #region Init

        void Awake()
        {
            if (ship == null)
            {
                ship = this;
            } 
        }

        void Start()
        {
            if (musicClip != null && playerAudio && playerAudio.enabled)
            {
                playerAudio.clip = musicClip;
                playerAudio.Play();
            }


            startPos   = transform.position;
            _rigidbody = GetComponent<Rigidbody>();
            Health     = MaxHealth;
            if (!healthBar) healthBar = gameObject.GetComponentInGOAndChildren<HealthBar>();


            if (healthBar != null)
            {
                healthBar.MaxHealth = MaxHealth;
                healthBar.Health    = Health;
            }


            var SwipeDetector = gameObject.AddComponent<SwipeDetector>();
            SwipeDetector.OnSwipe += DoSwipe;
            useSphere             &= outerSphere != null && innerSphere != null;


            if (useSphere)
            {
                var shipCanAchive = Utils.Hypothenys(MinZObstaclesOffset, Mathf.Max(SwipingSideSpeed.x, SwipingSideSpeed.y));
                var cam           = Camera.main;
                var frustumHeight = cam.farClipPlane * Mathf.Tan(cam.fieldOfView * 0.5f * Mathf.Deg2Rad);
                var frustumWidth  = frustumHeight * cam.aspect;


                var visible = Utils.Hypothenys(Mathf.Max(frustumHeight, frustumWidth), MinZObstaclesOffset);

                innerSphere.radius = Mathf.Max(shipCanAchive, visible);
                outerSphere.radius = innerSphere.radius + 50f;
            }


            _sailMaterials = new List<Material>();


            foreach (var sail in Sails)
            {
                var renderer = sail.gameObject.GetComponentInChildren<MeshRenderer>();
                if (renderer) _sailMaterials.Add(renderer.material);
            }

            ChangeSideMovement();

            _control = gameObject.GetOrAdd<ShipControl>();


            DOTween.SetTweensCapacity(3000, 500);
            PauseManager.Instance.Resume();
        }

        #endregion

    
        #region Control

        void DoSwipe(SwipeData data)
        {
            switch (data.Direction)
            {
                case SwipeDirection.Up:
                    ChangeView();


                    break;
                case SwipeDirection.Down:


                    if (FirstPersonView)
                    {
                        ShootCannon();
                    } else
                    {
                        //   ShootGrapleHook();
                    }


                    break;
                case SwipeDirection.Left:
                    DoAbility(false);


                    break;
                case SwipeDirection.Right:
                    DoAbility(true);


                    break;
                default:


                    throw new ArgumentOutOfRangeException();
            }
        }

        public void InvertY()
        {
            invert.y *= -1;
        }


        public void ChangeControls()
        {
            Fast_Swap = !Fast_Swap;


            //    if (joystick != null)
            {
                //  joystick.joystickMode = Fast_Swap ? JoystickMode.Horizontal : JoystickMode.AllAxis;
            }
        }

        Quaternion GyroToUnity(Quaternion q) => new Quaternion(q.x, q.y, -q.z, -q.w);


        [Serializable]
        public struct InputRaw
        {
            public float x;
            public float y;


            public InputRaw(float _x = 0, float _y = 0)
            {
                x = _x;
                y = _y;
            }


            public float X
            {
                get { return x; }
                set { x = Mathf.Clamp(value, -1, 1); }
            }

            public float Y
            {
                get { return y; }
                set { y = Mathf.Clamp(value, -1, 1); }
            }
        }

        #endregion

        #region Deep

        public float deepLevelNorm           => deepLevel / MaxDist;
        public float FakeYSpeed              => Mathf.Sin(deepAngleRad) * curVelocity.z;
        public float FakeY                   => Mathf.Clamp(Traveled.z * Mathf.Sin(deepAngleRad), 0, MaxDist);
        public float deepLevel               => Mathf.Clamp(-transform.position.y + MinDeep + FakeY, 0, MaxDist);
        public float ShipYDeep               => Mathf.Clamp(-transform.position.y + MinDeep, 0, MaxDist);
        public float deepLevelWorld          => Mathf.Clamp(-transform.position.y + MinDeep , 0, MaxDist);
        public float DeepToReal(float  Deep) => -(Deep) + startPos.y ;
        public float DeepYToReal(float Deep) => -Deep + startPos.y + FakeY;
        public float RealToDeep(float  Deep) => -Deep - startPos.y - FakeY;

        float   MinDeep  => startPos.y + MaxDist * StartDeepNorm;
        float   MaxDeep  => startPos.y - MaxDist * (1 - StartDeepNorm);
        Vector3 Traveled => transform.position - startPos;

        #endregion


        #region Updates

        void FixedUpdate()
        {
            if (Use360Velocity)
            {
                ShipMovement360();
            } else
            {
                ShipMovement();
            }

            CheckCollision();

            FX();
            ShipFX();


            CameraManaging();

            ManageHealth();
            UpdateGUI();
            PredictorUpdate();
        }


        void Update()
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                Reload();
            }

            if (Input.GetKey(KeyCode.L))
            {
                if (PauseManager.Instance != null)
                {
                    PauseManager.Instance.TogglePause();
                }
            }

            if (Input.GetKey(KeyCode.P))
            {
                ChangeSpeedRelative(.05f);
            }


            if (Input.GetKey(KeyCode.O))
            {
                ChangeSpeedRelative(-.05f);
            }


            if (Input.GetButton("Shoot") || _control.playerActions.Fire.WasPressed)
            {
                ShootCannon();
            }


            if (Input.GetKeyDown(KeyCode.I))
            {
                InvertY();
            }


            if (Input.GetKeyDown(KeyCode.F))
            {
                ChangeControls();
            }


            if (Input.GetKeyDown(KeyCode.V))
            {
                ChangeView();
            }


            if (Input.GetKeyDown(KeyCode.Keypad4))
            {
                DoAbility(true);
            }


            if (Input.GetKeyDown(KeyCode.Keypad6))
            {
                DoAbility(false);
            }

            if (Input.GetKeyDown(KeyCode.KeypadPlus))
            {
                _cameras[curActiveCamera].enabled = false;

                curActiveCamera = (curActiveCamera + 1) % _cameras.Count;
                _cameras[curActiveCamera].enabled = true;
            }

            if (Input.GetKeyDown(KeyCode.KeypadMinus))
            {
                _cameras[curActiveCamera].enabled = false;

                curActiveCamera = (curActiveCamera - 1) % _cameras.Count;
                _cameras[curActiveCamera].enabled = true;
            }

            if (Input.GetKeyDown(KeyCode.Keypad0))
            {
                _cameras[curActiveCamera].enabled = false;

                curActiveCamera = 0;
                _cameras[curActiveCamera].enabled = true;
            }

            if (Input.GetKeyDown(KeyCode.Keypad1))
            {
                _cameras[curActiveCamera].enabled = false;

                curActiveCamera = 1;
                _cameras[curActiveCamera].enabled = true;
            }

            if (Input.GetKeyDown(KeyCode.Keypad2))
            {
                _cameras[curActiveCamera].enabled = false;

                curActiveCamera = 2;
                _cameras[curActiveCamera].enabled = true;
            }

            if (Input.GetKeyDown(KeyCode.Keypad3))
            {
                _cameras[curActiveCamera].enabled = false;

                curActiveCamera = 3;
                _cameras[curActiveCamera].enabled = true;
            }

            if (Input.GetKeyDown(KeyCode.Keypad4))
            {
                _cameras[curActiveCamera].enabled = false;

                curActiveCamera = 4;
                _cameras[curActiveCamera].enabled = true;
            }

            if (Input.GetKeyDown(KeyCode.Keypad5))
            {
                _cameras[curActiveCamera].enabled = false;

                curActiveCamera = 5;
                _cameras[curActiveCamera].enabled = true;
            }
        }


        void CheckCollision()
        {
            RaycastHit hit;
            Ray        ray = new Ray(transform.position, ship.curVelocity);
            if (Physics.Raycast(ray, out hit, ship.curVelocity.magnitude * Time.fixedDeltaTime, ObstaclesLayers.value))
            {
                var impactForce = ship.curVelocity.magnitude;
                if (hit.rigidbody)
                {
                    impactForce += Vector3.Dot(ship.curVelocity, hit.rigidbody.velocity);
                }

                var distFromCenter = Vector3.Distance(hit.point, hit.collider.bounds.center);
                GetDamage(impactForce  / distFromCenter);
            }
        }

        #endregion

     


   

        Vector2 invert = Vector2.one;


        public void ChangeSpeedRelative(float _speed)
        {
            speed *= 1 + _speed;
        }

        #region Predictor

        [NotNull]
        public PosPredictor Predictor
        {
            get
            {
                if (_predictor == null)
                {
                    _predictor = new PosPredictor();
                }

                return _predictor;
            }
        }


        public PosPredictor                      _predictor          = new PosPredictor();
        public PosPredictor.PredictorUpdateTypes predictorUpdateType = PosPredictor.PredictorUpdateTypes.Accel;

        public float m_LookaheadSmoothing;

        void PredictorUpdate()
        {
            var deltaTime = Time.deltaTime;
            if ((double) deltaTime < 0.0)
            {
                Predictor.Reset();
            }


            Vector3 followTargetPosition = transform.position;


            Predictor.Smoothing = m_LookaheadSmoothing;
            switch (predictorUpdateType)
            {
                case PosPredictor.PredictorUpdateTypes.Pos:
                    Predictor.AddPosition(followTargetPosition);
                    break;
                case PosPredictor.PredictorUpdateTypes.Velocity:
                    Predictor.AddVelocityAndPos(followTargetPosition, curVelocity);
                    break;
                case PosPredictor.PredictorUpdateTypes.Accel:
                    Predictor.AddVelocityPosAccel(followTargetPosition, curVelocity, curAccel);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }


            TrackedPoint = (double) m_LookaheadTime <= 0.0 ? followTargetPosition : Predictor.PredictPosition(m_LookaheadTime);
        }


        public Vector3 ShipPosAtTime(float t)
        {
            var shipPosAtTime = (double) t <= 0.0 ? transform.position : Predictor.PredictPosition(t) ;

            return shipPosAtTime;
        }

        public Vector3 TrackedPoint { get ; set ; }

        #endregion

        #region Cameras


        void ChangeView()
        {
            FirstPersonView = !FirstPersonView;

            // firtsPersonCamera.enabled = !firtsPersonCamera.enabled;
        }
      

        public int curActiveCamera = 0;

        void CameraManaging()
        {
            if (_cameras.Count > 2)
            {
                _cameras[curActiveCamera].enabled = true;
                return;
            }

            if (downCamera != null)
            {
                downCamera.enabled = curVelocity.y < 0;
            }

            if (trickCamera != null)
            {
                trickCamera.enabled = isStateTrick(ShipState) & !FirstPersonView;
            }

            if (firtsPersonCamera != null)
            {
                firtsPersonCamera.enabled = FirstPersonView;
            }
        }


     
        #endregion

        #region Movement



        #region Trics


        public int  TrickDir;
        Quaternion  RollRot  ;
        public bool canPerfromTrck => _shipState == ShipStates.Flying;


        void DoAbility(bool rigth)
        {
            if (!canPerfromTrck)
            {
                return ;
            }


            var lastState = ShipState;

            ShipState = ShipStates.OnTrick;
            TrickDir  = rigth ? 1 : -1;
            var status = false;


            if (rigth && currentRigthAbility)
            {
                status = currentRigthAbility.StartPerform();
            } else if (currentLeftAbility)
            {
                status = currentLeftAbility.StartPerform();
            }


            if (!status)
            {
                ShipState = lastState;
            }
        }


        void ShootCannon()
        {
            cannon.StartShooting();
        }


        bool isStateTrick(ShipStates state) => state == ShipStates.OnTrick;


        public ShipStates ShipState
        {
            get { return _shipState; }
            set
            {
                if (isStateTrick(value)) //Do trick
                {
                    if (isStateTrick(_shipState)) // Ship already on trick
                    {
                        return;
                    }
                }


                _shipState = value;
            }
        }


        public void EndTrick()
        {
            ShipState = ShipStates.Flying;
        }

        #endregion

        void ShipMovement()
        {
            //Input    
            if (FirstPersonView && useGyro)
            {
                var gyro = GyroToUnity(Input.gyro.attitude).eulerAngles;
                input.x = gyro.y / GyroSens.x + Input.GetAxis("Horizontal");
                input.y = gyro.x / GyroSens.y + Input.GetAxis("Vertical");
            } else
            {
                input.X = _control.playerActions.Move.X;
                input.Y = _control.playerActions.Move.Y;
            }


            var transformPosition = transform.position;
            transformPosition.y = -deepLevel + MinDeep + FakeY;
            transform.position  = transformPosition;


            switch (ShipState)
            {
                case ShipStates.Flying:
                    Flying();


                    break;

                case ShipStates.OnTrick:


                    break;

                default:


                    throw new ArgumentOutOfRangeException();
            }
        }


      
      

        public void Flying()
        {
            var SideSpeed     = input.X * SwipingSideSpeed.x * SideSpeedFallof.Evaluate(deepLevelNorm);
            var VerticalSpeed = (Fast_Swap ? Mathf.Sign(input.Y) : input.Y);

            VerticalSpeed = VerticalSpeed * SwipingSideSpeed.y * VerticalSpeedFallof.Evaluate(deepLevelNorm);
            DebugF1       = VerticalSpeedFallof.Evaluate(deepLevelNorm);


            wantedVelocity = new Vector3(SideSpeed, VerticalSpeed, speed);
            var lerpSpeed = new Vector3(Time.deltaTime / timeToGetMaxRotationSpeed.x, (Fast_Swap ? 8 : 1) * Time.deltaTime / timeToGetMaxRotationSpeed.y,
                Time.deltaTime);
            curVelocity = new Vector3(Mathf.Lerp(curVelocity.x, wantedVelocity.x, lerpSpeed.x), Mathf.Lerp(curVelocity.y, wantedVelocity.y, lerpSpeed.y),
                Mathf.Lerp(curVelocity.z, wantedVelocity.z, lerpSpeed.z));

            curAccel = PirateSwim.Utils.Mul(wantedVelocity - curVelocity, lerpSpeed);

            transform.forward = Vector3.Lerp(transform.forward, curVelocity, Time.deltaTime);


            var euler = transform.eulerAngles;
            euler.z = -curVelocity.x / SwipingSideSpeed.x * FlyingRollStrength ;
            if (!Utils.IsVectorNaN(euler)) transform.eulerAngles = euler;


            foreach (var sail in Sails)
            {
                var HorizontalVel = wantedVelocity;
                HorizontalVel.y = 0;


                var rot = Quaternion.AngleAxis(wantedVelocity.x * SailRotatingSpeed, Vector3.up) * Quaternion.LookRotation(HorizontalVel, Vector3.up);


                sail.localRotation = Quaternion.Slerp(sail.localRotation, rot, Time.deltaTime * (Mathf.Abs(HorizontalVel.x * SwipingSailSpeed) + 0.5f));
            }


            _rigidbody.velocity = curVelocity;
        }

        public Vector3 curAccel { get ; set ; }


        void ShipMovement360()
        {
            var SideSpeed     = input.X                                     * SwipingSideSpeed.x;
            var VerticalSpeed = (Fast_Swap ? Mathf.Sign(input.Y) : input.Y) * SwipingSideSpeed.y;

            var wantedVelocity = new Vector3(SideSpeed, VerticalSpeed, speed) * Time.deltaTime;


            curVelocity = new Vector3(Mathf.Lerp(curVelocity.x, wantedVelocity.x, timeToGetMaxRotationSpeed.x * Time.deltaTime),
                Mathf.Lerp(curVelocity.y, wantedVelocity.y, Fast_Swap ? 2 : 1 * timeToGetMaxRotationSpeed.y * Time.deltaTime),
                Mathf.Lerp(curVelocity.z, wantedVelocity.z, Time.deltaTime));


            transform.Rotate(Vector3.up,    curVelocity.x);
            transform.Rotate(Vector3.right, -curVelocity.y);


            if (Math.Abs(transform.rotation.eulerAngles.z) > 20f)

                //     transform.rotation = Quaternion.Slerp(transform.rotation,Quaternion.LookRotation(transform.forward, Vector3.up + curVelocity.x * Vector3.right),Time.deltaTime);
                //  transform.forward = forward;

                foreach (var sail in Sails)
                {
                    var HorizontalVel = wantedVelocity;
                    HorizontalVel.y = 0;


                    var rot = Quaternion.AngleAxis(HorizontalVel.x * SailRotatingSpeed, Vector3.up) * Quaternion.LookRotation(HorizontalVel, Vector3.up);


                    sail.localRotation = Quaternion.Slerp(sail.localRotation, rot, Time.deltaTime * (Mathf.Abs(HorizontalVel.x * SwipingSailSpeed) + 0.5f));


                    if (Fast_Swap)
                    {
                        if (wantedVelocity.y >= 0)
                        {
                            sail.gameObject.GetComponentInChildren<MeshRenderer>().material.color = Color.black;
                        } else
                        {
                            sail.gameObject.GetComponentInChildren<MeshRenderer>().material.color = Color.red;
                        }
                    }
                }


            if (Camera.main != null)
            {
                var crateColliderLocalPosition = CrateCollider.transform.localPosition;


                crateColliderLocalPosition.z = MinZObstaclesOffset + CrateCollider.GetComponent<Collider>().bounds.extents.z;


                CrateCollider.transform.localPosition = crateColliderLocalPosition;
            }


            transform.position += curVelocity.z * transform.forward;

            // transform.Translate(curVelocity );
        }

        #endregion

        #region FX

        List<Material>         _sailMaterials;
        [SerializeField] float FogEndOffset = 50f;
        Vignette                      _vigFX;
        PostProcessVolume             volume  ;
        [SerializeField] float        m_LookaheadTime;
        public           ColorGrading _gradingFX { get; set; }


        void FX()
        {
            //FX

            // dirLight.rotation = Quaternion.Euler(StartLightAngle+ transform.position.y, 0, 0);
            //   skybox.SetColor("_HorizonColor",Color.Lerp(Color.magenta, Color.black, (transform.position.y-SwipingVerticalDistance*.5f)/SwipingVerticalDistance));
            //    if(!isGradient){
            //        var lowColorLerp = Color.Lerp(LowColor,MiddleColor,(transform.position.y+SwipingVerticalDistance*3)/SwipingVerticalDistance/lowSkyCoof);
            //        var color = Color.Lerp(lowColorLerp, HeigthColor, (transform.position.y-SwipingVerticalDistance*3)/SwipingVerticalDistance/HeigthSkyCoof);
            //       // dirLight.GetComponent<Light>().color = Color.Lerp(dirLight.GetComponent<Light>().color, color, Time.deltaTime);
            //        skybox.SetColor("_SkyTint",Color.Lerp(skybox.GetColor("_SkyTint"),color,Time.deltaTime));
            //        skybox.SetColor("_GroundColor",Color.Lerp(skybox.GetColor("_GroundColor"),lowColorLerp,Time.deltaTime));
            //        RenderSettings.fogColor = color;
            //        }else
            // {

            var color = SkyGradient.Evaluate(deepLevelNorm);


            if (dirLight != null)
            {
                dirLight.intensity = MaxLightIntensity * lightIntensityFallof.Evaluate(deepLevelNorm);
            }


            //            skybox.SetColor("_SkyTint", color);
            Camera.main.backgroundColor     = color;
            RenderSettings.fogColor         = color;
            RenderSettings.ambientLight     = color;
            RenderSettings.fogStartDistance = Camera.main.farClipPlane + FogStartOffset;
            RenderSettings.fogEndDistance = MinZObstaclesOffset + FogEndOffset;


            if (deepLevelNorm >= 0.33f)
            {
                if (!volume)
                {
                    if (!_vigFX)
                    {
                        spotLight.gameObject.SetActive(dirLight.intensity < MaxLightIntensity * .6f);

                        _vigFX = ScriptableObject.CreateInstance<Vignette>();
                        _vigFX.enabled.Override(true);
                        _vigFX.intensity.Override(_vignetteCurve.Evaluate(deepLevelNorm));
                    }


                    if (!_gradingFX)
                    {
                        _gradingFX = ScriptableObject.CreateInstance<ColorGrading>();
                        _gradingFX.enabled.Override(true);
                        _gradingFX.tonemapper.Override(Tonemapper.ACES);
                    }

                    volume = PostProcessManager.instance.QuickVolume(Camera.main.gameObject.layer, 100f, _vigFX, _gradingFX);
                    volume.weight = 0f;
                    DOTween.Sequence().Append(DOTween.To(() => volume.weight, x => volume.weight = x, 1f, 3f));
                }


                _vigFX.intensity.Override( Mathf.Lerp(   _vigFX.intensity, _vignetteCurve.Evaluate(deepLevelNorm), Time.deltaTime));
            } else
            {
                spotLight.gameObject.SetActive(false);
            }


            // playerAudio.pitch               = 1;
        }

        void ShipFX()
        {
            foreach (var material in _sailMaterials)
            {
                material.color = Color.Lerp(material.color, sailColorGradient.Evaluate(Utils.Normalize(curVelocity.y, SwipingSideSpeed.y, .5f)),
                    Time.deltaTime);
            }
        }

        #endregion

        #region GUI

        public void Score(float amount)
        {
            totalScore      += amount * AddictiveContiniusScore;
            continiusAmount += AddictiveContiniusScore;
            Health          += amount * continiusAmount;

            UpdateGUI();
        }


        void UpdateGUI()
        {
            if (scoreText == null)
            {
                return;
            }


            scoreText.text = continiusAmount >= 2 ? $"Score:{totalScore:0.} x{continiusAmount:0.0} " : $"Score:{totalScore:0.}";


            scoreText.text += $"\n Deep:{deepLevel:000.0} meters";
            if (isDead) scoreText.text += "Wasted";
        }

        #endregion

        /*public IEnumerator SideMove
        {
            get
            {
                var side = Input.GetAxis("Horizontal") > 0 ? Vector3.right : Vector3.left;
                var dir =  SwipingSideDistance * side;
                Quaternion newRot=Quaternion.LookRotation(dir+velocity);
               var rotating= StartCoroutine(RotateTo(transform, SwipingRotationSpeed, newRot, true));
                for(float i=0;i<1;i+=Time.deltaTime*SwipingSpeed)
                {
                    transform.position+=
                }
            }
            
        }*/

        #region Health

        void ManageHealth()
        {
            Health = Mathf.Clamp(Health + HealthRegenSpeed * Time.deltaTime, -1000, MaxHealth);


            if (healthBar)
            {
                healthBar.Health = Health;
            }


            if (damaged)
            {
                // ... set the colour of the damageImage to the flash colour.
                if (damageImage != null)
                {
                    damageImage.color = flashColour;
                }
            }

            // Otherwise...
            else
            {
                // ... transition the colour back to clear.
                if (damageImage != null)
                {
                    damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
                }
            }


            // Reset the damaged flag.
            damaged = false;
        }

        public void GetDamage(float value)
        {
            if (invulnerable)
            {
                return;
            }

            Debug.Log("Total damage := " + value);
            continiusAmount = 1;

            // Set the damaged flag so the screen will flash.
            damaged = true;

            // Reduce the current health by the damage amount.
            Health -= value;


            // Set the health bar's value to the current health.
            if (healthBar)
            {
                healthBar.Health = Health;
            }


            // Play the hurt sound effect.
            if (playerAudio != null)
            {
                //   playerAudio.Play();
                playerAudio.pitch = 3;
            }


            // If the player has lost all it's health and the death flag hasn't been set yet...
            if (Health <= 0 && !isDead)
            {
                // ... it should die.
                Die();
            }
        }


        void Die()
        {
            isDead = true;

            // Turn off any remaining shooting effects.
            // playerShooting.DisableEffects ();

            // Tell the animator that the player is dead.
            //anim.SetTrigger ("Die");


            // Set the audiosource to play the death clip and play it (this will stop the hurt sound from playing).
            if (playerAudio != null)
            {
                playerAudio.clip = deathClip;
                playerAudio.Play();
            }

            if (scoreText != null)
            {
                scoreText.text += "\nWasted";
            }

            if (PauseManager.Instance != null)
            {
                PauseManager.Instance.Pause();
            }


            // Turn off the movement and shooting scripts.
            // playerMovement.enabled = false;
            // playerShooting.enabled = false;
        }

        #endregion

        #endregion

        #region NonGame

        public void Reload()
        {
            SceneManager.LoadScene(0);
        }

        void ChangeSideMovement()
        {
            if (useAngles)
            {
                var rad = SwpingAngles * Mathf.Deg2Rad;


                SwipingSideSpeed.x = speed * Mathf.Tan(rad.x);
                SwipingSideSpeed.y = speed * Mathf.Tan(rad.y);
            } else
            {
                //float t     = MinZObstaclesOffset / speed;
                var katet = new Vector2(SwipingSideSpeed.x, SwipingSideSpeed.y);
                SwpingAngles.x = Mathf.Atan(katet.x / speed) * Mathf.Rad2Deg;
                SwpingAngles.y = Mathf.Atan(katet.y / speed) * Mathf.Rad2Deg;

                //SwpingAngles =Utils.Mul(katet,katet)+Vector2.one*speed*speed ;
                //   SwpingAngles.x = Mathf.Sqrt(SwpingAngles.x);
                //  SwpingAngles.y = Mathf.Sqrt(SwpingAngles.y);
                //
            }
        }

        #endregion

        #region Propetry

        public static Ship Main
        {
            get {
                if (!ship)
                {
                    ship = FindObjectOfType<Ship>();
                }

                return ship; }
        }

            #endregion
        #region Fields

        [HideInInspector]   public          Rigidbody  _rigidbody;
        [BoxGroup("Debug")] public          float      DebugF1;
        [BoxGroup("Debug")] public          InputRaw   input        = new InputRaw(1, 1);
        [BoxGroup("Debug")] public          bool       invulnerable = false;
        [BoxGroup("Debug"), SerializeField] ShipStates _shipState   = ShipStates.Flying;

        public Vector3 wantedVelocity;

        bool isDead  = false; // Whether the player is dead.
        bool damaged = false; // True when the player gets damaged.

        [Header("Health")] public float Health = 100;

        public float MaxHealth = 100;

        public float HealthRegenSpeed = 5f;


        [TabGroup("Moving", "Control")] public bool Fast_Swap      = true;
        [TabGroup("Moving", "Control")] public bool Use360Velocity = false;

        [TabGroup("Moving", "Control") , SerializeField]
        Vector2 GyroSens;

        [TabGroup("Moving", "Control") , SerializeField]
        bool useGyro = false;

        [TabGroup("Moving", "Control"), OnValueChanged(nameof(InvertY))]
        public bool invertY = false;

        [TabGroup("Moving", "Control"), OnValueChanged(nameof(ChangeSideMovement))]
        public Vector2 SwpingAngles = new Vector2(45, 45);

        [TabGroup("Moving", "Control"), OnValueChanged(nameof(ChangeSideMovement))]
        public bool useAngles = true;

        [TabGroup("Moving", "Speed")] public Vector3 curVelocity;
        [TabGroup("Moving", "Speed")] public float   speed                     = 50;
        [TabGroup("Moving", "Speed")] public Vector2 timeToGetMaxRotationSpeed = new Vector2(1,  1);
        [TabGroup("Moving", "Speed")] public Vector2 SwipingSideSpeed          = new Vector2(20, 30);


        [TabGroup("Moving", "Speed")] public AnimationCurve VerticalSpeedFallof = AnimationCurve.EaseInOut(0, 1.2f, 1, 0);

        [TabGroup("Moving", "Speed")] public AnimationCurve SideSpeedFallof    = AnimationCurve.Constant(0, 1, 1);
        [TabGroup("Moving", "Speed")] public float          TimeToCompleteRoll = 1;

        [TabGroup("Moving", "Speed"), SerializeField]
        float FlyingRollStrength = 2;


        [TabGroup("Moving", "Sails")] public Transform[] Sails;

        [TabGroup("Moving", "Sails"), SerializeField, Range(0, 15)]
        float SailRotatingSpeed = 14;

        [TabGroup("Moving", "Sails")] public float SwipingSailSpeed = 3;

        [TabGroup("Moving", "Sails"), SerializeField]
        public Gradient sailColorGradient;


        [TabGroup("Moving", "Tricks"), SerializeField]
        public List<Ability> Abilties = new List<Ability>();

        [TabGroup("Moving", "Tricks"), SerializeField]
        Ability currentLeftAbility;

        [TabGroup("Moving", "Tricks"), SerializeField]
        Ability currentRigthAbility;


        [TabGroup("Settings", "GameSettings"), Sirenix.OdinInspector.ReadOnly]
        public float MinZObstaclesOffset => TimeToAbostacles * speed;

    

        [TabGroup("Settings", "GameSettings")] public float TimeToAbostacles = 7;

        [TabGroup("Settings", "GameSettings"), SerializeField, MinValue(0)]
        public float MaxDist = 1500f;

        [TabGroup("Settings", "GameSettings"), SerializeField, Range(1, 10)]
        float AddictiveContiniusScore = 1.2f;

        [TabGroup("Settings", "GameSettings"), SerializeField, Range(0, 1)]
        float StartDeepNorm = .05f;

        [TabGroup("Settings", "GameSettings"), SerializeField]
        LayerMask ObstaclesLayers;

        [TabGroup("Settings", "GameSettings"), SerializeField]
        float deepAngleRad = Mathf.PI / 12f;


        [TabGroup("Settings", "UI")] public HealthBar healthBar;
        [TabGroup("Settings", "UI")] public Text      scoreText;

        [TabGroup("Weapons", "Cannon"), SerializeField]
        Cannon cannon;


        [TabGroup("Settings", "FX")] public Material skybox;
        [TabGroup("Settings", "FX")] public Gradient SkyGradient;

        [TabGroup("Settings", "FX"), SerializeField]
        AnimationCurve lightIntensityFallof = AnimationCurve.EaseInOut(0, 1, 0, 0.05f);

        [TabGroup("Settings", "FX"), SerializeField]
        float MaxLightIntensity = 2;

        [TabGroup("Settings", "FX"), SerializeField]
        public Color flashColour = new Color(1f, 0f, 0f, 0.1f); // The colour the damageImage is set to, to flash.


        [TabGroup("Settings", "FX"), SerializeField]
        public float flashSpeed = 5f; // The speed the damageImage will fade at.

        [TabGroup("Settings", "Camera"), SerializeField]
        AnimationCurve _vignetteCurve;

        [TabGroup("Settings", "Camera"), SerializeField]
        public float firstPersonViewFov = 103;

        [TabGroup("Settings", "Camera"), SerializeField]
        public float thirdPersonViewFov = 60;

        [TabGroup("Settings", "Camera"), SerializeField]
        float CameraFarClip = 150;

        [TabGroup("Settings", "Camera"), SerializeField]
        public float CameraThirdPesonNearClip = 30;

        public float farClipEnd(Transform cameraT) => (int) ( CameraFarClip + transform.position.z - cameraT.position.z);

        [TabGroup("Settings", "Resource")] public AudioClip musicClip;

        [TabGroup("Settings", "Resource")] public Transform GrabTransform;

        [TabGroup("Settings", "Resource")] public SphereCollider innerSphere;

        [TabGroup("Settings", "Resource")] public SphereCollider outerSphere;

        [TabGroup("Settings", "Resource"), SerializeField]
        BoxCollider CrateCollider;


        [TabGroup("Settings", "Resource")] public Light dirLight;

        [TabGroup("Settings", "Resource")] public Image damageImage; // Reference to an image to flash on the screen on being hurt.

        [TabGroup("Settings", "Resource")] public AudioClip deathClip; // The audio clip to play when the player dies.

        [TabGroup("Settings", "Resource"), SerializeField]
        public Transform SceneParent;

        [TabGroup("Settings", "Resource")] public AudioSource playerAudio; // Reference to the AudioSource component.


        [TabGroup("Settings", "Resource"), SerializeField]
        Light spotLight;

        [BoxGroup("Camera"), SerializeField] bool                     FirstPersonView;
        [BoxGroup("Camera"), SerializeField] CinemachineVirtualCamera upCamera;
        [BoxGroup("Camera"), SerializeField] CinemachineVirtualCamera downCamera;
        [BoxGroup("Camera"), SerializeField] CinemachineVirtualCamera trickCamera;
        [BoxGroup("Camera"), SerializeField] CinemachineVirtualCamera firtsPersonCamera;

        [BoxGroup("Camera"), SerializeField] List<CinemachineVirtualCamera> _cameras;


        Animator                     anim; // Reference to the Animator component.
        [Header("Misc")] public bool useSphere = true;

        public Vector3 LootCoofSize = Vector3.one;


        float totalScore = 0;

        float continiusAmount = 1;

        ShipControl _control;


        public                   float   FogStartOffset = 100f;
        [ShowInInspector ,NotNull] public static Ship    ship;
        public                   Vector3 startPos;

        #endregion
    }
}