using UnityEngine;

namespace PirateSwim.Scripts.ShipSripts
{
    public class SpeedControl : BiAbility
    {
        public Vector3 TurboMultipier = Vector3.one * 2;
        public Vector3 SlowMultipier  = Vector3.one / 2;

        public AnimationCurve speedMultipierCurve;





        protected override void AbilityUpdate()
        {
            var multipier = direction == -1 ? SlowMultipier : TurboMultipier;
            var AnimCoef  =trickTimeCoef* speedMultipierCurve.Evaluate(Utils.Normalize(Time.time - startTime, duration));
            _ship.speed            = oldSpeed * multipier.z   * AnimCoef;
            _ship.SwipingSideSpeed = oldSideSpeed * multipier * AnimCoef;
            _ship.Flying();
        }





        float   oldSpeed;
        Vector2 oldSideSpeed;





        protected override void AbilityStart()
        {
            oldSpeed     = _ship.speed;
            oldSideSpeed = _ship.SwipingSideSpeed;
        }





        protected override void AbilityEnd()
        {
            _ship.speed            = oldSpeed;
            _ship.SwipingSideSpeed = oldSideSpeed;
        }





        public override bool StartPerform()
        {
            trickTimeCoef = 1; //TODO вынести в старт, сейчас это тут тоьлко чтобы играться с анимацией во время игры


            trickTimeCoef = 0;


            for (float t = 0; t < 1; t += Time.fixedDeltaTime / duration)
            {
                trickTimeCoef += speedMultipierCurve.Evaluate(t) * Time.fixedDeltaTime / duration;
            }


            trickTimeCoef = 1 / trickTimeCoef;


            Invoke(nameof(EndPerform), duration );


            return base.StartPerform();
        }
    }
}