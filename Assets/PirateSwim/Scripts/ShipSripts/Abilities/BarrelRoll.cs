using UnityEngine;

namespace PirateSwim.Scripts.ShipSripts {
    public class BarrelRoll : BiAbility
    {
     


        [ SerializeField] bool NormAnimCurveBarrelRoll = true;


        [ SerializeField] float BarrelDist = 15;
        [ SerializeField] float BarrelAngle = 360;

        [ SerializeField] float BarrelRollSpeedMultiplier = 1 / 4;



        [ SerializeField] AnimationCurve BarrelRollSpeedCurve = AnimationCurve.EaseInOut(0, 0.2f, 1, 1);





        protected override void AbilityStart()
        {
            _ship.invulnerable = true;
        }





        protected override void AbilityEnd()
        {
            _ship.invulnerable = false;
        }




        protected override void AbilityUpdate() {
            var vel = trickTimeCoef * Time.fixedDeltaTime / duration*BarrelRollSpeedCurve.Evaluate(Utils.Normalize(Time.time-startTime,duration));


            transform.Translate((oldTransform.forward * _ship.curVelocity.z * BarrelRollSpeedMultiplier + direction * Vector3.left * BarrelDist) * vel,
                                Space.World);


            transform.Rotate(oldTransform.forward, vel* direction * BarrelAngle);
        }





        public override bool StartPerform()
        {
            trickTimeCoef = 1;//TODO вынести в старт, сейчас это тут тоьлко чтобы играться с анимацией во время игры


            if (NormAnimCurveBarrelRoll)
            {
                trickTimeCoef = 0;


                for (float t = 0; t < 1; t += Time.fixedDeltaTime / duration)
                {
                    trickTimeCoef += BarrelRollSpeedCurve.Evaluate(t) * Time.fixedDeltaTime / duration;
                }


                trickTimeCoef = 1 / trickTimeCoef;
            }


            Invoke(nameof(EndPerform), duration);


            return base.StartPerform();
        }
    }
}