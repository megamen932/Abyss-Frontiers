using PirateSwim.Scripts.ShipScripts;

namespace PirateSwim.Scripts.ShipSripts {
    public abstract class BiAbility : Ability
    {
        public int direction;


        protected float trickTimeCoef;





        public override bool StartPerform()
        {
            var oldDir = direction;


            if (!_ship)
            {
                _ship=Ship.Main;
            }


            direction = _ship.TrickDir ;

            var status =  base.StartPerform();


            if (!status)
            {
                direction = oldDir;
            }


            return status;
        }
    }
}