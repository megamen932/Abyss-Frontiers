using UnityEngine;

namespace PirateSwim.Scripts.ShipSripts
{
    public class SizeControl : BiAbility
    {
        public Vector3        BigMultipier    = Vector3.one * 2;
        public Vector3        LittleMultipier = Vector3.one / 2;
        public float          speedSizeChange = 4;
        public AnimationCurve speedMultipierCurve;





        protected override void AbilityUpdate()
        {
            var multipier = direction == -1 ? BigMultipier : LittleMultipier;


            var AnimCoef = trickTimeCoef *
                           speedMultipierCurve.Evaluate(Utils.Normalize(Time.time - startTime, duration));


            _ship.transform.localScale = Vector3.Slerp(  _ship.transform.localScale, Utils.Mul(oldSize, multipier),
                                                         Time.fixedDeltaTime * trickTimeCoef * speedSizeChange *
                                                         AnimCoef);


            _ship.Flying();
        }





        Vector3 oldSize;





        protected override void AbilityStart()
        {
            oldSize = _ship.transform.localScale;


            if (direction == 1)
            {
                _ship.invulnerable = true;
            }
        }





        protected override void AbilityEnd()
        {
            _ship.transform.localScale = oldSize ;


            if (direction == 1)
            {
                _ship.invulnerable = false;
            }
        }





        public override bool StartPerform()
        {
            trickTimeCoef = 1; //TODO вынести в старт, сейчас это тут тоьлко чтобы играться с анимацией во время игры


            trickTimeCoef = 0;


            for (float t = 0; t < 1; t += Time.fixedDeltaTime / duration)
            {
                trickTimeCoef += speedMultipierCurve.Evaluate(t) * Time.fixedDeltaTime / duration;
            }


            trickTimeCoef = 1 / trickTimeCoef;


            Invoke(nameof(EndPerform), duration );


            return base.StartPerform();
        }
    }
}