using UnityEngine;

namespace PirateSwim.Scripts.ShipSripts
{
    public class ReverseTornado : BiAbility
    {
        [ SerializeField] bool NormAnimCurveBarrelRoll = true;


        [ SerializeField] float Angle = 360;

        [ SerializeField] float ReverseTornadoSpeedMultiplier = 1.25f;


        [ SerializeField] AnimationCurve ReverseTornadoSpeedCurve = AnimationCurve.EaseInOut(0, 0.2f, 1, 1);





        protected override void AbilityUpdate()
        {
            var vel = trickTimeCoef * Time.fixedDeltaTime / duration *
                      ReverseTornadoSpeedCurve.Evaluate(Utils.Normalize(Time.time - startTime, duration));


            transform.Translate((oldTransform.forward * _ship.curVelocity.z * ReverseTornadoSpeedMultiplier) * vel,
                                Space.World);


            transform.Rotate(Vector3.up, vel * direction * Angle);
        }





        protected override void AbilityStart()
        {
            _ship.invulnerable = true;
        }





        protected override void AbilityEnd()
        {
            _ship.invulnerable = false;
        }





        public override bool StartPerform()
        {
            trickTimeCoef = 1; //TODO вынести в старт, сейчас это тут тоьлко чтобы играться с анимацией во время игры


            if (NormAnimCurveBarrelRoll)
            {
                trickTimeCoef = 0;


                for (float t = 0; t < 1; t += Time.fixedDeltaTime / duration)
                {
                    trickTimeCoef += ReverseTornadoSpeedCurve.Evaluate(t) * Time.fixedDeltaTime / duration;
                }


                trickTimeCoef = 1 / trickTimeCoef;
            }


            Invoke(nameof(EndPerform), duration);


            return base.StartPerform();
        }
    }
}