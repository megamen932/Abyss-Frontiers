using System.Collections;
using PirateSwim.Scripts.ShipScripts;
using UnityEngine;

namespace PirateSwim.Scripts.ShipSripts
{
    public abstract class Ability : MonoBehaviour
    {
        public        bool          activated = false;
        protected     Ship          _ship;
        public        System.Action startEvent;
        public        System.Action endEvent;
        public        Transform     oldTransform;
//        public        string        name;
        public static GameObject    TricksParent;
        public        float         duration = 1.25f;
        protected        float         startTime;





        void Start()
        {
            if (!TricksParent)
            {
                TricksParent                  = new GameObject(nameof(TricksParent));
                TricksParent.transform.parent = transform;
            }


            _ship        = Ship.Main;
            oldTransform = new GameObject(name).transform;

            oldTransform.parent = TricksParent.transform;
        }





        public virtual bool StartPerform()
        {
            oldTransform.position = transform.position;
            oldTransform.rotation = transform.rotation;
            oldTransform.localRotation = transform.localRotation;
            oldTransform.forward = transform.forward;
            oldTransform.up = transform.up;

            AbilityStart();
            startEvent?.Invoke();
            activated = true;
            startTime = Time.time;
            StartCoroutine(nameof(Perform));


            return true;
        }





        protected void EndPerform()
        {
            if (!activated)
            {
                return;
            }


            AbilityEnd();

            activated = false;
            endEvent?.Invoke();
            _ship.EndTrick();
        }





        protected abstract void AbilityUpdate();

        protected virtual void AbilityStart() { }
        protected virtual void AbilityEnd() { }





        /// <summary>
        /// Base call to end trick
        /// </summary>
        /// <returns></returns>
        protected virtual IEnumerator Perform()
        {
            while (activated)
            {
                AbilityUpdate();


                yield return new WaitForFixedUpdate();
            }


            if (activated)
            {
                EndPerform();
            }


            yield break;
        }
    }
}