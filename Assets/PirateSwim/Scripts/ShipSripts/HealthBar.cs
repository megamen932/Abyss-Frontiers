using System;
using UnityEngine;
using UnityEngine.UI;

namespace PirateSwim
{
    public class HealthBar : MonoBehaviour
    {
        public Vector3       Offset;
        public RectTransform myRect;
        public RectTransform canvasRect;
        public float         Health { get; set; }
        public float         MaxHealth;
        public bool          healthSetted = false;
        public float         showHealth   = 0;
       public bool                 DontUpdateGUI    = false;
        public bool          isStatic     = true;
        Text healthText;
        Image _image;
        void Start()
        {
            if (Health != 0)
                MaxHealth = Health;
            if (myRect != null)
            {
                _image = myRect.GetComponent<Image>();
                healthText = _image.GetComponentInChildren<Text>();
            }
        }


        Color _color
        {
            get
            {
                return Color.white - new Color(0, Math.Max(0, MaxHealth - Health - MaxHealth * .25f) / MaxHealth,
                           Math.Max(0, MaxHealth - Health - MaxHealth * .25f) / MaxHealth, 0);
            }
        }

        void FixedUpdate()
        {
            if (!DontUpdateGUI)
            {
                Reload();
            }
            
        }

        void Reload()
        {
            if (Camera.main != null && _image != null && myRect != null)
            {
                showHealth = Health;
                if (!isStatic)
                {
                    var pos = Camera.main.WorldToViewportPoint(transform.position) + transform.InverseTransformDirection(Offset);
                    Vector2 screenPosition = new Vector2(((pos.x * canvasRect.sizeDelta.x) - (canvasRect.sizeDelta.x * 0.5f)),
                        ((pos.y * canvasRect.sizeDelta.y) - (canvasRect.sizeDelta.y * 0.5f)));
                    myRect.anchoredPosition = Vector2.Lerp(myRect.anchoredPosition, screenPosition, Time.deltaTime);
                }

                //  myRect.GetComponent<Image>().material.color = _color;
                _image.fillAmount = _color.b;
                if (healthText)
                {
                    healthText.text = String.Format("{0:D}/{1:D}",(int)Health ,(int)MaxHealth);
                }

            }

          
        }

    }
}