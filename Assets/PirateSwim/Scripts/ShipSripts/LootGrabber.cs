using System.Collections;

using PirateSwim.Scripts.ShipScripts;
using UnityEngine;

namespace PirateSwim
{
    
    public class LootGrabber : MonoBehaviour
    {
        public Ship             ship;
        public GameObject       cannon;
        RopeControllerRealistic rope2;
        RopeBezierSpring        rope1;

        public RangedWeapon rangedWeapon;
        public float        arrowSpeed    = 15f;
        public float        guidanceTime  = .25f;
        public float        AimHeigth     = 3f;
        public bool         useBallistick = true;

        void Start()
        {
            if (!ship)
            {
                ship = gameObject.GetComponentInGOAndParent<Ship>();
            }

            if (!rope2)
            {
                rope2 = cannon.GetComponent<RopeControllerRealistic>();
            }

            if (!rope1)
            {
                rope1 = cannon.GetComponent<RopeBezierSpring>();
            }

            rangedWeapon.transform = cannon.transform;
        }

        Loot target;
        bool looting = false;

        IEnumerator OnTriggerEnter(Collider other)
        {
//            Debug.Log("Loot Collision with " + other);

            if (target != null)
            {
                if (looting || target.looting)
                {
                    yield break;
                }
            }

            target = other.gameObject.GetComponentInGOAndParent<Loot>();

            if (target != null && target.free)
            {
                looting = true;
                target.StartTargeting();
                var Frame = new PosRot(rangedWeapon.transform);
                // Debug.Log(Time.time+"Start Throw Sword");
                yield return StartCoroutine(useBallistick ? Ballistick : Follow);
                //   Debug.Log(Time.time+"Throw Sword");

                if (target != null)
                {
                   // rope2.isPhysics = true;
                    rope2.Init(target.sword.transform, target, true);
                    //      Debug.Log(Time.time+"Rope Inited ");
                    // _rope.isPhysics = true;
                    target.StartLooting(ship, rope2);
                }

                looting = false;
                yield return StartCoroutine(Frame.Slerp(rangedWeapon.transform, guidanceTime));
            }
        }

        void FixedUpdate()
        {
            var transformLocalPosition = transform.localPosition;
            transformLocalPosition.z = arrowSpeed - GetComponent<Collider>().bounds.extents.z;
            transform.localPosition = transformLocalPosition;
        }

        IEnumerator Follow
        {
            get
            {
                var dir    = target.transform.position - transform.position;
                var newRot = Quaternion.LookRotation(-dir);
                for (var i = 0f; i < 1f; i += Time.deltaTime / guidanceTime)
                {
                    rangedWeapon.transform.rotation =
                        Quaternion.Slerp(rangedWeapon.transform.rotation, newRot, Time.deltaTime / guidanceTime);
                    yield return null;
                }

                if (!target)
                {
                    yield break;
                }

                var arrowGO = Instantiate(rangedWeapon.ShellPrefab, rangedWeapon.transform.position,
                    rangedWeapon.transform.rotation);

                //  _rope.Init(arrow.transform, false);
                if (!rope2)
                {
                    rope2 = cannon.GetComponent<RopeControllerRealistic>();
                }


                rope2.Init(arrowGO.transform, target, false);

                var startPos = arrowGO.transform.position;
                float time = 1;//Vector3.Distance(startPos, target.transform.position)/arrowSpeed;
                var startTime=time;
                while (time>0)
                {
                    if (target)
                    {
                        arrowGO.transform.LookAt(target.transform);
                    }
                    else
                    {
                        //  Destroy(arrowGO);
                        break;
                    }

                    //arrowGO.transform.Translate(Vector3.forward * arrowSpeed * Time.deltaTime);
                    arrowGO.transform.position = Vector3.Lerp(startPos, target.sword.transform.position,(startTime-time)/startTime);
                    time -= Time.deltaTime;
                    yield return null;
                }

                  Destroy(arrowGO.gameObject);
                arrowGO.transform.parent = target.transform;
            }
        }

        IEnumerator Ballistick
        {
            get
            {
                if (!target)
                {
                    yield break;
                }


                var g = Physics.gravity.magnitude;

                // var     startSpeed = new Vector2(arrowSpeed, 0.25f * width * g / arrowSpeed);
                Vector3 fireVelocity;
                Vector3 impactPoint;
                float   fullTime;
                if (!fts.solve_ballistic_arc_lateral(rangedWeapon.transform.position, arrowSpeed,
                    target.transform.position,
                    Vector3.zero /*we set shell parent, so no prediction needed*/, AimHeigth, out fireVelocity, out g,
                    out impactPoint, out fullTime))
                {
                    yield break;
                }

                var dir = new Vector3(fireVelocity.x * .5f * fullTime, fireVelocity.y * fireVelocity.y / (2 * g),
                    fireVelocity.z * fullTime * .5f);
                var newRot = Quaternion.LookRotation(dir);

                for (var i = 0f; i < 1f; i += Time.deltaTime / guidanceTime)
                {
                    rangedWeapon.transform.rotation =
                        Quaternion.Slerp(rangedWeapon.transform.rotation, newRot, Time.deltaTime / guidanceTime);
                    yield return null;
                }

                if (!target)
                {
                    yield break;
                }

                var arrowGO = Instantiate(rangedWeapon.ShellPrefab, rangedWeapon.transform.position,
                    rangedWeapon.ShellPrefab.transform.rotation).transform;
                arrowGO.parent = target.transform;
                var rope = arrowGO.gameObject.AddComponent<RopeBezierSpring>();
                rope.Init(cannon.transform, Vector3.Distance(cannon.transform.position, arrowGO.position), true);
                rope.lineRenderer.materials = rope2.lineRenderer.materials;
                rope.lineRenderer.colorGradient = rope2.lineRenderer.colorGradient;
                rope.lineRenderer.startWidth = rope2.lineRenderer.startWidth;
                rope.lineRenderer.endWidth = rope2.lineRenderer.endWidth;

                var arrow = arrowGO.GetComponent<Arrow>();
                if (!arrow)
                {
                    arrow = arrowGO.gameObject.AddComponent<Arrow>();
                }

                arrow.Init(fullTime, fireVelocity, g, false, impactPoint);
                //  _rope.Init(arrowGO, false);
                yield return new WaitForSeconds(fullTime);
                if (rope)
                    rope.enabled = false;
            }
        }
    }

    public struct RangedWeapon
    {
        public Transform transform;
        public GameObject ShellPrefab;
       
    }
}