using System.Collections;
using MoreMountains.Tools;
using PirateSwim.ShipScripts;
using Sirenix.OdinInspector;
using UnityEngine;

namespace PirateSwim
{
    public  class Weapon : MonoBehaviour
    {
        public Transform  ShootPlace;
        [HideIf(nameof(pooler))]
        public GameObject ShellPrefab;


        public bool           CanShoot => timeToCompleteReload < 0 && !isShooting;
        public bool           isShooting = false;
        public float          ReloadTime = 1;
        public MMObjectPooler pooler;


        protected virtual void Start()
        {
            pooler = GetComponent<MMObjectPooler>();
            timeToCompleteReload = 0;
        }


        protected Component SpawnSpell()
        {
            GameObject shell = null;
            if (pooler)
            {
                shell = pooler.GetPooledGameObject();
                if (!shell)
                {
                    return null;
                }
            }

            if (shell == null) shell =  Instantiate(ShellPrefab, ShootPlace.position, ShootPlace.rotation);

            shell.transform.position = ShootPlace.position;
            shell.transform.rotation = ShootPlace.rotation;
            shell.SetActive(true);


            LastSpell = shell.GetComponent(typeof(ISpell));
            if (LastSpell != null)
            {
                (LastSpell as Initable).Init();
            }

            //      shell.transform.position = ShootPlace.position;
            //     shell.transform.rotation = ShootPlace.rotation;
            OnShoot?.Invoke();
            return LastSpell;
        }


     public Component     LastSpell { get; set; }
        [HideInInspector] public System.Action OnShoot   { get; set; }
        public float         timeToCompleteReload;
        public bool          autoShoot = false;


        public bool StartShooting()
        {
            if (!CanShoot)
            {
                return false;
            }


            StartCoroutine(Shoot());

            if (ReloadTime > 0)
            {
                timeToCompleteReload = ReloadTime;
            }

            return true;
        }

        void FixedUpdate()
        {
            timeToCompleteReload -= Time.deltaTime;
            if (CanShoot && autoShoot)
            {
                StartShooting();
            }
        }


        protected  virtual IEnumerator Shoot()
        {
            yield break;
        }
    }
}