using System.Collections;
using UnityEngine;

namespace PirateSwim.ShipScripts
{
    public class CannonBackGround : Weapon
    {
        public ParticleSystem ShootParticles;


        Vector3        offset;
        public Vector3 OffsetBounds;
        public Vector3 OffsetSpeed;


        
  

        protected override IEnumerator Shoot()
        {
            if (isShooting)
            {
                yield break;
            }


            isShooting = true;


            var spell = SpawnSpell() as ISpell;
            if (spell != null)
            {
             //   spell.Set(settings);
                spell.PosOffset = Mathf.Sin(Time.time * OffsetSpeed.x) * Vector3.right * OffsetBounds.x +
                                  Mathf.Cos(Time.time * OffsetSpeed.z) * Vector3.forward * OffsetBounds.z;
                if (ShootParticles != null)
                {
                    ShootParticles.gameObject.SetActive(true);
                }
            }
 

            isShooting = false;
        }
    }
}