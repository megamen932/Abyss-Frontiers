using PirateSwim.Scripts.ShipScripts;
using Sirenix.OdinInspector;
using UnityEngine;

namespace PirateSwim.ShipScripts
{
   
   

    public interface Initable
    {
          void Init();
         bool Inited { get; } 
    }

    public interface ISpell : Initable
    {
        Rigidbody rigidbody { get; set ; }
       
       
        void Destroy();
        Vector3 PosOffset { get;set; }
    }

    public class Spell : SerializedMonoBehaviour, ISpell
    {
     

        protected Ship _ship;

         

        Rigidbody _rigid;

        public new Rigidbody rigidbody
        {
            get
            {
                if (!_rigid)
                {
                    _rigid = gameObject.GetOrAdd<Rigidbody>();
                    _rigid.useGravity = false;
                }

                return _rigid;
            }
            set { _rigid = value; }
        }

        public LayerMask _layerMaskAffected;


        public virtual void Init()
        {
            Inited = true;
            _ship = Ship.Main;

          rigidbody.velocity=Vector3.zero;
          rigidbody.angularVelocity=Vector3.zero;
        }

        public bool Inited { get; set; } = false;


        public virtual void Destroy()
        {
            //  Debug.Log("Destroyd" + name + " :pos " + transform.position);
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
            Inited = false;
            gameObject.SetActive(false);
            CancelInvoke();
            
        }

        public Vector3 PosOffset { get; set; }
    }
}