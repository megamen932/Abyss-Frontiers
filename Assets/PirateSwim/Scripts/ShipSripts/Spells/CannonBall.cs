using System;
using MoreMountains.Tools;
using PirateSwim.Scripts.ShipScripts;
using UnityEngine;

namespace PirateSwim.ShipScripts
{
   
    [Serializable]
    public class CannonBall : Spell
    {
    

        public float ShootForce           ;
        public float ExplosionForce       ;
        public float ExplosionRadius       ;
        public float timeToExplode        ;
        public float damage                ;

        public bool ForceOnInit            ;

      

        public GameObject         ExplodeParticles;


        public override void Init()
        {
            base.Init();
           
                if (ForceOnInit) rigidbody.AddForce(transform.forward * ShootForce + Ship.Main.curVelocity, ForceMode.VelocityChange);

                if (timeToExplode > 0) Invoke("Explode", timeToExplode);
            
        }


        void OnCollisionEnter(Collision other)
        {
            if (_layerMaskAffected.Contains(other.gameObject.layer))
            {
                Explode();
            }
        }


        protected virtual  void FixedUpdate()
        {
           
            if (!Inited )

            {
                return;
            }

           

            CheckCollision();
        }

        void CheckCollision()
        {
            if ( cantExplode)
            {
                return;
            }
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, Time.fixedDeltaTime * shareVelocity(), _layerMaskAffected.value))
            {
                Invoke("Explode", hit.distance / shareVelocity());
            }
        }

        protected Rigidbody targetRigid;
        GameObject target;
        protected virtual float shareVelocity()
        {
            if (rigidbody != null)
            {
                if (targetRigid != null)
                {
                    return Mathf.Abs(rigidbody.velocity.Dot(targetRigid.velocity));
                }

                return rigidbody.velocity.magnitude;
            }


            return 0;
        }

        protected bool cantExplode = false;

        protected void Explode()
        {
            if (cantExplode)
            {
                return;
            }

            var objects =   Physics.OverlapSphere(transform.position, ExplosionRadius, _layerMaskAffected.value);

         
            foreach (var o in objects)
            {
                if (o.attachedRigidbody)
                {
                    var rigid = o.attachedRigidbody;
                    rigid.SendMessage("GetDamage", damage / ExplosionRadius / ExplosionRadius,
                        SendMessageOptions.DontRequireReceiver);

                    rigid.AddExplosionForce(ExplosionForce, transform.position, ExplosionRadius);
                  
                }
            }

          
            if (ExplodeParticles != null)
            {
                ExplodeParticles.transform.parent = null;
                ExplodeParticles.SetActive(true);
            }


            Destroy();
        }

       
    }
}