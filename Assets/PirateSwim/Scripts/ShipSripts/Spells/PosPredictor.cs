using System;
using Sirenix.Serialization;
using UnityEngine;

namespace PirateSwim.ShipScripts
{
    
    abstract class GaussianWindow1d<T>
    {
        protected T[]     mData;
        protected float[] mKernel;
        protected float   mKernelSum;
        protected int     mCurrentPos;

        public GaussianWindow1d(float sigma, int maxKernelRadius = 10)
        {
            GenerateKernel(sigma, maxKernelRadius);
            mCurrentPos = 0;
        }

        public float Sigma { get; private set; }

        public int KernelSize
        {
            get { return mKernel.Length; }
        }

        private void GenerateKernel(float sigma, int maxKernelRadius)
        {
            int num = Math.Min(maxKernelRadius, Mathf.FloorToInt(Mathf.Abs(sigma) * 2.5f));
            mKernel = new float[2 * num + 1];
            mKernelSum = 0.0f;
            if (num == 0)
            {
                mKernelSum = mKernel[0] = 1f;
            } else
            {
                for (int index = -num; index <= num; ++index)
                {
                    mKernel[index + num] = (float) (Math.Exp(-(index * index) / (2.0 * sigma * sigma)) / Math.Sqrt(2.0 * Math.PI * sigma));
                    mKernelSum += mKernel[index + num];
                }
            }

            Sigma = sigma;
        }

        protected abstract T Compute(int windowPos);

        public void Reset()
        {
            mData = null;
        }

        public bool IsEmpty()
        {
            return mData == null;
        }

        public void AddValue(T v)
        {
            if (mData == null)
            {
                mData = new T[KernelSize];
                for (int index = 0; index < KernelSize; ++index) mData[index] = v;
                mCurrentPos = Mathf.Min(1, KernelSize - 1);
            }

            mData[mCurrentPos] = v;
            if (++mCurrentPos != KernelSize) return;
            mCurrentPos = 0;
        }

        public T Filter(T v)
        {
            if (KernelSize < 3) return v;
            AddValue(v);
            return Value();
        }

        public T Value()
        {
            return Compute(mCurrentPos);
        }

        public int BufferLength
        {
            get { return mData.Length; }
        }

        public void SetBufferValue(int index, T value)
        {
            mData[index] = value;
        }

        public T GetBufferValue(int index)
        {
            return mData[index];
        }
    }

    class GaussianWindow1D_Vector3 : GaussianWindow1d<Vector3>
    {
        public GaussianWindow1D_Vector3(float sigma, int maxKernelRadius = 10) : base(sigma, maxKernelRadius)
        {
        }

        protected override Vector3 Compute(int windowPos)
        {
            Vector3 zero = Vector3.zero;
            for (int index = 0; index < KernelSize; ++index)
            {
                zero += mData[windowPos] * mKernel[index];
                if (++windowPos == KernelSize) windowPos = 0;
            }

            return zero / mKernelSum;
        }
    }
[Serializable]
    public class PosPredictor
    {
        public       float                    mSmoothing = 10f;
        private       GaussianWindow1D_Vector3 m_Velocity = new GaussianWindow1D_Vector3(10f, 10);
        private       GaussianWindow1D_Vector3 m_Accel    = new GaussianWindow1D_Vector3(10f, 10);
        private       Vector3                  m_Position;
        private const float                    kSmoothingDefault = 10f;

        public float Smoothing
        {
            get { return mSmoothing; }
            set
            {
                if (value == (double) mSmoothing) return;
                mSmoothing = value;
                int maxKernelRadius = Mathf.Max(10, Mathf.FloorToInt(value * 1.5f));
                m_Velocity = new GaussianWindow1D_Vector3(mSmoothing, maxKernelRadius);
                m_Accel = new GaussianWindow1D_Vector3(mSmoothing, maxKernelRadius);
            }
        }

        [OdinSerialize]public bool IgnoreY { get; set; }

     
        public bool IsEmpty
        {
            get { return m_Velocity.IsEmpty(); }
        }

        public void ApplyTransformDelta(Vector3 positionDelta)
        {
            m_Position += positionDelta;
        }

        public void Reset()
        {
            m_Velocity.Reset();
            m_Accel.Reset();
        }

   public  enum PredictorUpdateTypes
        {
            Pos,
            Velocity,
            Accel
        }

        public void AddPosition(Vector3 pos, float deltaTime)
        {

            if (IsEmpty)
                m_Velocity.AddValue(Vector3.zero);
            else if (deltaTime > 9.99999974737875E-06)
            {
                Vector3 vector3 = m_Velocity.Value();
                Vector3 v       = (pos - m_Position) / deltaTime;
                if (IgnoreY) v.y = 0.0f;
                m_Velocity.AddValue(v);
                m_Accel.AddValue(v - vector3);
            }

            m_Position = pos;
        }

        public void AddPosition(Vector3 pos)
        {
            AddPosition(pos,Time.deltaTime);
        }

        public void AddVelocityAndPos(Vector3 pos,Vector3 velocity)
        {
            if (IsEmpty)
                m_Velocity.AddValue(Vector3.zero);
            else if (Time.deltaTime > 9.99999974737875E-06)
            {
                Vector3 vector3 = m_Velocity.Value();
             
                if (IgnoreY) velocity.y = 0.0f;
                m_Velocity.AddValue(velocity);
                m_Accel.AddValue(velocity - vector3);
            }

            m_Position = pos;
        }

        public void AddVelocityPosAccel(Vector3 pos, Vector3 velocity,Vector3 accel)
        {
            if (IsEmpty)
                m_Velocity.AddValue(Vector3.zero);
            else if (Time.deltaTime > 9.99999974737875E-06)
            {
            

                if (IgnoreY) velocity.y = 0.0f;
                m_Velocity.AddValue(velocity);
                m_Accel.AddValue(accel);
            }

            m_Position = pos;
        }

        public Vector3 PredictPosition(float lookaheadTime)
        {
            Vector3 position = m_Position;
            if (Time.deltaTime > 9.99999974737875E-06)
            {
                int     num1          = Mathf.Min(Mathf.RoundToInt(lookaheadTime / Time.deltaTime), 6);
                float   num2          = lookaheadTime / num1;
                Vector3 fromDirection = !m_Velocity.IsEmpty() ? m_Velocity.Value() : Vector3.zero;
                Vector3 vector3       = !m_Accel.IsEmpty() ? m_Accel.Value() : Vector3.zero;
                for (int index = 0; index < num1; ++index)
                {
                    position += fromDirection * num2;
                    Vector3 toDirection = fromDirection + vector3 * num2;
                    vector3 = Quaternion.FromToRotation(fromDirection, toDirection) * vector3;
                    fromDirection = toDirection;
                }
            }

            return position;
        }
    }
}