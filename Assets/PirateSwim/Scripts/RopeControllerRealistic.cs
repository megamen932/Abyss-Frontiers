namespace PirateSwim
{
    using System.Collections.Generic;
    using UnityEngine;

//A class that will hold information about each rope section


    [RequireComponent(typeof(LineRenderer))]
    public class RopeControllerRealistic : MonoBehaviour
    {
        public struct RopeSection
        {
            public Vector3 pos;
            public Vector3 vel;

            //To write RopeSection.zero
            public static readonly RopeSection zero = new RopeSection();

            public RopeSection(Vector3 pos = default(Vector3), Vector3 vel = default(Vector3))
            {
                this.pos = pos;
                this.vel = vel;
            }
        }

        //Objects that will interact with the rope
        public Transform whatTheRopeIsConnectedTo;

        //Line renderer used to display the rope
        //  public LineRenderer lineRenderer;

        //A list with all rope section
        List<RopeSection> allRopeSections;

        //Rope data
        [Range(0.01f, 150)] public float ropeSectionLength = 1f;

        //Data we can change to change the properties of the rope
        //Spring constant
        public float SpringForce = 40f;

        //Damping from rope friction constant
        public float Damping = 2f;

        //Damping from air resistance constant
        public float Resistance = 0.05f;

        //Mass of one rope section
        public float     massRopeSection = 0.2f;
        public int       segments        = 7;
        public bool      isBackDrop      = true;
        public float     segmentLength   = 10;
        public Transform target;


        public LineRenderer lineRenderer
        {
            get { return GetComponent<LineRenderer>(); }
        }

       

        public List<RopeSection> AllRopeSections
        {
            get { return allRopeSections; }
            set
            {
                for (var i = 0; i < value.Count; i++)
                {
                    var ropeSection = value[i];
                    if (Utils.IsVectorNaN(ropeSection.pos))
                    {
                        for (int k = 1, b = 1;
                            i + k < value.Count && i - b >= 0 &&
                            (Utils.IsVectorNaN(allRopeSections[i - b].pos) ||
                             Utils.IsVectorNaN(allRopeSections[i + k].pos));
                            ++k)
                        {
                            if (Utils.IsVectorNaN(allRopeSections[i - b].pos))
                            {
                                ++b;
                            }
                            else
                            {
                                ++k;
                            }

                            ropeSection.pos = (allRopeSections[i + k].pos - allRopeSections[i - b].pos) * .5f;
                        }


                        if (Utils.IsVectorNaN(ropeSection.vel))
                        {
                            for (int k = 1, b = 1;
                                i + k < value.Count && i - b >= 0 &&
                                (Utils.IsVectorNaN(allRopeSections[i - b].vel) ||
                                 Utils.IsVectorNaN(allRopeSections[i + k].vel));
                                ++k)
                            {
                                if (Utils.IsVectorNaN(allRopeSections[i - b].vel))
                                {
                                    ++b;
                                }
                                else
                                {
                                    ++k;
                                }

                                ropeSection.vel = (allRopeSections[i - b].vel - allRopeSections[i + k].vel) * .5f;
                            }
                        }
                    }
                }

                allRopeSections = value;
            }
        }

        void Start()
        {
            allRopeSections = new List<RopeSection>();
        }

        bool NoTargets => !target || !whatTheRopeIsConnectedTo;
        bool isReady   => !NoTargets && isInited;

        public void Init(Transform newTarget, Loot loot = null, bool _backDrop = false)
        {
            target = newTarget;


            if (NoTargets)
            {
                isInited = false;
                lineRenderer.enabled = false;
                return;
            }

            lineRenderer.enabled = true;

            isBackDrop = _backDrop;
            //
            //Create the rope
            //
            //Build the rope from the top


            segments = (int) (Mathf.Max(Vector3.Distance(whatTheRopeIsConnectedTo.position, target.position)
                                  , Vector3.Distance(target.position, loot.transform.position)) /
                              segmentLength);


            ropeSectionLength = segmentLength;

            AllRopeSections.Clear();

            if (segments <= 4)
            {
                segments = 4;
            }


            for (float s = 0; s <= 1; s += 1.0f / segments)
            {
                // Find the each segments position using the slope from above
                Vector3 vector = Vector3.Lerp(target.position, whatTheRopeIsConnectedTo.position,
                    isBackDrop ? s : s * s);
                AllRopeSections.Add(new RopeSection(vector));
            }


            isInited = true;
        }

        public bool LogDebug = false;
        bool        isInited = false;

        void Update()
        {
            if (!isReady)
            {
                return;
            }


            //Display the rope with the line renderer
//            Debug.Log("Display Rope");
            DisplayRope();

            //Compare the current length of the rope with the wanted length
            if (LogDebug)
                DebugRopeLength();

            //Move what is hanging from the rope to the end of the rope
        }




        [Range(1, 100)] [SerializeField] int iterations = 1;
        public float lightingSpeedCoef = 4f;
        float lightingSpeed;
        void FixedUpdate()
        {
            if (!isReady)
            {
                return;
            }

            if (AllRopeSections.Count <= 0)
            {
                return;
            }
            lineRenderer.material.mainTextureOffset+=lightingSpeed*Vector2.right;

            if (FXTimer < 0)
            {

                lineRenderer.material.mainTextureOffset += Vector2.right*.5f*Time.deltaTime;

                var scale = lineRenderer.material.mainTextureScale;
                var clamp = Mathf.Clamp(scale.y + Random.Range(-1.5f, 1.5f), 0.8f, 6.3f);
                if (clamp > 1.4 && Random.value < 0.5)
                {
                    clamp = Mathf.Clamp(scale.y, 0.6f, Random.Range(0.6f, 1.5f));
                }

                lineRenderer.material.mainTextureScale = new Vector2(scale.x, clamp);
                lightingSpeed = Time.deltaTime * clamp*lightingSpeedCoef ;
              if(Random.value>0.7)
              {
                  lightingSpeed *= -2;
              }
              
                FXTimer = 1 / LightingFrequency;
            }

            FXTimer -= Time.deltaTime;
            if (!isBackDrop)
            {
                for (int s = 0; s < AllRopeSections.Count; ++s)
                {
                    // Find the each segments position using the slope from above
                    float step = s / (float) segments;
                    Vector3 vector =
                        Vector3.Lerp(target.position, whatTheRopeIsConnectedTo.position, step * step);
                    var allRopeSection = AllRopeSections[s];
                    allRopeSection.pos = vector;
                    AllRopeSections[s] = allRopeSection;
                }

            }
            else
            {
                //Simulate the rope
                //How accurate should the simulation be?

                //Time step
                float timeStep = Time.fixedDeltaTime / (float) iterations;
                //      Debug.Log("before physics");

                for (int i = 0; i < iterations; i++)
                {
                    UpdateRopeSimulation(AllRopeSections, timeStep);
                }

                target.position = AllRopeSections[0].pos;

                //Make what's hanging from the rope look at the next to last rope position to make it rotate with the rope

                target.LookAt(AllRopeSections[1].pos);
            }
        }


        [SerializeField] bool invertVectorBetween;

        private void DisplayRope()
        {
            lineRenderer.positionCount = AllRopeSections.Count;

            for (int i = 0; i < AllRopeSections.Count; i++)
            {
                lineRenderer.SetPosition(i, AllRopeSections[i].pos);
            }
            lineRenderer.material.mainTextureOffset+=LightingVerticalSpeed*Mathf.Sign(-lightingSpeed)*Vector2.up*Time.deltaTime;
           
        }

        [SerializeField, Range(0, 40)] int   maximumStretchIterations = 15;
        [SerializeField, Range(0, 5)]  float LightingFrequency        = 3;
        [SerializeField]               float ropeGrabSpeed            = .3f;
        float                                FXTimer            = 0;
        [SerializeField] float LightingVerticalSpeed=1.1f;

        private void UpdateRopeSimulation(List<RopeSection> allRopeSections, float timeStep)
        {
            //Move the last position, which is the top position, to what the rope is attached to
            RopeSection lastRopeSection = allRopeSections[allRopeSections.Count - 1];

            lastRopeSection.pos = whatTheRopeIsConnectedTo.position;

            allRopeSections[allRopeSections.Count - 1] = lastRopeSection;


            //
            //Calculate the next pos and vel with Forward Euler
            //
            //Calculate acceleration in each rope section which is what is needed to get the next pos and vel
            List<Vector3> accelerations = CalculateAccelerations(allRopeSections);

            List<RopeSection> nextPosVelForwardEuler = new List<RopeSection>();


            //Loop through all line segments (except the last because it's always connected to something)
            for (int i = 0; i < allRopeSections.Count - 1; i++)
            {
                RopeSection thisRopeSection = RopeSection.zero;

                //Forward Euler
                //vel = vel + acc * t
                thisRopeSection.vel = allRopeSections[i].vel + accelerations[i] * timeStep;

                //pos = pos + vel * t
                thisRopeSection.pos = allRopeSections[i].pos + allRopeSections[i].vel * timeStep;

                //Save the new data in a temporarily list
                nextPosVelForwardEuler.Add(thisRopeSection);
            }

            //Add the last which is always the same because it's attached to something
            nextPosVelForwardEuler.Add(allRopeSections[allRopeSections.Count - 1]);


            //
            //Calculate the next pos with Heun's method (Improved Euler)
            //
            //Calculate acceleration in each rope section which is what is needed to get the next pos and vel
            List<Vector3> accelerationFromEuler = CalculateAccelerations(nextPosVelForwardEuler);

            List<RopeSection> nextPosVelHeunsMethod = new List<RopeSection>();

            //Loop through all line segments (except the last because it's always connected to something)
            for (int i = 0; i < allRopeSections.Count - 1; i++)
            {
                RopeSection thisRopeSection = RopeSection.zero;

                //Heuns method
                //vel = vel + (acc + accFromForwardEuler) * 0.5 * t
                thisRopeSection.vel = allRopeSections[i].vel +
                                      (accelerations[i] + accelerationFromEuler[i]) * 0.5f * timeStep;

                //pos = pos + (vel + velFromForwardEuler) * 0.5f * t
                thisRopeSection.pos = allRopeSections[i].pos +
                                      (allRopeSections[i].vel + nextPosVelForwardEuler[i].vel) * 0.5f * timeStep;

                //Save the new data in a temporarily list
                nextPosVelHeunsMethod.Add(thisRopeSection);
            }

            //Add the last which is always the same because it's attached to something
            nextPosVelHeunsMethod.Add(allRopeSections[allRopeSections.Count - 1]);


            //From the temp list to the main list
            for (int i = 0; i < allRopeSections.Count; i++)
            {
                AllRopeSections[i] = nextPosVelHeunsMethod[i];

                //allRopeSections[i] = nextPosVelForwardEuler[i];
            }


            //Implement maximum stretch to avoid numerical instabilities
            //May need to run the algorithm several times


            for (int i = 0; i < maximumStretchIterations; i++)
            {
                ImplementMaximumStretch(allRopeSections);
            }
        }

        //Calculate accelerations in each rope section which is what is needed to get the next pos and vel
        private List<Vector3> CalculateAccelerations(List<RopeSection> allRopeSections)
        {
            List<Vector3> accelerations = new List<Vector3>();

            //Spring constant
            float k = SpringForce;
            //Damping constant
            float d = Damping;
            //Damping constant from air resistance
            float a = Resistance;
            //Mass of one rope section
            float m = massRopeSection;
            //How long should the rope section be
            float wantedLength = ropeSectionLength;


            //Calculate all forces once because some sections are using the same force but negative
            List<Vector3> allForces = new List<Vector3>();

            for (int i = 0; i < allRopeSections.Count - 1; i++)
            {
                //From Physics for game developers book
                //The force exerted on body 1
                //pos1 (above) - pos2
                Vector3 vectorBetween = allRopeSections[i + 1].pos - allRopeSections[i].pos;
                if (invertVectorBetween)
                {
                    vectorBetween = -vectorBetween;
                }

                float distanceBetween = vectorBetween.magnitude;

                Vector3 dir = vectorBetween.normalized;

                float springForce = k * (distanceBetween - wantedLength);


                //Damping from rope friction 
                //vel1 (above) - vel2
                float frictionForce =
                    d * ((Vector3.Dot(allRopeSections[i + 1].vel - allRopeSections[i].vel, vectorBetween)) /
                         distanceBetween);


                //The total force on the spring
                Vector3 springForceVec = -(springForce + frictionForce) * dir;

                //This is body 2 if we follow the book because we are looping from below, so negative
                springForceVec = -springForceVec;

                allForces.Add(springForceVec);
            }

            allForces[0] += target.forward * Time.deltaTime;


            //Loop through all line segments (except the last because it's always connected to something)
            //and calculate the acceleration
            for (int i = 0; i < allRopeSections.Count - 2; i++)
            {
                Vector3 springForce = Vector3.zero;

                //Spring 1 - above
                springForce += allForces[i];

                //Spring 2 - below
                //The first spring is at the bottom so it doesnt have a section below it
                if (i != 0)
                {
                    springForce -= allForces[i + 1];
                }

                //Damping from air resistance, which depends on the square of the velocity
                float vel = allRopeSections[i].vel.magnitude;

                Vector3 dampingForce = a * vel * vel * allRopeSections[i].vel.normalized;

                //The mass attached to this spring
                float springMass = m;

                //end of the rope is attached to a box with a mass
                if (target.GetComponent<Rigidbody>())
                    if (i == 0)
                    {
                        springMass += target.GetComponent<Rigidbody>().mass;
                    }

                //Force from gravity
                Vector3 gravityForce = springMass * Physics.gravity;


                //The total force on this spring
                Vector3 totalForce = springForce + gravityForce - dampingForce;

                //Calculate the acceleration a = F / m
                Vector3 acceleration = totalForce / springMass;

                accelerations.Add(acceleration);
            }

            //The last line segment's acc is always 0 because it's attached to something
            accelerations.Add(Vector3.zero);


            return accelerations;
        }

        //Implement maximum stretch to avoid numerical instabilities
        private void ImplementMaximumStretch(List<RopeSection> allRopeSections)
        {
            //Make sure each spring are not less compressed than 90% nor more stretched than 110%
            float maxStretch = 1.1f;
            float minStretch = 0.9f;

            //Loop from the end because it's better to adjust the top section of the rope before the bottom
            //And the top of the rope is at the end of the list
            for (int i = allRopeSections.Count - 1; i > 0; i--)
            {
                RopeSection topSection = allRopeSections[i];

                RopeSection bottomSection = allRopeSections[i - 1];

                //The distance between the sections
                float dist = (topSection.pos - bottomSection.pos).magnitude;

                //What's the stretch/compression
                float stretch = dist / ropeSectionLength;

                if (stretch > maxStretch)
                {
                    //How far do we need to compress the spring?
                    float compressLength = dist - (ropeSectionLength * maxStretch);

                    //In what direction should we compress the spring?
                    Vector3 compressDir = (topSection.pos - bottomSection.pos).normalized;

                    Vector3 change = compressDir * compressLength;

                    MoveSection(change, i - 1);
                }
                else if (stretch < minStretch)
                {
                    //How far do we need to stretch the spring?
                    float stretchLength = (ropeSectionLength * minStretch) - dist;

                    //In what direction should we compress the spring?
                    Vector3 stretchDir = (bottomSection.pos - topSection.pos).normalized;

                    Vector3 change = stretchDir * stretchLength;

                    MoveSection(change, i - 1);
                }
            }
        }

        //Move a rope section based on stretch/compression
        private void MoveSection(Vector3 finalChange, int listPos)
        {
            RopeSection bottomSection = AllRopeSections[listPos];

            //Move the bottom section
            Vector3 pos = bottomSection.pos;

            pos += finalChange;

            bottomSection.pos = pos;

            AllRopeSections[listPos] = bottomSection;
        }

        //Compare the current length of the rope with the wanted length
        private void DebugRopeLength()
        {
            float currentLength = 0f;
            float wantedLength  = ropeSectionLength * (float) (AllRopeSections.Count);
            for (var i = AllRopeSections.Count - 1; i >= 1; i--)
            {
                float thisLength = (AllRopeSections[i].pos - AllRopeSections[i - 1].pos).magnitude;

                currentLength += thisLength;
            }

            Debug.Log("Wanted: " + wantedLength + " Actual: " + currentLength);
        }

        float Length => Vector3.Distance(target.position, whatTheRopeIsConnectedTo.position);

        public void Grab(float amount)
        {
            var speed = (2 * (Mathf.Max(Length, 30f) / 220f) * ropeGrabSpeed / amount) * Time.deltaTime;
            lineRenderer.material.mainTextureOffset-=speed*2*Vector2.right ;
            //   lineRenderer.material.mainTextureScale=new Vector2( lineRenderer.material.mainTextureScale.x,1+Mathf.Cos(Time.time*.1f)*3);
            ropeSectionLength = Mathf.Clamp(ropeSectionLength - speed, 0.1f, 1000f);
        }
    }
}