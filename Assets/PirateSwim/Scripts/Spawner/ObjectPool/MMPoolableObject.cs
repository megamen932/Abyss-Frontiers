﻿using PirateSwim;
using UnityEngine;

namespace MoreMountains.Tools
{
	public interface IMMPoolableObject:IMMObjectBounds
	{
		event MMPoolableObject.Events OnSpawnComplete;


		/// <summary>
		/// Turns the instance inactive, in order to eventually reuse it.
		/// </summary>
		void Destroy();

		/// <summary>
		/// Triggers the on spawn complete event
		/// </summary>
		void TriggerOnSpawnComplete();

		/// <summary>
		/// When this component is added we define its bounds.
		/// </summary>
		void Reset();
	}

	/// <summary>
	/// Add this class to an object that you expect to pool from an objectPooler. 
	/// Note that these objects can't be destroyed by calling Destroy(), they'll just be set inactive (that's the whole point).
	/// </summary>
	[RequireComponent(typeof(MMObjectBounds))]
	public class MMPoolableObject : MonoBehaviour, IMMPoolableObject
	{
		public delegate void Events();
		public event Events OnSpawnComplete;

		MMObjectBounds _bounds;
		public Bounds ObjBounds => _bounds.ObjBounds;
		public void Reset()
		{
			if (_bounds == null)
			{
				this.GetOrAdd<MMObjectBounds>();
			}

			_bounds.Reset();
		}

		/// The life time, in seconds, of the object. If set to 0 it'll live forever, if set to any positive value it'll be set inactive after that time.
		public float LifeTime = 0f;

		/// <summary>
		/// Turns the instance inactive, in order to eventually reuse it.
		/// </summary>
		public virtual void Destroy()
		{
			StopAllCoroutines();
			gameObject.SetActive(false);
			
		}

		void Awake()
		{
			_bounds = this.GetOrAdd<MMObjectBounds>();
		}


		/// <summary>
		/// When the objects get enabled (usually after having been pooled from an ObjectPooler, we initiate its death countdown.
		/// </summary>
	    protected virtual void OnEnable()
		{
			
			Reset();
			if (LifeTime>0)
			{
				Invoke("Destroy", LifeTime);	
			}
		}

		/// <summary>
		/// When the object gets disabled (maybe it got out of bounds), we cancel its programmed death
		/// </summary>
	    protected virtual void OnDisable()
		{
			CancelInvoke();
		}

		/// <summary>
		/// Triggers the on spawn complete event
		/// </summary>
		public void TriggerOnSpawnComplete()
		{
			if(OnSpawnComplete != null)
			{
				OnSpawnComplete();
			}
		}
	}
}
