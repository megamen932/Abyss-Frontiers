using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using DG.Tweening;
using JetBrains.Annotations;
using MoreMountains.Tools;
using PirateSwim.Scripts.ShipScripts;
using PirateSwim.ShipScripts;
using ProceduralToolkit;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace PirateSwim
{
    public class ObjectCreator : MonoBehaviour
    {
        const                      int   maxErrorsCountOnSpawn = 3;
        [Range(0.01f, 120)] public float normalTimeCreation    = 5;
        float                            CreationTimer;
        public           Ship            ship;
        [SerializeField] AnimationCurve  RateOverDeep = AnimationCurve.EaseInOut(0, 0.1f, 1, 1f);
        public           Vector3         MinimumSize  = new Vector3(.5f, .5f, .5f) ;
        public           Vector3         MaximumSize  = new Vector3(3,   3,   3) ;

        public bool NoRandom;

        /// the minimum gap bewteen two spawned objects
        public Vector3 MinimumGap = new Vector3(1, 1, 1);

        /// the maximum gap between two spawned objects
        public Vector3 MaximumGap = new Vector3(1, 1, 1);

        public Vector3 MinimumRotation ;

        /// the maximum size of a spawned object
        public Vector3 MaximumRotation = Vector3.one * 360 ;

        /// the maximum size of a spawned object
        /// if set to true, the random size will preserve the original's aspect ratio
        public bool PreserveRatio  ;

        [Range(0, 500)] public                     int       spawnCount            = 3;
        [Range(0, 500), SerializeField]            int       MaxSpawnCountPerFrame = 10;
        [TabGroup("Settings", "Colliding")] public LayerMask layers;
        [TabGroup("Settings", "Colliding")] public bool      AddMeshColliderIfNoCollider  ;


        public Transform parent;

        [TabGroup("Settings", "Group"), SerializeField]
        public bool CreateGroup  ;

        [SerializeField, ReadOnly]
        int currentObjCount  ;

        [TabGroup("Settings", "Colliding"), SerializeField]
        bool isCheckForCollisions  ;

        [TabGroup("Settings", "Gravity"), SerializeField]
        float gravCoof = 3;

        [TabGroup("Settings", "Gravity"), SerializeField]
        bool NewGravityOnSpawn  ;

        [TabGroup("Settings", "Gravity"), SerializeField]
        Vector3 gravity  ;

        [TabGroup("Settings", "Gravity"), SerializeField]
        bool UseGravity;

        [TabGroup("Settings", "Group"), SerializeField]
        float chanceOfnewGroup;

        [TabGroup("Settings", "Group"), SerializeField, MinValue(0)]
        int MaxGroupCount = 6;

        [TabGroup("Settings", "Group"), SerializeField, ReadOnly]
        int GroupCount  ;

        [TabGroup("Settings", "Group"), SerializeField]
        Transform groupeChildRoot;

        public List<Group> in_completeGroupes = new List<Group>();
        public List<Group> completeGroupes    = new List<Group>();

        public BoundsTypes bounds;

        [TabGroup("Surface", "Sphere"), Wrap(0, 360), SerializeField]
        float SphereAngle = 60;

        [TabGroup("Surface", "Pyramid")] [SerializeField]
        Vector3 LootCoofSize;


        Quaternion                                          groupRot  ;
        [NotNull] [SerializeField] protected MMObjectPooler _objectPooler;

        [SerializeField, ReadOnly]
        Transform lastOjectCreated;

        [SerializeField] bool  debugErrors  ;
        public           Color color       = Color.red;


        void Awake()
        {
            _objectPooler = GetComponent<MMObjectPooler>();

            CreationTimer = 0f;


            if (!_objectPooler)
            {
                Debug.LogError(gameObject.name + "No object pool set to this spawner");
            }
        }


        void FixedUpdate()
        {
            if (CreationTimer < 0)
            {
                StartCoroutine(StartSpawing());
            }


            CreationTimer -= Time.deltaTime;
        }

      public  bool destroyOnSpawn  ;
        IEnumerator StartSpawing()
        {
          

            if (NewGravityOnSpawn)
            {
                var norm =      Random.insideUnitCircle.ToVector3XY();
                gravity = norm * gravCoof;
            }


            groupRot      = Random.rotation;
            CreationTimer = Random.Range(0.25f, 1.75f) * normalTimeCreation;


            var _spawnCount = spawnCount * RateOverDeep.Evaluate(ship.deepLevelNorm);
            if (NoRandom)
            {
                CreationTimer = normalTimeCreation;
                //    _spawnCount = spawnCount;
            }


            if (MaxSpawnCountPerFrame >= _spawnCount)
            {
                for (var j = 0; j < _spawnCount; j++)
                {
                    GenerateNewObject();
                }
            } else
            {
                for (var i = 0; i < _spawnCount / MaxSpawnCountPerFrame; ++i)
                {
                    for (var j = 0; j < MaxSpawnCountPerFrame; j++)
                    {
                        GenerateNewObject();
                    }


                    yield return null;
                }
            }
        }


        void GenerateNewObject()
        {
            var prefab =  _objectPooler.GetPooledGameObject();


            if (prefab == null)
            {
                return ;
            }


            Vector3 pos;


            Quaternion rotation;


            if (!GetPos(prefab, bounds, out pos, out rotation))
            {
                prefab.SetActive(false);


                return;
            }

            var spawn = Spawn(prefab, pos, rotation);
            spawn.GetComponent<Initable>()?.Init();
            var obj = spawn.GetComponent<MapObject>();
            if (!obj)
            {
                return;
            }

            if (destroyOnSpawn)
            {
                DOVirtual.DelayedCall(Mathf.Max(0,CreationTimer-0.1f), () => obj.Destroy());
            }
            bool inGroup = false;


            if (CreateGroup)
            {
                if (in_completeGroupes.Count > 0)
                {
                    var index = 0;

                    var groupe = in_completeGroupes[index];

                    groupe.AddMember(obj);
                    inGroup = true;


                    if (groupe.countOfMemebersFree <= 0)
                    {
                        completeGroupes.Add(groupe);
                        in_completeGroupes.Remove(groupe);
                    }
                }
            }


            var rock = obj.GetComponent<Rock>();


            if (rock != null)
            {
                rock.Init(groupRot, gravity, UseGravity);
            } else
            {
                var loot = prefab.GetComponentInGOAndChildren<Loot>();


                if (loot)
                {
                    loot.Init();
                }
            }


            obj.gameObject.SetActive(true);


            //Create new Group
            if (!CreateGroup || Group.groups.Count > MaxGroupCount || inGroup || Random.value >= chanceOfnewGroup)
            {
                return;
            }


            GameObject newOne = new GameObject("Group(" + Group.groups.Count + ")");
            newOne.transform.parent = groupeChildRoot;
            Group newGroup = newOne.AddComponent<Group>();
            newGroup.RandomInit();
            newGroup.AddMember(obj);
            inGroup = true;
            in_completeGroupes.Add(newGroup);
        }


        public enum BoundsTypes
        {
            Sphere,
            Box,
            TurnSide
        }


        public bool GetPos(GameObject prefab, BoundsTypes type, out Vector3 vec, out Quaternion rotation)
        {
            bool isCollision = isCheckForCollisions;


            int errors = -1;


            rotation = Quaternion.Euler(Random.Range (MinimumRotation.x, MaximumRotation.x),
                Random.Range (MinimumRotation.y, MaximumRotation.y), Random.Range (MinimumRotation.z, MaximumRotation.z));


            if (!PreserveRatio)
            {
                prefab.transform.localScale = new Vector3 (Random.Range (MinimumSize.x, MaximumSize.x),
                    Random.Range (MinimumSize.y, MaximumSize.y), Random.Range (MinimumSize.z, MaximumSize.z));
            } else
            {
                prefab.transform.localScale = Vector3.one * Random.Range (MinimumSize.x, MaximumSize.x);
            }


            var poolObj = prefab.GetComponent<MMPoolableObject>();
            
          


            do

            {
                switch (type)
                {
                    case BoundsTypes.Sphere:
                        vec = GetPosFromSphere(prefab, rotation);


                        break;
                    case BoundsTypes.Box:
                        vec = GetPosFromBox(prefab, rotation);


                        break;
                    case BoundsTypes.TurnSide:
                        vec = GetPosFromTurn(prefab, rotation);


                        break;


                    default:


                        throw new ArgumentOutOfRangeException(nameof(type), type, null);
                }


                ++errors;
                if (Utils.IsVectorNaN(vec))
                {
                    ++errors;
                    vec = Vector3.zero;
                    continue;
                }


                if (UseGravity)
                {
                    float gravTime = Mathf.Abs(ship.transform.position.z - vec.z) / ship.speed;
                    var   shift    = gravity * gravTime * gravTime                / 2;
                    shift.z =  Mathf.Abs(shift.z);
                    vec     -= shift;
                }


                //   prefab.GetComponent<MMObjectBounds>();


                prefab.transform.rotation = rotation;


                var boundsExtents = poolObj.ObjBounds.extents;


                isCollision = isCheckForCollisions && Physics.CheckBox(vec, boundsExtents, prefab.transform.rotation, layers.value);


                if (errors > maxErrorsCountOnSpawn)
                {
                    if (debugErrors)   Debug.Log("Too many count of errors when gen errors:" + errors);


                    return false;
                }
            } while (isCheckForCollisions && isCollision);


            return true;
        }


        public Vector3 GetPosFromSphere([NotNull] GameObject prefab, Quaternion rotation)
        {
            var   b            = Rnd(SphereAngle) * Mathf.Deg2Rad;
            var   a            = Rnd(SphereAngle)  * Mathf.Deg2Rad;
            float RandomRadius = Random.Range(ship.innerSphere.radius, ship.outerSphere.radius) + offset;
            var   x            = RandomRadius * Mathf.Sin(a) * Mathf.Cos(b);
            var   y            = RandomRadius * Mathf.Sin(a) * Mathf.Sin(b);
            var   z            = RandomRadius                * Mathf.Cos(a);

            var vec = ship.transform.position + ship.transform.rotation * new Vector3(x, y, z);


            //    vec = ship.outerSphere.GetComponent<SphereCollider>().ClosestPoint(vec);
            return vec;
        }


        [SerializeField] RndType rndType;

        public enum RndType
        {
            ExpRight,
            Linear,
            NormalShift,
            NormalSize,
            ExpLeft
        }

        float Rnd(float size)
        {
            switch (rndType)
            {
                case RndType.ExpRight:
                    return   RandomFromDistribution.RandomRangeExponential(-size, size, Shift, RandomFromDistribution.Direction_e.Right);
                    

                case RndType.NormalShift:
                    return RandomFromDistribution.RandomNormalDistribution(Shift, size);
                case RndType.NormalSize:
                    return  RandomFromDistribution.RandomNormalDistribution(size, Shift);
                case RndType.ExpLeft:
                    return   RandomFromDistribution.RandomRangeExponential(-size, size, Shift, RandomFromDistribution.Direction_e.Left);
                default:
                    return Random.Range(-size, size);
            }
        }

            FileStream file  ;
            StreamWriter writer  ;
         
     

        void OnDestroy()
        {
            if (writer != null)
            {
                writer.Close();
            }
        }

        void OnApplicationQuit()
        {
            if (writer != null)
            {
                writer.Close();
            }
        }

        Vector3 GetPosFromBox([NotNull] GameObject prefab, Quaternion rotation)
        {
            var   boundsExtents = prefab.GetComponent<MMObjectBounds>().ObjBounds.extents;
            float t             = ship.TimeToAbostacles;


            var CrateSize =  new Vector3(ship.SwipingSideSpeed.x * LootCoofSize.x * t, LootCoofSize.y * ship.SwipingSideSpeed.y * t, LootCoofSize.z) ;


            var Area = CrateSize - rotation * boundsExtents;

            var x = Rnd(Area.x);
            var y =Rnd(Area.y);
            var z = Random.Range(0, LootCoofSize.z);


            var Target = ship.transform.rotation * new Vector3(x, y, z) + offset * ship.transform.forward ;


            return Target;
        }


        Vector2                _turnAngleDeg = new Vector2(45, 45);
        Vector2                turnAngleRad => _turnAngleDeg * Mathf.Deg2Rad;
        Vector3                lastPos;
        float                  TotalZoffset;
        [SerializeField] float Shift;


        Vector3 lastMax = Vector3.left;


        Vector2 lastAngle  ;
        Vector3 startPos  ;
        float   accurate = 2f;

        Vector3 GetPosFromTurn(GameObject prefab, Quaternion rotation)
        {
            if (lastMax == Vector3.left)
            {
                startPos = ship.ShipPosAtTime(ship.TimeToAbostacles);
                startPos.x -= (ship.MinZObstaclesOffset) * Mathf.Tan(turnAngleRad.x) ;
                lastMax = startPos;
                lastPos = lastMax;
            } else if (Vector2.Distance(lastAngle, _turnAngleDeg) > accurate)
            {
                startPos = lastPos;
                lastAngle = _turnAngleDeg;
            }


            var obj_bounds = prefab.GetComponent<MMObjectBounds>().ObjBounds;

            var extens = rotation * obj_bounds.extents;


            extens.x *= turnAngleRad.x > 0 ? 1 : -1;


            var zffset = extens.z + lastMax.z;
            var xffset = startPos.x + (TotalZoffset) * Mathf.Tan(turnAngleRad.x) - extens.x;
            var yffset = startPos.y + (TotalZoffset) * Mathf.Tan(turnAngleRad.y) - extens.y;

            TotalZoffset = (lastPos - startPos).z;
            lastPos = new Vector3(xffset, yffset, zffset);
            Debug.DrawLine(lastPos, lastMax);
            lastMax = lastPos + extens;
            Debug.DrawLine(lastPos, lastMax, Color.red);


            return lastPos;
        }


        //   float curShft=0;
        [SerializeField] float offset  ;


        Vector3 GetPosFromTurnCurve(GameObject prefab, Quaternion rotation)
        {
            if (lastMax == Vector3.left)
            {
                startPos = ship.transform.position;
                lastMax  =  ship.ShipPosAtTime(ship.TimeToAbostacles);

                lastPos  = lastMax;
            }


            var obj_bounds = prefab.GetComponent<MMObjectBounds>().ObjBounds;

            var extens = rotation * obj_bounds.extents;


            // lastPos = spline.MoveAlongSpline( ref curShft, extens.magnitude*2 );


            return lastPos;
        }


        [NotNull]
        GameObject Spawn([NotNull] GameObject prefab, Vector3 pos, Quaternion rotaion)
        {
            // var localPos = ship.transform.InverseTransformPoint(pos);

            //  localPos.z = Mathf.Max(RenderSettings.fogEndDistance + LootCoofSize.z, localPos.z);
            //  prefab.transform.position = ship.transform.TransformPoint(localPos);

            Utils.SetLayerRecursive(prefab, gameObject.layer);
            prefab.transform.parent =  transform.GetChild(0);
         //   prefab.name             += " " + ++currentObjCount;
            MeshRenderer meshRenderer = prefab.SafeGetComponentInGOAndChildren<MeshRenderer>();
            

            //   if(Vector3.Distance(pos, ship.transform.position)<RenderSettings.fogEndDistance)
            //           Debug.LogError(Vector3.Distance(pos,ship.transform.position));


            lastOjectCreated = prefab.transform;
            prefab.transform.position = pos;
            prefab.SetActive(true);
            if (meshRenderer != null)
            {
                var material = meshRenderer.material;
                if (material != null)
                {
                    material.color = color;
                }

                while (meshRenderer.isVisible)
                {
                    transform.position += Vector3.forward;
                }

               
            }


            return prefab;
        }
    }
}