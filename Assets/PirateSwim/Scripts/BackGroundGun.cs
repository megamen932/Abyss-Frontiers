﻿using System.Collections;
using DG.Tweening;
using PirateSwim.Scripts.ShipScripts;
using UnityEngine;

namespace PirateSwim
{
    public class BackGroundGun : Weapon
    {
        // Use this for initialization
        public int burstCount = 15;
        Ship       _ship;

        [SerializeField] Weapon _cannon;
        [SerializeField] float  AimingDuration = 5;
        [SerializeField] float  predictionTime = 5;


        protected override void Start ()
        {
            base.Start();
            _ship = Ship.Main;
            _cannon.ShootPlace = ShootPlace;
            _cannon.ShellPrefab = ShellPrefab;

     
        }

        void Update()
        {
            if (Input.GetKey(KeyCode.B))
            {
                StartCoroutine(  Shoot());
            }

            if (Input.GetKey(KeyCode.G))
            {
                StopAllCoroutines();
            
            }

      
        }



        protected override IEnumerator Shoot()
        {
            if (isShooting)
            {
                yield break;
            }

            isShooting = true;

            if(AimingDuration>0)
                yield return transform.DORotate(_ship.ShipPosAtTime(predictionTime+ AimingDuration), AimingDuration).WaitForCompletion();


       
      
            _cannon.ShootPlace.LookAt(_ship.ShipPosAtTime(predictionTime));
            //    _cannon.OnShoot += () => { (_cannon.LastSpell as BackgroundSpell)?.Init(SpellSetting); };
            // Debug.Log(Time.time);
         
            
            _cannon.autoShoot=true;
            yield return new WaitForSeconds(_cannon.ReloadTime*burstCount);
            _cannon.autoShoot=false;
            

            //  Debug.Log(" 2 "+ Time.time);
       

            isShooting = false;
        }
    }
}