﻿using UnityEngine;

public class RotateHandle : MonoBehaviour {


	public Vector2        speedOfRotaion = Vector2.one;
	public AnimationCurve curve          = AnimationCurve.EaseInOut(0, 1, 0, 0.4f);
	public RectTransform handle;

	void LateUpdate()
	{

		var euler_angles    = handle.eulerAngles;
		euler_angles.x              = Input.GetAxis("Vertical")   * speedOfRotaion.y;
		euler_angles.y              = - Input.GetAxis("Horizontal") * speedOfRotaion.y;
		handle.eulerAngles             = euler_angles;

		handle.Rotate(Vector3.forward, -curve.Evaluate(Mathf.Abs(Input.GetAxis("Horizontal"))) * Mathf.Sign(Input.GetAxis("Horizontal")) * speedOfRotaion.x);
	}

}
