using UnityEngine;

namespace CastleToCastle
{
    public class SlowRotation : MonoBehaviour
    {
        public float speed=1;
        public float angle=15;
        public Vector3 axis=Vector3.right;

        void FixedUpdate()
        {
            transform.Rotate(axis,angle*Time.deltaTime*Mathf.Cos(Time.time*Mathf.PI)*speed);
        }
    }
}