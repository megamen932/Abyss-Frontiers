using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using Enumerable = System.Linq.Enumerable;
using Random = UnityEngine.Random;

namespace PirateSwim
{
    public static class Utils
    {
        #region Generic

        public  static Tween Invoke(float delay, TweenCallback call)
        {
            return  DOVirtual.DelayedCall(delay, call, false);
        }

        public static void SetLayerRecursive([NotNull] GameObject whom, int layer)
        {
            whom.layer = layer;

            var transes = whom.GetComponentsInChildren<Transform>(true);
            System.Diagnostics.Debug.Assert(transes != null, nameof(transes) + " != null");
            for (var index = 0; index < transes.Length; index++)
            {
                transes[index].gameObject.layer = layer;
            }
        }

        #endregion

        #region Math

        public static float Hypothenys(float a, float b)
        {
            return Mathf.Sqrt(Mathf.Pow(a, 2) + Mathf.Pow(b, 2));
        }

        public static bool NotZero(float value)
        {
            return !(Math.Abs(value) < Mathf.Epsilon);
        }

        public static float ToRad(this float x) => x * Mathf.Deg2Rad;
        public static float ToDeg(this float x) => x * Mathf.Rad2Deg;

        #endregion

        #region Vector

        public static float Dot(this Vector3 x, Vector3 lhs) => Vector3.Dot(x, lhs);

        public static float Normalize(float value, float max, float startOffsetPercentage = 0) => (value + max * startOffsetPercentage) / max;


        public static Vector3 CalcCenterList([NotNull] IEnumerable<MonoBehaviour> members)
        {
            System.Diagnostics.Debug.Assert(members != null, nameof(members) + " != null");
            if (!members.Any())
            {
                return Vector3.zero;
            }

            return CalcCenterList(Enumerable.Select(members, (x => x.transform.position)));
            //   var vec = new Vector3(members.Sum((m) => m.transform.position.x), members.Sum((m) => m.transform.position.y),
            //              members.Sum((m) => m.transform.position.z)) / members.Count;
        }

        public static Vector3 CalcCenterList([NotNull] IEnumerable<Vector3> members)
        {
            System.Diagnostics.Debug.Assert(members != null, nameof(members) + " != null");
            if (!members.Any())
            {
                return Vector3.zero;
            }

            var vec = members.Sum(x => x, Vector3Adder.Instance) / members.Count();
            //   var vec = new Vector3(members.Sum((m) => m.transform.position.x), members.Sum((m) => m.transform.position.y),
            //              members.Sum((m) => m.transform.position.z)) / members.Count;


            return IsVectorNaN(vec) ? Vector3.zero : vec;
        }

        public interface IAdder<T>
        {
            T Zero { get; }

            T Add(T a, T b);
        }

        struct Vector3Adder : IAdder<Vector3>
        {
            static IAdder<Vector3> _instance;


            public Vector3 Zero =>  Vector3.zero;

            public static IAdder<Vector3> Instance
            {
                get
                {
                    if (_instance == null)
                    {
                        _instance = new Vector3Adder();
                    }

                    return _instance;
                }
            }

            public Vector3 Add(Vector3 a, Vector3 b)
            {
                return a + b;
            }
        }

        public static TValue Sum<TSource, TValue>(this IEnumerable<TSource> source, Func<TSource, TValue> selector, IAdder<TValue> adder)
        {
            return Enumerable.Select(source, selector).Sum(adder);
        }

        public static T Sum<T>(this IEnumerable<T> source, IAdder<T> adder)
        {
            if (source == null) throw new ArgumentNullException("Sum source is null");
            T num1 = adder.Zero;
            return source.Aggregate( num1, adder.Add);
        }

        public  static Vector3 Mul(Vector3 a, Vector3 b) =>  new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
        public  static Vector3 Div(Vector3 a, Vector3 b) =>  new Vector3(a.x / b.x, a.y / b.y, a.z / b.z);
        public  static Vector2 Mul(Vector2 a, Vector2 b) =>  new Vector2(a.x * b.x, a.y * b.y);
        public  static Vector2 Div(Vector2 a, Vector2 b) =>  new Vector2(a.x / b.x, a.y / b.y);


        public static bool IsVectorNaN(Vector3 vec)
        {
            return float.IsNaN(vec.x) || float.IsNaN(vec.y) || float.IsNaN(vec.z);
        }

        public static bool IsNaN(this Vector3 vec)
        {
            return float.IsNaN(vec.x) || float.IsNaN(vec.y) || float.IsNaN(vec.z);
        }

        #endregion

        #region Get Component

        public static T GetComponentInGOAndParent<T>(this GameObject whom, bool require = true)
        {
            var component = whom.GetComponent<T>();
            if (component == null) component = whom.GetComponentInParent<T>();

            CheckNotNull(require, component);

            return component;
        }

        static void CheckNotNull<T>(bool require, T component)
        {
            if (component == null && require)
            {
                Debug.LogError("Ожидается компонент типа " + typeof(T) + ", но он отсутствует ");
            }
        }


      [NotNull]  public static T GetOrAdd<T>(this GameObject whom) where T : Component
        {
            var t = whom.GetComponent<T>();
            if (t == null) t = whom.AddComponent<T>();


            return t;
        }

      [NotNull]  public static T GetOrAdd<T>(this MonoBehaviour whom) where T : Component
        {
            var t = whom.GetComponent<T>();
            if (t == null) t = whom.gameObject.AddComponent<T>();


            return t;
        }


        public static T GetComponentInGOAndChildren<T>(this GameObject whom, bool require = true)
        {
            var component = whom.GetComponent<T>();


            if (component == null || component.ToString() == "null")
            {
                component = whom.GetComponentInChildren<T>();
            }

            CheckNotNull(require, component);


            return component;
        }

        public static T SafeGetComponentInGOAndChildren<T>(this GameObject whom) where T : Component
        {
            var component = whom.GetComponent<T>();


            if (component == null || component.ToString() == "null")
            {
                component = whom.GetComponentInChildren<T>();
            }


            if (component == null)
            {
                component = whom.AddComponent<T>();
            }


            return component;
        }

        #endregion

        #region Debug

        public static void DrawText(
            GUISkin guiSkin,
            string  text,
            Vector3 position,
            Color?  color    = null,
            int     fontSize = 0,
            float   yOffset  = 0
        )
        {
#if UNITY_EDITOR
            var prevSkin = GUI.skin;
            if (guiSkin == null)
                Debug.LogWarning("editor warning: guiSkin parameter is null");
            else
                GUI.skin = guiSkin;

            GUIContent textContent = new GUIContent(text);

            GUIStyle style = (guiSkin != null) ? new GUIStyle(guiSkin.GetStyle("Label")) : new GUIStyle();
            if (color != null) style.normal.textColor = (Color) color;
            if (fontSize > 0) style.fontSize = fontSize;

            Vector2 textSize    = style.CalcSize(textContent);
            Vector3 screenPoint = Camera.current.WorldToScreenPoint(position);

            if (screenPoint.z > 0) // проверка, необходимая чтобы текст не был виден, если  камера направлена в противоположную сторону относительно объекта
            {
                var worldPosition = Camera.current.ScreenToWorldPoint(new Vector3(screenPoint.x - textSize.x * 0.5f,
                    screenPoint.y + textSize.y * 0.5f + yOffset, screenPoint.z));
                Handles.Label(worldPosition, textContent, style);
            }

            GUI.skin = prevSkin;
#endif
        }

        public static void DrawString(string text, Vector3 worldPos, Color? colour = null)
        {
            Handles.BeginGUI();

            var restoreColor = GUI.color;

            if (colour.HasValue) GUI.color = colour.Value;
            var view = SceneView.currentDrawingSceneView;

            Vector3 screenPos = view.camera.WorldToScreenPoint(worldPos);

            if (screenPos.y < 0 || screenPos.y > Screen.height || screenPos.x < 0 || screenPos.x > Screen.width || screenPos.z < 0)
            {
                GUI.color = restoreColor;
                Handles.EndGUI();
                return;
            }

            Vector2 size = GUI.skin.label.CalcSize(new GUIContent(text));
            GUI.Label(new Rect(screenPos.x - (size.x / 2), -screenPos.y + view.position.height + 4, size.x, size.y), text);
            GUI.color = restoreColor;
            Handles.EndGUI();
        }

        #endregion

        #region Lerper

        public static IEnumerator RotateTo(Transform whom, float RotSpeed, Quaternion rotation, bool Slerp)
        {
            for (float i = 0; i < 1; i += Time.deltaTime * RotSpeed)
            {
                if (Slerp)
                {
                    whom.rotation = Quaternion.Slerp(whom.rotation, rotation, Time.deltaTime * RotSpeed);
                } else
                {
                    whom.rotation = Quaternion.Lerp(whom.rotation, rotation, Time.deltaTime * RotSpeed);
                }


                yield return null;
            }
        }

        public static IEnumerator MoveTo(Transform whom, float Speed, Vector3 position, bool Slerp)
        {
            for (float i = 0; i < 1; i += Time.deltaTime * Speed)
            {
                if (Slerp)
                {
                    whom.position = Vector3.Slerp(whom.position, position, Time.deltaTime * Speed);
                } else
                {
                    whom.position = Vector3.Lerp(whom.position, position, Time.deltaTime * Speed);
                }


                yield return null;
            }
        }

        #endregion

        #region Array

        public static T RandomArrayItem<T>(List<T> arr)
        {
            return arr[RandomArrayNumber(arr)];
        }


        public static int RandomArrayNumber<T>(T[] arr)
        {
            return Random.Range(0, arr.Length - 1);
        }


        public static int RandomArrayNumber<T>(List<T> arr)
        {
            return Random.Range(0, arr.Count - 1);
        }


        public static T RandomArrayItem<T>(T[] arr)
        {
            return arr[RandomArrayNumber(arr)];
        }

        #endregion
    }
}