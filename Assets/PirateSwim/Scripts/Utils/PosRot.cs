using System.Collections;
using UnityEngine;

namespace PirateSwim
{
    public class PosRot
    {
        Vector3    pos;
        Quaternion rot;


        public PosRot(Vector3 pos, Quaternion rot)
        {
            this.pos = pos;
            this.rot = rot;
        }


        public PosRot(Transform transform) : this(transform.localPosition, transform.rotation)
        {
        }


        public IEnumerator Slerp(Transform transform, float duration)
        {
            var speed = Time.deltaTime / duration;


            for (var i = 0f; i <= 1; i += speed)
            {
                transform.rotation      = Quaternion.Slerp(transform.rotation, rot, speed);
                transform.localPosition = Vector3.Slerp(transform.localPosition, pos, speed);


                yield return null;
            }
        }
    }
}