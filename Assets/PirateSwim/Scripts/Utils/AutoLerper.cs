using DG.Tweening.Core;

namespace PirateSwim
{
    public class AutoLerper<T> : Lerper<T>
    {
        public DOGetter<T> a;
        public DOGetter<T> b;

        public AutoLerper(float duration, DOGetter<T> a, DOGetter<T> b, LerperFunc func) : base(duration, func)
        {
            this.duration = duration;
            this.a = a;
            this.b = b;
        }

        public new T value => value(a.Invoke(), b.Invoke());
    }
}