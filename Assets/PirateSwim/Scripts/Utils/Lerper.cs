using System;
using UnityEngine;

namespace PirateSwim
{
    public  class Lerper<T>
    {
        float                    startTime;
        public float             time => Time.time - startTime;
        public float             t    => time / duration;
        public float             duration;
        public bool              Complete => t >= 1;
        public Action<Lerper<T>> onComplete;


        public delegate T LerperFunc(T a, T b, float t);


        public LerperFunc _func;


        public Lerper(float duration, LerperFunc func)
        {
            startTime = Time.time;
            this.duration = duration;
            _func = func;
        }

        /*T[] smoothHandles;
        public T cubicValue(T p0, T p3)
        {
          var  i1 = _func(p0,     smoothHandles[1], t); // p1 is the handle for p0
    var        i2 = _func(smoothHandles[1],     smoothHandles[2], t); // p2 is the handle for p3
     var       i3 = _func(smoothHandles[2],     p3, t);
     var       j1 = _func(i1,     i2, t); // lerp between the previous 3 points
      var      j2 = _func(i2,     i3, t);
        return     _func(j1, j2, t); // lerp between the 2 for the result
        }
*/
        public T value(T a, T b)
        {
            if (Complete && onComplete != null)
            {
                onComplete.Invoke(this);
                onComplete = null;
            }

            var res = _func(a, b, t);


            return res;
        }
    }
}