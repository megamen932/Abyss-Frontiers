using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine.Events;

namespace PirateSwim
{
    [Serializable]
    public struct SwitchEvent<T>
    {
        public   event Action<T, bool> Change;
        public event   Action<T>       On;
        public event   Action<T>       Off;


        [NotNull] T _target;

        public void Activate()
        {
            DoChange(true);
            On?.Invoke(_target);
        }

        public void Deactivate()
        {
            DoChange(false);
            Off?.Invoke(_target);
        }

        /// <summary>
        /// Activate change delegate, Activation happens before On or Off events
        /// </summary>
        /// <param name="arg"></param>
        void DoChange(bool arg)
        {
            Change?.Invoke(_target, arg);
        }

        public SwitchEvent([NotNull] T target) : this()
        {
            _target = target;
        }

        public void Changed(bool value)
        {
            if (value)
            {
                Activate();
            } else
            {
                Deactivate();
            }
        }
    }


 

    [Serializable]
    public abstract class  AutoPropertyEventBase<TValue> :SerializedBehaviour
    {
      //  public delegate void PropertyChangedEventHandler( TValue newValue);

       // public event PropertyChangedEventHandler PropertyChanged;

       public delegate void Call();

      // public event Call DoCall;
      public Call call;
       [CanBeNull,OdinSerialize,Sirenix.OdinInspector.ReadOnly] TValue _value ;

        public TValue Value
        {
            get => _value;
            set
            {
                if (Equals(_value, value))
                {
                    return;
                }

                _value = value;
                PropertyChange();
            }
        }

       public virtual void PropertyChange()
        {
          call?.Invoke();
        }


        public AutoPropertyEventBase( TValue value) 
        {
            _value = value;
        }
    }

   [Serializable]
   public class AutoPropertyEvent<TParent,TValue>:AutoPropertyEventBase<TValue>
    {

        readonly TParent _sender;

        public delegate void EventDelegate(TParent sender, TValue value);
        public event EventDelegate ChangeEvent;

        public TParent Sender => _sender;

        public AutoPropertyEvent(TParent sender,TValue value) : base(value)
        {
            _sender = sender;
            call = () => ChangeEvent.Invoke(Sender, Value);
        }

       
    }

   [Serializable]
    public class AutoPropertyEvent<TParent>: AutoPropertyEvent<TParent,bool>
    {
        public event Action On;
        public event Action Off;

        public override void PropertyChange()
        {
            base.PropertyChange();
            if (Value == false)
            {
                Off.Invoke();
            } else
            {
                On.Invoke();
            }

        }

        public AutoPropertyEvent(TParent sender, bool value) : base(sender, value)
        {
            
        }
    }




    #region UnityEvent

    [Serializable]
    struct SwitchUnityEvent
    {
        #region Fields

        #endregion

        #region Public properties

        [OdinSerialize] public UnityEvent Change { get; set; }
        [OdinSerialize] public UnityEvent On     { get; set; }
        [OdinSerialize] public UnityEvent Off    { get; set; }

        #endregion


        #region Methods

        public SwitchUnityEvent (UnityAction change = null, UnityAction on = null, UnityAction off = null)
        {
            Change = new UnityEvent();
            On = new UnityEvent();
            Off = new UnityEvent();

            On.AddListener(Change.Invoke);
            Off.AddListener(Change.Invoke);

            On.AddListener(on);
            Off.AddListener(off);
            Change.AddListener( change);
        }

        #endregion
    }

    [Serializable]
    struct SwitchUnityEvent<T>
    {
        #region Fields

        #endregion

        #region Public properties

        [OdinSerialize] public Switch2 Change { get; set; }
        [OdinSerialize] public Switch  On     { get; set; }
        [OdinSerialize] public Switch  Off    { get; set; }

        #endregion


        #region Methods

        public SwitchUnityEvent (T invoker, UnityAction<T, bool> change = null, UnityAction<T> on = null, UnityAction<T> off = null)
        {
            Change = new Switch2(invoker);
            On = new Switch(invoker);
            Off = new Switch(invoker);

            On.AddListener(Change.DoOn);
            Off.AddListener(Change.DoOff);

            On.AddListener(on);
            Off.AddListener(off);
            Change.AddListener( change);
        }

        #endregion

        [Serializable]
        public class Switch : UnityEvent<T>
        {
            #region Fields

            #endregion

            #region Public properties

            public T Target { get; }

            #endregion

            public Switch(T target)
            {
                this.Target = target;
            }

            public new void Invoke(T arg0)
            {
                Do();
            }

            public void Invoke()
            {
                Do();
            }

            public void Do()
            {
                base.Invoke(Target);
            }

            #region Methods

            #endregion
        }

        [Serializable]
        public  class Switch2 : UnityEvent<T, bool>
        {
            public T Target { get; }

            public Switch2(T target)
            {
                this.Target = target;
            }

            public void Do(bool value)
            {
                base.Invoke(Target, value);
            }

            public void DoOn()
            {
                Do(true);
            }

            public void DoOff()
            {
                Do(false);
            }

            public void DoOff(T arg0)
            {
                DoOff();
            }

            public void DoOn(T arg0)
            {
                DoOn();
            }
        }
    }

    #endregion
}