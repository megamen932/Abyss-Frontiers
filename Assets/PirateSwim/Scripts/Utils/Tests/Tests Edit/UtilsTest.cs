﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.Diagnostics;
using UnityEngine.TestTools;


    namespace Tests
    {
        public class UtilsTest
        {
            // A Test behaves as an ordinary method
            [Test]
            public void UtilsCalcCenter()
            {
                // Use the Assert class to test conditions
                Vector3[] points =new Vector3[Random.Range(100,1000)];
                for (var index = 0; index < points.Length; index++)
                {
                    points[index] = Random.onUnitSphere;
                }

                var vec1 = new Vector3(points.Sum((m) => m.x), points.Sum((m) => m.y), points.Sum((m) => m.z)) / points.Length;
                var vec2 = PirateSwim.Utils.CalcCenterList(points);
                Assert.LessOrEqual((vec1- vec2).sqrMagnitude, 1E-10f);

                points = new [] {Vector3.left, Vector3.up};

                vec2 = PirateSwim.Utils.CalcCenterList(points);
                
                Assert.AreEqual(new Vector3(-0.5f,0.5f,0),vec2 );
                //  Assert.LessOrEqual((vec1 ).sqrMagnitude, 1E-10f);
                //   Assert.AreEqual(vec1, Vector3.zero);

            }

            // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
            // `yield return null;` to skip a frame.
            // [UnityTest]
            // public IEnumerator UnitTestWithEnumeratorPasses()
            // {
            //     // Use the Assert class to test conditions.
            //     // Use yield to skip a frame.
            //     yield return null;
            // }
        }
    }
