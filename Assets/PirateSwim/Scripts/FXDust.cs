﻿using PirateSwim.Scripts.ShipScripts;
using UnityEngine;

public class FXDust : MonoBehaviour {

	private ParticleSystem ps;
	
	Ship _ship;
	void Start()
	{
		ps = GetComponent<ParticleSystem>();
		_ship=Ship.Main;
	}

	void FixedUpdate()
	{
	
	//	var vel = ps.velocityOverLifetime;
		//vel.x = -_ship.curVelocity.x;
		//vel.y = -_ship.curVelocity.y-_ship.FakeYSpeed;
		//vel.z = -_ship.curVelocity.z;

		var shape=ps.shape;
		shape.rotation = Quaternion.LookRotation(-_ship.curVelocity-_ship.transform.up*_ship.FakeYSpeed).eulerAngles;
	}
	
}
