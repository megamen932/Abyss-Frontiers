﻿using UnityEngine;

namespace BGE
{
    public class NewFish:MonoBehaviour
    {
        public float height;
        public float headLength;
        public float bodyLength;
        public float backLength;
        public float tailLength;

    }
}
