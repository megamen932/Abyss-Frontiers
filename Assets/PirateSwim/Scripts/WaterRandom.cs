﻿using Sirenix.OdinInspector;
using UnityEngine;
         [RequireComponent(typeof(Renderer))]
public class WaterRandom : MonoBehaviour
{
    [SerializeField]   [MinMaxSlider(-1, 1,true)] Vector2 FlowY;
    [SerializeField]    [MinMaxSlider(-1, 1,true)] Vector2 FlowX;
    [SerializeField, MinValue(0.001f)] Vector2 flowDuration = Vector2.one;
    [SerializeField]      Vector2 speed;
    [SerializeField]      float   duration;
    float                         timer=-1;
    Material _material;





    // Start is called before the first frame update
    void Start() {
        // ReSharper disable once PossibleNullReferenceException
        _material = this.GetComponent<Renderer>().material;
    }





   
    void FixedUpdate()
    {
        if (timer < 0)
        {
            timer = duration;
            speed = new Vector2(Random.Range(FlowX.x, FlowX.y), Random.Range(FlowX.x, FlowY.y));
        }


       
        _material.SetFloat("_FlowSpeedY", Mathf.Sin(Time.time/flowDuration.y ) * speed.y);
        _material.SetFloat("_FlowSpeedX", Mathf.Sin(Time.time/flowDuration.x ) * speed.x);
    }
}