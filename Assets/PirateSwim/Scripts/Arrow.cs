using System.Collections;
using UnityEngine;

namespace PirateSwim
{
    public class Arrow : Shell
    {
        float          t0;
        public float   fullTime { private get; set; }
        Vector3        startPos;
        public Vector3 fireVelocity { private get; set; }
        public float   g            { private get; set; }



        void FixedUpdate()
        {
            if (!inited)
            {
                return;
            }

            var t1 = Time.time - t0;
            
           //     transform.position = startPos + fireVelocity * t1 + t1 * t1 * g * .5f * Vector3.down;


                transform.localPosition = Vector3.Lerp(transform.localPosition, -AproxEnd + transform.parent.position,
                    5 * Time.deltaTime);
            

            transform.forward = fireVelocity + g * t1 * Vector3.down;
        }

        public void Init(float hTime,       Vector3    velocity, float gravity, bool logAttack,
            Vector3            impactPoint,
            Transform               target = null)
        {
            inited = true;
            fullTime = hTime;
            fireVelocity = velocity;
            g = gravity;
            this.target = target;
           
            Invoke("CreateSparcels", fullTime);

            startPos = transform.position;
            t0 = Time.time;
           

            this.logAttack = logAttack;
            AproxEnd = impactPoint;
        }

        public Vector3 AproxEnd { get; set; }
    }

    public abstract class Shell : MonoBehaviour
    {
        public Transform       target;
      
   
        public GameObject DestroySparcs;
        public bool       inited = false;

        protected bool logAttack { get; set; }
        //public abstract void Init();

        IEnumerator MoveSparcs(GameObject sparcs)
        {
            for (float i = 0; i < 0.20f; i += Time.deltaTime)
            {
                if (target == null)
                {
                    yield break;
                } else
                {
                    sparcs.transform.position = Vector3.Lerp(sparcs.transform.position, target.transform.position, i);
                }

                yield return null;
            }
        }

        protected void CreateSparcels()
        {
            if (DestroySparcs != null)
            {
                var sparcs = Instantiate(DestroySparcs, transform.position, Quaternion.identity);
                Destroy(sparcs, .5f);

                if (target)
                {
                    StartCoroutine("MoveSparcs", sparcs);
                  
                }
            }

            Destroy(gameObject);
        }
    }
}