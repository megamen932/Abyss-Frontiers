﻿using UnityEngine;

using QuickEngine.Extensions;

public class FgController : MonoBehaviour
{

	

	public GameObject[] fogPlanes=new GameObject[1];

	public Material fogMerial;
	// Use this for initialization

	// Update is called once per frame

	void FixedUpdate()
	{
		fogMerial.SetColor("_TintColor",RenderSettings.fogColor);
		for (var i = 0; i < fogPlanes.Length; i++)
		{
			var plane = fogPlanes[i];
			var start = RenderSettings.fogStartDistance;
			var end   = RenderSettings.fogEndDistance;
			plane.transform.SetLocalZ(Mathf.Lerp(start, end,((float)i)/fogPlanes.Length));
		}
	}
}
