using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[Serializable]
[PostProcess(typeof(ForwardFogRender), PostProcessEvent.AfterStack, "Hidden/Custom/ForwardFog")]
public class ForwardFogCameraEffect : PostProcessEffectSettings
{
    /// <summary>
    /// Should the fog affect the skybox?
    /// </summary>
    [Tooltip("Mark true for the fog to ignore the skybox")]
    public BoolParameter excludeSkybox = new BoolParameter {value = true};
}


public sealed class ForwardFogRender  : PostProcessEffectRenderer<ForwardFogCameraEffect>
{
    /// <summary>
    /// If <c>true</c>, enables the internal deferred fog pass. Actual fog settings should be
    /// set in the Lighting panel.
    /// </summary>
    public override DepthTextureMode GetCameraFlags()
    {
        return DepthTextureMode.Depth;
    }


    public override void Render(PostProcessRenderContext context)
    {
        var sheet = context.propertySheets.Get(Shader.Find("Hidden/Custom/ForwardFog"));

        sheet.ClearKeywords();

        var fogColor = RuntimeUtilities.isLinearColorSpace ? RenderSettings.fogColor.linear : RenderSettings.fogColor;
        sheet.properties.SetVector(Shader.PropertyToID("_FogColor"), fogColor);
        sheet.properties.SetVector(Shader.PropertyToID("_FogParams"),
            new Vector3(RenderSettings.fogDensity, RenderSettings.fogStartDistance, RenderSettings.fogEndDistance));

        var cmd = context.command;
        cmd.BlitFullscreenTriangle(context.source, context.destination, sheet,  settings.excludeSkybox ? 1 : 0);
    }
}