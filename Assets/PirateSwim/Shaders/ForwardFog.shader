Shader "Hidden/Custom/ForwardFog"
{
   
    HLSLINCLUDE

        #pragma multi_compile __ FOG_LINEAR FOG_EXP FOG_EXP2
       // #include "../StdLib.hlsl"
         #include "Packages/com.unity.postprocessing/PostProcessing/Shaders/StdLib.hlsl"
        #ifndef UNITY_POSTFX_FOG
        #define UNITY_POSTFX_FOG
        
        half4 _FogColor;
        float3 _FogParams;
        
        #define FOG_DENSITY _FogParams.x
        #define FOG_START _FogParams.y
        #define FOG_END _FogParams.z
        
        half ComputeFog(float z)
        {
            half fog = 0.0;
        #if FOG_LINEAR
            fog = (FOG_END - z) / (FOG_END - FOG_START);
        #elif FOG_EXP
            fog = exp2(-FOG_DENSITY * z);
        #else // FOG_EXP2
            fog = FOG_DENSITY * z;
            fog = exp2(-fog * fog);
        #endif
            return saturate(fog);
        }
        
        float ComputeFogDistance(float depth)
        {
            float dist = depth * _ProjectionParams.z;
            dist -= _ProjectionParams.y;
            return dist;
        }
        
        #endif // UNITY_POSTFX_FOG

        TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);
        TEXTURE2D_SAMPLER2D(_CameraDepthTexture, sampler_CameraDepthTexture);

        #define SKYBOX_THREASHOLD_VALUE 0.9999

        float4 Frag(VaryingsDefault i) : SV_Target
        {
            half4 color = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoordStereo);

            float depth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, i.texcoordStereo);
            depth = Linear01Depth(depth);
            float dist = ComputeFogDistance(depth);
            half fog = 1.0 - ComputeFog(dist);

            return lerp(color, _FogColor, fog);
        }

        float4 FragExcludeSkybox(VaryingsDefault i) : SV_Target
        {
            half4 color = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoordStereo);

            float depth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, i.texcoordStereo);
            depth = Linear01Depth(depth);
            float skybox = depth < SKYBOX_THREASHOLD_VALUE;
            float dist = ComputeFogDistance(depth);
            half fog = 1.0 - ComputeFog(dist);

            return lerp(color, _FogColor, fog * skybox);
        }

    ENDHLSL

    SubShader
    {
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            HLSLPROGRAM

                #pragma vertex VertDefault
                #pragma fragment Frag

            ENDHLSL
        }

        Pass
        {
            HLSLPROGRAM

                #pragma vertex VertDefault
                #pragma fragment FragExcludeSkybox

            ENDHLSL
        }
    }
}
