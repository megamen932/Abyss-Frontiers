// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Cannon"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 5.0
		#pragma surface surf Standard keepalpha noshadow noambient novertexlights 
		struct Input
		{
			float3 worldPos;
		};

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldPos = i.worldPos;
			float temp_output_29_0 = ( ( ase_worldPos.y - _WorldSpaceCameraPos.z ) - (float)-500 );
			float4 color4 = IsGammaSpace() ? float4(0.6235294,0.08528921,0,1) : float4(0.3467041,0.007889552,0,1);
			float4 color3 = IsGammaSpace() ? float4(0.8679245,0.8679245,0.8679245,1) : float4(0.7254258,0.7254258,0.7254258,1);
			float4 lerpResult6 = lerp( color4 , color3 , ( temp_output_29_0 / 850 ));
			float4 color5 = IsGammaSpace() ? float4(0.1828438,0,0.3773585,0) : float4(0.0280088,0,0.1175492,0);
			float clampResult34 = clamp( ( ( (float)-150 - temp_output_29_0 ) / 150.0 ) , (float)0 , (float)1 );
			float4 lerpResult32 = lerp( color4 , color5 , clampResult34);
			o.Albedo = (( temp_output_29_0 > (float)-150 ) ? lerpResult6 :  lerpResult32 ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16100
-1820;132;1436;783;-234.5342;192.3365;1.819405;True;True
Node;AmplifyShaderEditor.WorldPosInputsNode;9;889.1607,870.7065;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceCameraPos;10;820.4117,1095.529;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleSubtractOpNode;23;1123.852,1059.839;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;26;1153.145,1215.911;Float;False;Constant;_Int2;Int 2;0;0;Create;True;0;0;False;0;-500;0;0;1;INT;0
Node;AmplifyShaderEditor.IntNode;17;1615.941,1191.81;Float;False;Constant;_Int1;Int 1;0;0;Create;True;0;0;False;0;-150;0;0;1;INT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;29;1394.803,1031.98;Float;False;2;0;FLOAT;0;False;1;INT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;39;1861.004,1167.192;Float;False;Constant;_Float1;Float 1;0;0;Create;True;0;0;False;0;150;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;33;1844.29,1009.329;Float;False;2;0;INT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;35;1882.276,1260.031;Float;False;Constant;_Int3;Int 3;0;0;Create;True;0;0;False;0;0;0;0;1;INT;0
Node;AmplifyShaderEditor.IntNode;36;1929.377,1346.001;Float;False;Constant;_Int4;Int 4;0;0;Create;True;0;0;False;0;1;0;0;1;INT;0
Node;AmplifyShaderEditor.IntNode;14;1357.6,715.761;Float;False;Constant;_Int0;Int 0;0;0;Create;True;0;0;False;0;850;0;0;1;INT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;37;2006.867,1100.338;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;3;725.3314,609.3521;Float;False;Constant;_Color0;Color 0;0;0;Create;True;0;0;False;0;0.8679245,0.8679245,0.8679245,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;34;2199.831,1073.144;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;12;1190.515,830.6672;Float;False;2;0;FLOAT;0;False;1;INT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;5;1889.487,658.6121;Float;False;Constant;_Color2;Color 2;0;0;Create;True;0;0;False;0;0.1828438,0,0.3773585,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;4;973.3202,645.3577;Float;False;Constant;_Color1;Color 1;0;0;Create;True;0;0;False;0;0.6235294,0.08528921,0,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;6;1320.498,550.4423;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;32;1941.532,874.1017;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;31;1396.066,194.9276;Float;False;Constant;_Float0;Float 0;0;0;Create;True;0;0;False;0;0.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;30;1370.236,308.8829;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareGreater;24;1578.669,668.186;Float;False;4;0;FLOAT;0;False;1;INT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2046.968,191.2031;Float;False;True;7;Float;ASEMaterialInspector;0;0;Standard;Cannon;False;False;False;False;True;True;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;False;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;23;0;9;2
WireConnection;23;1;10;3
WireConnection;29;0;23;0
WireConnection;29;1;26;0
WireConnection;33;0;17;0
WireConnection;33;1;29;0
WireConnection;37;0;33;0
WireConnection;37;1;39;0
WireConnection;34;0;37;0
WireConnection;34;1;35;0
WireConnection;34;2;36;0
WireConnection;12;0;29;0
WireConnection;12;1;14;0
WireConnection;6;0;4;0
WireConnection;6;1;3;0
WireConnection;6;2;12;0
WireConnection;32;0;4;0
WireConnection;32;1;5;0
WireConnection;32;2;34;0
WireConnection;30;0;29;0
WireConnection;30;1;31;0
WireConnection;24;0;29;0
WireConnection;24;1;17;0
WireConnection;24;2;6;0
WireConnection;24;3;32;0
WireConnection;0;0;24;0
ASEEND*/
//CHKSM=1110D800C30FA099DB6841FA9F6E65A843CDEBC6