﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace JetBrains.Rider.Unity.Editor.AssetPostprocessors
{
	[InitializeOnLoad]
	public static class Bootstrap
	{
		static Bootstrap()
		{
			if (UnityUtils.UnityVersion > new Version(2017, 1))
			{
				Assembly[] loadedAssemblies = (Assembly[]) ourSLoadedAssembliesGetter?.Invoke(null, new object[0]);
				if (loadedAssemblies == null)
				{
					Debug.LogError("Error getting 'UnityEditor.EditorAssemblies.loadedAssemblies' by reflection");
					return;
				}

				if (ShiftToLast(loadedAssemblies, a => Equals(a, typeof(CsprojAssetPostprocessor).Assembly)))
				{
					CsprojAssetPostprocessor.OnGeneratedCSProjectFiles();
				}
			}
		}

		private static bool ShiftToLast<T>(this T[] list, Predicate<T> predicate)
		{
			int lastIdx = list.Length - 1;
			int idx     = Array.FindIndex(list, predicate);
			if (lastIdx < 0 || idx < 0 || idx == lastIdx) return false;
			T temp = list[idx];
			Array.Copy(list, idx + 1, list, idx, lastIdx - idx);
			list[lastIdx] = temp;
			return true;
		}

		private static readonly MethodInfo ourSLoadedAssembliesGetter = typeof(EditorWindow).Assembly.GetType("UnityEditor.EditorAssemblies")?.
			GetProperty("loadedAssemblies", BindingFlags.Static | BindingFlags.NonPublic)?.GetGetMethod(true);
	}
}