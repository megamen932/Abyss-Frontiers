using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;


namespace ProceduralToolkit.Examples
{
    public class LowTubeGenerator : MonoBehaviour
    {
        [Serializable]
        public class Config
        {
            [MinValue(0)] public float height  = 1f;
            [MinValue(0)] public int   nbSides = 24;

            [MinValue(0)] public int segmentsCount = 4;

            // Outter shell is at radius1 + radius2 / 2, inner shell at radius1 - radius2 / 2

            public float bottomRadius1 = .5f;
            public float bottomRadius2 = .15f;
            public float topRadius1    = .5f;
            public float topRadius2    = .15f;

            public int nbVerticesCap   => nbSides * 2 + 2;
            public int nbVerticesSides => nbSides * 2 + 2;


            public List<CurveConfig> curves     = new List<CurveConfig>(3);
            public List<MultiSine>   multiSines = new List<MultiSine>(3);


            [Serializable]
            public class CurveConfig
            {
                [MinValue(0)] public float          xR         = 3;
                [MinValue(0)] public float          yR         = 3;
                [MinValue(0)] public float          totalAngle = 360;
                public               Vector2        offset;
                public               AnimationCurve AnimStengthX = AnimationCurve.Linear(0, 1, 1, 1);
                public               AnimationCurve AnimStengthY = AnimationCurve.Linear(0, 1, 1, 1);
                public               AnimationCurve AnimFreqX    = AnimationCurve.Linear(0, 1, 1, 1);
                public               AnimationCurve AnimFreqY    = AnimationCurve.Linear(0, 1, 1, 1);
                public               bool           enabled      = true;
            }

            [Serializable]
            public class Bend
            {
                public bool  enabled = false;
                public float rotate  = 90;


                public float fromPosition = 0.5F; //from 0 to 1

                public bool X;
                public bool Y;
                public bool Z;
            }

            [Serializable]
            public class MultiSine
            {
                public Vector3 timeOffset;


                public float          Strength    = 5;
                public Vector3        frequncy    = Vector3.one;
                public AnimationCurve AnimStength = AnimationCurve.Linear(0, 1, 1, 1);
                public AnimationCurve AnimFreq    = AnimationCurve.Linear(0, 1, 1, 1);
                public bool           enabled     = true;
            }


            public Bend      bend;
            public Transform parent;
            public bool      Bottom;
            public bool      Top;
            public bool      SidesIn;
            public bool      SidesOut;
        }





        public static Vector3 MultiSane(
                Config           c,
                Config.MultiSine b,
                Vector3          normal,
                Vector3          point,
                float            yNorm
                )
        {
            normal = -normal.normalized;


            if (Math.Abs(normal.y) > Mathf.Epsilon)
            {
                return Vector3.zero;
            }
            /*   var deb = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
               deb.parent = c.parent;
               deb.localScale = new Vector3(0.1f, 0.1f, 1f);
               deb.position = point + normal;
               deb.rotation = Quaternion.LookRotation(normal);*/


            yNorm = (yNorm + b.timeOffset.z) * b.frequncy.z;
            var x = (b.frequncy.x + b.timeOffset.x) / (c.bottomRadius1 + c.topRadius1) * 2       * point.x;
            var z = (b.frequncy.y + b.timeOffset.y) * point.y / (c.bottomRadius1 + c.topRadius1) * 2;


            float y = b.AnimStength.Evaluate(yNorm) * b.Strength *
                      Mathf.Sin(Mathf.PI * (x + yNorm + z) * b.AnimFreq.Evaluate(yNorm));


            return y * normal;
        }





        static Vector3 Curve(float t, Config.CurveConfig c)
        {
            return new
                    Vector3(c.AnimStengthX.Evaluate(t) * c.xR * Mathf.Sin((t + c.offset.x) * c.totalAngle * Mathf.Deg2Rad * c.AnimFreqX.Evaluate(t)),
                            0,
                            c.AnimStengthY.Evaluate(t) * c.yR *
                            Mathf.Cos((t + c.offset.y) * c.totalAngle * Mathf.Deg2Rad * c.AnimFreqY.Evaluate(t)));
        }





        public static MeshDraft Tube(Config c)
        {
            var       steps   = c.segmentsCount / 2;
            MeshDraft draft   = new MeshDraft();
            float     newbot2 = 0;
            float     newbot1 = 0;


            for (var i = 0; i < steps; ++i)
            {
                var b = new Config();
                b.nbSides       = c.nbSides;
                b.segmentsCount = c.segmentsCount;
                b.height        = c.height / steps;
                b.Bottom        = i == 0         || c.Bottom;
                b.Top           = i == steps - 1 || c.Top;
                b.SidesIn       = c.SidesIn;
                b.SidesOut      = c.SidesOut;

                var normStep = ((float) i) / steps;


                if (i == 0)
                {
                    b.bottomRadius1 = c.bottomRadius1 + (c.topRadius1 - c.bottomRadius1) * normStep;
                    b.bottomRadius2 = c.bottomRadius2 + (c.topRadius2 - c.bottomRadius2) * normStep;
                } else
                {
                    b.bottomRadius1 = newbot1;
                    b.bottomRadius2 = newbot2;
                }


                newbot1 = b.topRadius1 =
                                  c.topRadius1 + (c.bottomRadius1 - c.topRadius1) * (((float) steps - i - 1) / steps);


                newbot2 = b.topRadius2 =
                                  c.topRadius2 + (c.bottomRadius2 - c.topRadius2) * (((float) steps - i - 1) / steps);


                var newDraft = Cylynder(b);
                newDraft.Move(Vector3.up * i * b.height);


                if (c.bend.enabled)
                {
                    Bend.MeshBend(ref newDraft, c.bend.fromPosition, c.bend.rotate, c.bend.X, c.bend.Y, c.bend.Z);
                }


                draft.Add(newDraft);
            }


            draft.Move(Vector3.down * c.height / 2);
            var center = Vector3.zero;


            for (int i = 0; i < draft.vertices.Count; i++)
            {
                var zoffsetNorm = draft.vertices[i].y / c.height;


                foreach (var curveConfig in c.curves)
                {
                    if (curveConfig.enabled)
                    {
                        draft.vertices[i] += Curve(zoffsetNorm, curveConfig);
                    }
                }


                foreach (var sine in c.multiSines)
                {
                    if (sine.enabled)
                    {
                        var normal = draft.vertices[i].ToVector3XZ();
                        draft.vertices[i] += MultiSane(c, sine, normal, draft.vertices[i], zoffsetNorm);
                    }
                }


                center += draft.vertices[i];
            }


            center /= draft.vertices.Count;
            draft.Move(-center / 2);


            return draft;
        }





    #region Cylinder

        public static MeshDraft Cylynder(Config c)
        {
            var vertexCount = (c.Bottom ? c.nbVerticesCap : 0)  + (c.Top ? c.nbVerticesCap : 0) +
                              (c.SidesIn ? c.nbVerticesCap : 0) + (c.SidesOut ? c.nbVerticesSides : 0);


        #region Vertices

            // bottom + top + sides

            Vector3[] vertices = new Vector3[vertexCount];
            int       vert     = 0;
            float     _2pi     = Mathf.PI * 2f;

            // Bottom cap
            int sideCounter = 0;


            if (!c.Bottom) { } else
            {
                while (vert < c.nbVerticesCap)
                {
                    sideCounter = sideCounter == c.nbSides ? 0 : sideCounter;

                    float r1  = (float) (sideCounter++) / c.nbSides * _2pi;
                    float cos = Mathf.Cos(r1);
                    float sin = Mathf.Sin(r1);


                    vertices[vert] = new Vector3(cos * (c.bottomRadius1 - c.bottomRadius2 * .5f), 0f,
                                                 sin * (c.bottomRadius1 - c.bottomRadius2 * .5f));


                    vertices[vert + 1] = new Vector3(cos * (c.bottomRadius1 + c.bottomRadius2 * .5f), 0f,
                                                     sin * (c.bottomRadius1 + c.bottomRadius2 * .5f));


                    vert += 2;
                }
            }


            // Top cap
            sideCounter = 0;


            if (!c.Top) { } else
            {
                while (vert < c.nbVerticesCap * 2)
                {
                    sideCounter = sideCounter == c.nbSides ? 0 : sideCounter;

                    float r1  = (float) (sideCounter++) / c.nbSides * _2pi;
                    float cos = Mathf.Cos(r1);
                    float sin = Mathf.Sin(r1);


                    vertices[vert] = new Vector3(cos * (c.topRadius1 - c.topRadius2 * .5f), c.height,
                                                 sin * (c.topRadius1 - c.topRadius2 * .5f));


                    vertices[vert + 1] = new Vector3(cos * (c.topRadius1 + c.topRadius2 * .5f), c.height,
                                                     sin * (c.topRadius1 + c.topRadius2 * .5f));


                    vert += 2;
                }
            }


            // Sides (out)
            sideCounter = 0;


            if (c.SidesOut)
            {
                while (vert < c.nbVerticesCap * 2 + c.nbVerticesSides)
                {
                    sideCounter = sideCounter == c.nbSides ? 0 : sideCounter;

                    float r1  = (float) (sideCounter++) / c.nbSides * _2pi;
                    float cos = Mathf.Cos(r1);
                    float sin = Mathf.Sin(r1);


                    vertices[vert] = new Vector3(cos * (c.topRadius1 + c.topRadius2 * .5f), c.height,
                                                 sin * (c.topRadius1 + c.topRadius2 * .5f));


                    vertices[vert + 1] = new Vector3(cos * (c.bottomRadius1 + c.bottomRadius2 * .5f), 0,
                                                     sin * (c.bottomRadius1 + c.bottomRadius2 * .5f));


                    vert += 2;
                }
            }


            // Sides (in)
            sideCounter = 0;


            if (c.SidesIn)
            {
                while (vert < vertices.Length)
                {
                    sideCounter = sideCounter == c.nbSides ? 0 : sideCounter;

                    float r1  = (float) (sideCounter++) / c.nbSides * _2pi;
                    float cos = Mathf.Cos(r1);
                    float sin = Mathf.Sin(r1);


                    vertices[vert] = new Vector3(cos * (c.topRadius1 - c.topRadius2 * .5f), c.height,
                                                 sin * (c.topRadius1 - c.topRadius2 * .5f));


                    vertices[vert + 1] = new Vector3(cos * (c.bottomRadius1 - c.bottomRadius2 * .5f), 0,
                                                     sin * (c.bottomRadius1 - c.bottomRadius2 * .5f));


                    vert += 2;
                }
            }

        #endregion

        #region Normales

            // bottom + top + sides
            Vector3[] normales = new Vector3[vertices.Length];
            vert = 0;


            // Bottom cap
            if (c.Bottom)
            {
                while (vert < c.nbVerticesCap)
                {
                    normales[vert++] = Vector3.down;
                }
            }


            // Top cap
            if (c.Top)
            {
                while (vert < c.nbVerticesCap * 2)
                {
                    normales[vert++] = Vector3.up;
                }
            }


            // Sides (out)
            sideCounter = 0;


            if (c.SidesOut)
            {
                while (vert < c.nbVerticesCap * 2 + c.nbVerticesSides)
                {
                    sideCounter = sideCounter == c.nbSides ? 0 : sideCounter;

                    float r1 = (float) (sideCounter++) / c.nbSides * _2pi;

                    normales[vert]     =  new Vector3(Mathf.Cos(r1), 0f, Mathf.Sin(r1));
                    normales[vert + 1] =  normales[vert];
                    vert               += 2;
                }
            }


            // Sides (in)
            if (c.SidesIn)
            {
                sideCounter = 0;
            }


            while (vert < vertices.Length)
            {
                sideCounter = sideCounter == c.nbSides ? 0 : sideCounter;

                float r1 = (float) (sideCounter++) / c.nbSides * _2pi;

                normales[vert]     =  -(new Vector3(Mathf.Cos(r1), 0f, Mathf.Sin(r1)));
                normales[vert + 1] =  normales[vert];
                vert               += 2;
            }

        #endregion

        #region UVs

            Vector2[] uvs = new Vector2[vertices.Length];

            vert = 0;

            // Bottom cap
            sideCounter = 0;


            if (c.Bottom)
            {
                while (vert < c.nbVerticesCap)
                {
                    float t = (float) (sideCounter++) / c.nbSides;
                    uvs[vert++] = new Vector2(0f, t);
                    uvs[vert++] = new Vector2(1f, t);
                }
            }


            // Top cap
            sideCounter = 0;


            if (c.Top)
            {
                while (vert < c.nbVerticesCap * 2)
                {
                    float t = (float) (sideCounter++) / c.nbSides;
                    uvs[vert++] = new Vector2(0f, t);
                    uvs[vert++] = new Vector2(1f, t);
                }
            }


            // Sides (out)
            sideCounter = 0;


            if (c.SidesOut)
            {
                while (vert < c.nbVerticesCap * 2 + c.nbVerticesSides)
                {
                    float t = (float) (sideCounter++) / c.nbSides;
                    uvs[vert++] = new Vector2(t, 0f);
                    uvs[vert++] = new Vector2(t, 1f);
                }
            }


            // Sides (in)
            sideCounter = 0;


            if (c.SidesIn)
            {
                while (vert < vertices.Length)
                {
                    float t = (float) (sideCounter++) / c.nbSides;
                    uvs[vert++] = new Vector2(t, 0f);
                    uvs[vert++] = new Vector2(t, 1f);
                }
            }

        #endregion

        #region Triangles

            int   nbFace      = c.nbSides   * 4;
            int   nbTriangles = nbFace      * 2;
            int   nbIndexes   = nbTriangles * 3;
            int[] triangles   = new int[nbIndexes];

            // Bottom cap
            int i = 0;
            sideCounter = 0;
         


            if (c.Bottom)
            {
                sideCounter = Triangules(c, sideCounter, triangles, ref i);
            }


            var was  = sideCounter;
            var must = sideCounter + c.nbSides;

            // Top cap


            if (c.Top)
            {
                while (sideCounter < must)
                {
                    int current = sideCounter * 2 + 2;
                    int next    = sideCounter * 2 + 4;

                    triangles[i++] = current;
                    triangles[i++] = next;
                    triangles[i++] = next + 1;

                    triangles[i++] = current;
                    triangles[i++] = next    + 1;
                    triangles[i++] = current + 1;

                    sideCounter++;
                }
            }


            was  = sideCounter;
            must = sideCounter + c.nbSides;


            // Sides (out)
            if (c.SidesOut)
            {
                while (sideCounter < must)
                {
                    int current = sideCounter * 2 + 4;
                    int next    = sideCounter * 2 + 6;

                    triangles[i++] = current;
                    triangles[i++] = next;
                    triangles[i++] = next + 1;

                    triangles[i++] = current;
                    triangles[i++] = next    + 1;
                    triangles[i++] = current + 1;

                    sideCounter++;
                }
            }


            was  = sideCounter;
            must = 0;


            // Sides (in)
            if (c.SidesIn)
            {
                must = sideCounter + c.nbSides;


                while (sideCounter < must)
                {
                    int current = sideCounter * 2 + 6;
                    int next    = sideCounter * 2 + 8;

                    triangles[i++] = next + 1;
                    triangles[i++] = next;
                    triangles[i++] = current;

                    triangles[i++] = current + 1;
                    triangles[i++] = next    + 1;
                    triangles[i++] = current;

                    sideCounter++;
                }
            }

        #endregion


            var draft = new MeshDraft
                        {
                                name      = "Terrain",
                                vertices  = new List<Vector3>(vertexCount),
                                triangles = new List<int>(vertexCount),
                                normals   = new List<Vector3>(vertexCount),
                                colors    = new List<Color>(vertexCount)
                        };


            for (i = 0; i < vertexCount; i++)
            {
                draft.colors.Add(new Color(Random.value, Random.value, Random.value));
            }


            draft.vertices.AddRange(vertices);
            draft.normals.AddRange(normales);
            draft.uv.AddRange(uvs);
            draft.triangles.AddRange(triangles);


            return draft;
        }





        static int Triangules(Config c, int sideCounter, int[] triangles, ref int i)
        {
            while (sideCounter < c.nbSides)
            {
                int current = sideCounter * 2;
                int next    = sideCounter * 2 + 2;

                triangles[i++] = next + 1;
                triangles[i++] = next;
                triangles[i++] = current;

                triangles[i++] = current + 1;
                triangles[i++] = next    + 1;
                triangles[i++] = current;

                sideCounter++;
            }


            return sideCounter;
        }

    #endregion
    }
}