using Sirenix.OdinInspector;
using UnityEngine;

namespace ProceduralToolkit.Examples
{
    public class Bend : MonoBehaviour
    {
        public enum BendAxis
        {
            X,
            Y,
            Z
        };

        [OnValueChanged(nameof(DoBend))] public float rotate = 90;

        [OnValueChanged(nameof(DoBend)), Range(0, 1)]
        public float fromPosition = 0.5F; //from 0 to 1

        [OnValueChanged(nameof(DoBend))] public bool X;
        [OnValueChanged(nameof(DoBend))] public bool Y;
        [OnValueChanged(nameof(DoBend))] public bool Z;


        public static Mesh MeshBend(ref Mesh mesh, float fromPosition, float rotate, bool X, bool Y, bool Z)
        {
            var draft = new MeshDraft(mesh);
            return mesh = MeshBend(ref draft, fromPosition, rotate, X, Y, Z).ToMesh();
        }

        public static MeshDraft MeshBend(ref MeshDraft mesh, float fromPosition, float rotate, bool X, bool Y, bool Z)
        {
            Vector3[] vertices;
            vertices = mesh.vertices.ToArray();

            if (X)
            {
                float meshWidth = mesh.bounds.size.z;
                for (var i = 0; i < vertices.Length; i++)
                {
                    float formPos     = Mathf.LerpUnclamped(meshWidth / 2, -meshWidth / 2, fromPosition);
                    float zeroPos     = vertices[i].z + formPos;
                    float rotateValue = (-rotate / 2) * (zeroPos / meshWidth);

                    zeroPos -= 2 * vertices[i].x * Mathf.Cos((90 - rotateValue) * Mathf.Deg2Rad);

                    vertices[i].x += zeroPos * Mathf.Sin(rotateValue * Mathf.Deg2Rad);
                    vertices[i].z = zeroPos * Mathf.Cos(rotateValue * Mathf.Deg2Rad) - formPos;
                }
            }

            if (Y)
            {
                float meshWidth = mesh.bounds.size.z;
                for (var i = 0; i < vertices.Length; i++)
                {
                    float formPos     = Mathf.LerpUnclamped(meshWidth / 2, -meshWidth / 2, fromPosition);
                    float zeroPos     = vertices[i].z + formPos;
                    float rotateValue = (-rotate / 2) * (zeroPos / meshWidth);

                    zeroPos -= 2 * vertices[i].y * Mathf.Cos((90 - rotateValue) * Mathf.Deg2Rad);

                    vertices[i].y += zeroPos * Mathf.Sin(rotateValue * Mathf.Deg2Rad);
                    vertices[i].z = zeroPos * Mathf.Cos(rotateValue * Mathf.Deg2Rad) - formPos;
                }
            }

            if (Z)
            {
                float meshWidth = mesh.bounds.size.x;
                for (var i = 0; i < vertices.Length; i++)
                {
                    float formPos     = Mathf.LerpUnclamped(meshWidth / 2, -meshWidth / 2, fromPosition);
                    float zeroPos     = vertices[i].x + formPos;
                    float rotateValue = (-rotate / 2) * (zeroPos / meshWidth);

                    zeroPos -= 2 * vertices[i].y * Mathf.Cos((90 - rotateValue) * Mathf.Deg2Rad);

                    vertices[i].y += zeroPos * Mathf.Sin(rotateValue * Mathf.Deg2Rad);
                    vertices[i].x = zeroPos * Mathf.Cos(rotateValue * Mathf.Deg2Rad) - formPos;
                }
            }
mesh.vertices.Clear();
            
            mesh.vertices.AddRange(vertices);
            return mesh;
        }

        [Button("Bend")]
        void DoBend()
        {
            GetComponent<TubeConfigurator>().Generate();
            var temp = GetComponent<MeshFilter>().sharedMesh;

            GetComponent<MeshFilter>().sharedMesh = MeshBend(ref temp, fromPosition, rotate, X, Y, Z);
        }
    }
}