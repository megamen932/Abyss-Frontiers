using Sirenix.OdinInspector;
using UnityEngine;

namespace ProceduralToolkit.Examples
{
    [RequireComponent(typeof(MeshCollider))]
    [RequireComponent(typeof(MeshFilter))]
    public class TubeConfigurator : ConfiguratorBase
    {
        public MeshFilter   terrainMeshFilter;
        public MeshCollider terrainMeshCollider;
       

        [OnValueChanged(nameof(Generate), true)]
        public LowTubeGenerator.Config config = new LowTubeGenerator.Config();

        //BezierSpline spline;


        Transform debugRoot
        {
            get
            {
                if (!_debugRoot)
                {
                    _debugRoot = new GameObject(nameof(debugRoot)).transform;
                    _debugRoot.parent = transform;
                }

                return _debugRoot;
            }
        }

        Transform _debugRoot;


        private Mesh terrainMesh;


        [Button("Generate", ButtonSizes.Medium)]
        public void Generate()
        {
            if (_debugRoot != null)
            {
                DestroyImmediate(_debugRoot.gameObject);
            }


            config.parent = debugRoot;

            //  config.parent = debugRoot;
            var draft = LowTubeGenerator.Tube(config);
            // draft.Move(Vector3.down * config.height / 2 + Vector3.left * (config.bottomRadius1 + config.topRadius1) / 2);
            AssignDraftToMeshFilter(draft, terrainMeshFilter, ref terrainMesh);
           // terrainMesh.RecalculateNormals();
            terrainMeshCollider.sharedMesh = terrainMesh;
        }

        void Awake()
        {
            terrainMeshFilter = GetComponent<MeshFilter>();
            terrainMeshCollider = GetComponent<MeshCollider>();
        }

        void Start()
        {
            Generate();
        }
    }
}